// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'

// Components.
import NavbarButtonPreview from '../../ButtonPreviews/NavbarButtonPreview/NavbarButtonPreview'
import Image from '../../../UI/Image/Image'
import Callout from '../../../UI/Callout/Callout'
import KeyboardKey from '../../../UI/KeyboardKey/KeyboardKey'

// Screenshots.
import gridEnabledScreenshot from '../../../../images/Screenshots/grid-enabled.jpg'
import gridDisabled from '../../../../images/Screenshots/grid-disabled.jpg'

// Constants.
import { shortcuts } from '../../../../shared/constants'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const section = (props) => {
  // Init the depth class.
  let depthClass = null
  if (+props.depth > 1) depthClass = `ml-${+props.depth - 1}0`

  return (
    <Fragment>
      <p className={depthClass}>You can configure the visibility of the grid in your lines by clicking <NavbarButtonPreview>Edit</NavbarButtonPreview> - <NavbarButtonPreview>Toggle grid</NavbarButtonPreview>, on the main navbar. The grid visibility <b>will be saved</b> within the information about your project, and will remain when you re-open the project.</p>

      <div className={depthClass}>
        <Callout type='Info' label='TIP'>
          You can also toggle the grid by clicking {shortcuts.collapseLine.keys.map((key, idx) => <KeyboardKey key={idx} idx={idx}>{key}</KeyboardKey>)}, {shortcuts.toggleGrid.keys.map((key, idx) => <KeyboardKey key={idx} idx={idx}>{key}</KeyboardKey>)} and {shortcuts.removeLine.keys.map((key, idx) => <KeyboardKey key={idx} idx={idx}>{key}</KeyboardKey>)} respectively while having your cursor over it.
        </Callout>
      </div>

      <table style={{ marginTop: 30, width: '100%' }}>
        <tbody>
          <tr>
            <td>
              <div className='img-container'>
                <Image src={gridEnabledScreenshot} alt='Grid enabled (6/8 sample)' />
                <div className='img-legend'>Grid enabled (6/8 sample).</div>
              </div>
            </td>
            <td>
              <div className='img-container'>
                <Image src={gridDisabled} alt='Grid disabled (6/8 sample)' />
                <div className='img-legend'>Grid disabled (6/8 sample).</div>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </Fragment>
  )
}

// Export.
export default section
