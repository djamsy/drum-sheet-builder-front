// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import _ from 'lodash'
import React, { Fragment } from 'react'

// Constants.
import { instruments, instrumentIconTypes } from '../../../../shared/constants'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const hitTypeDocs = (props) => {
  // Init the depth class.
  let depthClass = null
  if (+props.depth > 1) depthClass = `ml-${+props.depth - 1}0`

  // Init the icon types.
  let iconTypeTitles = _.values(instrumentIconTypes).map((iconType, iconTypeIdx) => {
    return (
      <th key={iconTypeIdx}>
        Icon {iconTypeIdx + 1}
        <small>{iconType}</small>
      </th>
    )
  })

  // Init the content.
  let content = instruments.map((instrument, instrIdx) => {
    // Init the hit type icosn for this instrument.
    let hitTypeIcons = instrument.hits.map((hit, hitIdx) => {
      // Init the icon types for this instrument.
      let iconTypes = _.values(hit.icons).map((icon, iconIdx) => {
        return (
          <td key={iconIdx}>
            <img src={icon} alt={hit.name} />
          </td>
        )
      })
      return (
        <tr key={hitIdx}>
          <td>
            {hit.name}
          </td>
          {iconTypes}
        </tr>
      )
    })

    return (
      <div className='table-container responsive' key={instrIdx}>
        <h4>{instrument.name}</h4>
        <table>
          <thead>
            <tr>
              <th>
                Type of hit
              </th>
              {iconTypeTitles}
            </tr>
          </thead>
          <tbody>
            {hitTypeIcons}
          </tbody>
        </table>
      </div>
    )
  })

  return (
    <Fragment>
      <p className={depthClass}>You can select different types of hit depending on the instruments:</p>
      {content}
    </Fragment>
  )
}

// Export.
export default hitTypeDocs
