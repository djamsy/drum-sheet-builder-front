// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Style.
import './NavbarButtonPreview.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const navbarButtonPreview = (props) => {
  return (
    <span className='NavbarButtonPreview'>
      {props.children}
    </span>
  )
}

// Export.
export default navbarButtonPreview
