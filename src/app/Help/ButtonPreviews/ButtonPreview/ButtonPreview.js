// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Style.
import './ButtonPreview.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const buttonPreview = (props) => {
  return (
    <span className={'ButtonPreview btn-' + props.type}>
      {props.children}
    </span>
  )
}

// Export.
export default buttonPreview
