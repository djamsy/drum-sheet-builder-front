// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Style.
import './InstrumentTools.css'

// Comonents.
import ToolsDropdown from './ToolsDropdown/ToolsDropdown'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const instrumentTools = (props) => {
  return (
    <div className={props.lastElement ? 'InstrumentTools' : 'InstrumentTools hidden-lg'}>
      <table className='InstrumentToolsTable'>
        <tbody>
          {
            props.instruments.map((i, idx) => (
              <tr key={'instrument-' + idx} style={{ borderColor: props.borderColor }}>
                <td style={{ height: props.instrumentSize }}>
                  <ToolsDropdown
                    instrumentSize={props.instrumentSize}
                    instrument={i}
                    lineIdx={props.lineIdx}
                    backgroundColor={props.backgroundColor} />
                </td>
              </tr>
            ))
          }
        </tbody>
      </table>
    </div>
  )
}

// Export.
export default instrumentTools
