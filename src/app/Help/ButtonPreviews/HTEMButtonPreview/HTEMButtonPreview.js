// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Style.
import './HTEMButtonPreview.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const htemButtonPreview = (props) => {
  return (
    <span className={'HTEMButtonPreview' + (props.active ? ' active' : '')}>
      {props.children}
    </span>
  )
}

// Export.
export default htemButtonPreview
