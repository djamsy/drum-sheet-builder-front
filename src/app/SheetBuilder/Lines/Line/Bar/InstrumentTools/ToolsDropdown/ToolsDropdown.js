// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCaretDown, faCaretUp } from '@fortawesome/free-solid-svg-icons'

// Components.
import ToolsDropdownContent from './ToolsDropdownContent/ToolsDropdownContent'
import Backdrop from '../../../../../../UI/Backdrop/Backdrop'

// Style.
import './ToolsDropdown.css'

// Shared.
import { makeLighter } from '../../../../../../../shared/functions'
import { timeSignatures } from '../../../../../../../shared/constants'

// Redux actions.
import * as actions from '../../../../../../../store/actions/index'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateful component declaration.
class ToolsDropdown extends Component {
  state = {
    active: false
  }

  // Class method, inherited from Component.
  shouldComponentUpdate (nextProps, nextState) {
    return (nextProps.lineIdx === this.props.lineIdx &&
      nextState.active !== this.state.active) ||
      nextProps.backgroundColor !== this.props.backgroundColor
  }

  // Toggle current state of the dropdown.
  toggleShowDropdownContent = () => {
    this.setState((prevState) => {
      return {
        active: !prevState.active
      }
    })
  }

  // Hide dropdown.
  hideDropdownContent = () => {
    return this.setState({
      active: false
    })
  }

  // Class method, inherited from Component.
  render () {
    // Calculate font size responsively.
    let fontSize = this.props.instrumentSize > 15
      ? 11 + 'px'
      : (this.props.instrumentSize - 4) + 'px'

    return (
      <div className='ToolsDropdown'>
        <button
          className='ToolsDropdownButton'
          style={{ fontSize: fontSize, backgroundColor: this.props.backgroundColor }}
          onClick={this.toggleShowDropdownContent}
        >
          Tools <FontAwesomeIcon icon={this.state.active ? faCaretUp : faCaretDown} />
        </button>
        <Backdrop show={this.state.active} clicked={() => this.hideDropdownContent()} transparent />
        <ToolsDropdownContent
          show={this.state.active}
          lineIdx={this.props.lineIdx}
          instrument={this.props.instrument}
          instrumentSize={this.props.instrumentSize}
          hideDropdownContent={this.hideDropdownContent}
          fillEachSteps={this.props.fillEachSteps}
          fillEachStepsOptions={timeSignatures[this.props.timeSignature].fillEachSteps}
          shiftStepsLeft={this.props.shiftStepsLeft}
          shiftStepsRight={this.props.shiftStepsRight}
          resetSteps={this.props.resetSteps}
          backgroundColor={makeLighter(this.props.backgroundColor, 1)}
        />
      </div>
    )
  }
}

// ============================================================================
// Connect with Redux and export.
// ============================================================================

// State mapping.
const mapStateToProps = state => {
  return {
    timeSignature: state.project.timeSignature
  }
}

// Action mapping.
const mapDispatchToProps = dispatch => {
  return {
    fillEachSteps: (stepSize, lineIdx, instrument) => dispatch(actions.fillEachSteps(stepSize, lineIdx, instrument)),
    shiftStepsLeft: (lineIdx, instrument) => dispatch(actions.shiftStepsLeft(lineIdx, instrument)),
    shiftStepsRight: (lineIdx, instrument) => dispatch(actions.shiftStepsRight(lineIdx, instrument)),
    resetSteps: (lineIdx, instrument) => dispatch(actions.resetSteps(lineIdx, instrument))
  }
}
// Export.
export default connect(mapStateToProps, mapDispatchToProps)(ToolsDropdown)
