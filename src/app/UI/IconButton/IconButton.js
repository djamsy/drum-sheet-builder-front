// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'

// Style.
import './IconButton.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const iconButton = props => {
  // Default position.
  if (props.right === undefined && props.bottom === undefined && props.left === undefined && props.top === undefined) props.right = 0
  // Init the position props.
  let positionProps = ['right', 'bottom', 'left', 'top']

  // Init custom style.
  let style = {}
  for (let i in positionProps) {
    if (positionProps.hasOwnProperty(i)) {
      if (props[positionProps[i]] !== undefined) {
        if (props[positionProps[i]] === true) style[positionProps[i]] = `0px`
        else style[positionProps[i]] = `${props[positionProps[i]]}px`
      }
    }
  }

  return (
    <button className='IconButton' onClick={props.clicked} style={style}>
      <FontAwesomeIcon icon={props.icon} />
    </button>
  )
}

// Export.
export default iconButton
