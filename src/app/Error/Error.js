// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'
import { withRouter } from 'react-router-dom'

// Components.
import Button from '../UI/Button/Button'

// Style.
import './Error.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const error = (props) => {
  // Default status.
  let status = props.status || 404

  // Get the error description, depending on the status.
  let description = null
  switch (+props.status) {
    case 404:
      description = 'Page not found'
      break
    default:
      description = 'Unknown error'
  }

  return (
    (
      <div className='Error'>
        <div className='ErrorBox'>
          <h1>{status}</h1>
          <div>{description}</div>
          <div>
            <Button
              type='primary'
              clicked={() => { props.history.push('/') }}
            >
              HOME
            </Button>
          </div>
        </div>
      </div>
    )
  )
}

// Export.
export default withRouter(error)
