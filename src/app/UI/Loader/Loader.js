// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Styles.
import './Loader.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const loader = (props) => (
  <div className='Loader'>
    <div className='LoaderAnimation' style={props.top ? { top: props.top } : null}><div /><div /><div /></div>
  </div>
)

// Export.
export default loader
