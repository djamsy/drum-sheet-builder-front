// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Component } from 'react'
import { Route, Switch, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

// Style.
import './App.css'
import './stacking.css'

// Components.
import Auth from './Auth/Auth'
import Help from './Help/Help'
import Recovery from './Recovery/Recovery'
import ErrorPage from './Error/Error'
import SheetBuilder from './SheetBuilder/SheetBuilder'
import CookiePolicy from './CookiePolicy/CookiePolicy'

// Redux actions.
import * as actions from '../store/actions/index'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateful component declaration.
class App extends Component {
  // Class method, inherited from Component.
  componentDidMount () {
    // Check if cookie policy has been accepted.
    this.props.checkCookieConsent()
    // Try auto-login.
    this.props.tryAutoLogin()
  }

  // Class method, inherited from Component.
  render () {
    // Render routes depending on auth state.
    let routes = (
      <Switch>
        <Route path='/' exact component={SheetBuilder} />
        <Route path='/login' render={() => <Auth mode='signIn' />} />
        <Route path='/signup' render={() => <Auth mode='signUp' />} />
        <Route path='/recovery' render={() => <Recovery />} />
        <Route path='/docs' component={Help} />
        <Route render={() => <ErrorPage status='404' />} />
      </Switch>
    )

    // Init the cookie policy modal.
    let cookiePolicy = null
    if (!this.props.cookiePolicyAccepted) {
      cookiePolicy = (
        <CookiePolicy show={!this.cookiePolicyAccepted} onAccept={this.props.acceptCookiePolicy} />
      )
    }

    if (this.props.isAuth) {
      routes = (
        <Switch>
          <Route path='/' exact render={() => <SheetBuilder isAuth />} />
          <Route path='/login' render={() => <Auth mode='signIn' />} />
          <Route path='/signup' render={() => <Auth mode='signUp' />} />
          <Route path='/recovery' render={() => <Recovery />} />
          <Route path='/docs' render={() => <Help isAuth />} />
          <Route render={() => <ErrorPage status='404' />} />
        </Switch>
      )
    }

    // Init the alert message.
    return (
      <div className='App'>
        {cookiePolicy}
        {routes}
      </div>
    )
  }
}

// ============================================================================
// Connect with Redux and export.
// ============================================================================

// State mapping.
const mapStateToProps = state => {
  return {
    isAuth: state.auth.userId !== null,
    cookiePolicyAccepted: state.auth.cookiePolicyAccepted
  }
}

// Action mapping.
const mapDispatchToProps = dispatch => {
  return {
    tryAutoLogin: () => dispatch(actions.authCheckState()),
    acceptCookiePolicy: () => dispatch(actions.acceptCookiePolicy()),
    checkCookieConsent: () => dispatch(actions.checkCookieConsent())
  }
}

// Export.
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App))
