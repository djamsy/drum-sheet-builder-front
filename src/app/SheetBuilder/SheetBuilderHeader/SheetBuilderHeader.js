// =============================================================================
// Dependencies.
// =============================================================================

// Components.
import GlobalNavbar from './GlobalNavbar/GlobalNavbar'
import ProjectNavbar from './ProjectNavbar/ProjectNavbar'
import React from 'react'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const sheetBuilderHeader = () => (
  <header>
    <GlobalNavbar fixed />
    <ProjectNavbar />
  </header>
)

// Export.
export default sheetBuilderHeader
