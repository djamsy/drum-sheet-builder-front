// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlay, faStop } from '@fortawesome/free-solid-svg-icons'

// Styles.
import './LinePlayer.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const linePlayer = (props) => {
  // Init the button.
  let button = (
    <button onClick={props.play}>
      <FontAwesomeIcon icon={faPlay} style={{ marginLeft: 1 }} />
    </button>
  )
  if (props.player) {
    if (props.lineIdx === props.linePlaying) {
      button = (
        <button className='active' onClick={() => {
          // Update the played step.
          if (props.onStopPlaying) props.onStopPlaying()
          // Stop playing.
          props.stopPlaying()
        }}>
          <FontAwesomeIcon icon={faStop} />
        </button>
      )
    }
  }

  return (
    <div className='LinePlayButton'>
      {button}
    </div>
  )
}

// Export.
export default linePlayer
