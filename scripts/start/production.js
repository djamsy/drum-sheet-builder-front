'use strict'

// Load environment variables.
require('dotenv').config()

// =============================================================================
// Dependencies.
// =============================================================================

// Vendor dependencies.
const fs = require('fs')
const Koa = require('koa')
const path = require('path')
const send = require('koa-send')
const util = require('util')

// =============================================================================
// Constants.
// =============================================================================

// Port.
const port = 3000

// Build path.
const distPath = path.join(__dirname, '../../dist')

// Init Koa and Router.
const app = new Koa()

// Promisify fs.access.
const access = util.promisify(fs.access)

// =============================================================================
// Export.
// =============================================================================

module.exports = () => {
  // Serve static files located in the build directory.
  app.use(async (ctx) => {
    // If root path, redirect to 'index.html'.
    if (ctx.path === '/') {
      await send(ctx, 'index.html', { root: distPath })
      return
    }
    // If no root path, check that the file exists under build path.
    await access(path.join(distPath, ctx.path), fs.constants.F_OK)
      .catch(notExists => {
        // If the file exists, it's a static file in the build directory.
        // If not, rewrite path to 'index.html' to handle 404 error from the SPA.
        if (notExists) {
          ctx.path = 'index.html'
        }
      })
    // Cache assets for 10 minutes.
    // Do not cache JS and CSS files.
    let maxAge = (['.css', '.js'].indexOf(path.extname(ctx.path)) !== -1 ? 0 : 600000)
    // Send the file.
    await send(ctx, ctx.path, { root: distPath, maxage: maxAge })
  })

  // Start server.
  app.listen(port, () => {
    console.log(`Server running on port ${port}`)
  })
}
