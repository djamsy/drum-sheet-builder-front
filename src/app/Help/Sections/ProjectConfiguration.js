// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'

// Components.
import NavbarButtonPreview from '../ButtonPreviews/NavbarButtonPreview/NavbarButtonPreview'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const section = (props) => {
  // Init the depth class.
  let depthClass = null
  if (+props.depth > 1) depthClass = `ml-${+props.depth - 1}0`

  return (
    <Fragment>
      <p className={depthClass}>You can configure several parameters about your project, which are available under section <NavbarButtonPreview>Edit</NavbarButtonPreview>, on the main navbar.</p>
    </Fragment>
  )
}

// Export.
export default section
