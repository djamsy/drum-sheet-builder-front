// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Style.
import './Select.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const select = (props) => (
  <div className='Select'>
    <select
      value={props.value}
      onChange={props.changed}
      style={{ width: (props.width ? props.width + 'px' : '100%') }}
      disabled={props.disable}
    >
      {
        props.options.map((option, idx) => (
          <option
            value={option}
            key={idx}
          >
            {option}
          </option>
        ))
      }
    </select>
  </div>
)

// Export.
export default select
