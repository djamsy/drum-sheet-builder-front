// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Styles.
import './ProjectNavbarButton.css'

// PNG icons.
import metronomeIcon from './icons/metronome.png'
import loopIcon from './icons/loop.png'
import analyzerIcon from './icons/analyzer.png'

// =============================================================================
// Constants.
// =============================================================================

// Init the icons object.
const icons = {
  metronome: metronomeIcon,
  loop: loopIcon,
  analyzer: analyzerIcon
}

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const projectPlayer = (props) => {
  return (
    <div className={`ProjectNavbarButton ${props.active ? 'active' : ''}`}>
      <button type='button' onClick={props.onClick}>
        <img src={icons[props.icon]} alt='Icon' />
      </button>
    </div>
  )
}

// Export.
export default projectPlayer
