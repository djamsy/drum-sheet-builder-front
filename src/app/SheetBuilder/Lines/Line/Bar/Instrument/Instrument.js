// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Component, Fragment } from 'react'
import _ from 'lodash'

// Style.
import './Instrument.css'
import './HitTypes/HitType/HitType.css'

// Components.
import HitTypes from './HitTypes/HitTypes'

// Shared.
import { instruments } from '../../../../../../shared/constants'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
class Instrument extends Component {
  state = {
    showHitTypeEditor: false
  }

  // Class method, inherited from Component.
  shouldComponentUpdate (nextProps, nextState) {
    return nextState.showHitTypeEditor !== this.state.showHitTypeEditor ||
      nextProps.type !== this.props.type ||
      nextProps.hitTypeEditingMode !== this.props.hitTypeEditingMode ||
      nextProps.instrumentOrder !== this.props.instrumentOrder ||
      nextProps.instrumentIcons !== this.props.instrumentIcons ||
      nextProps.instrumentSize !== this.props.instrumentSize ||
      nextProps.borderLeft !== this.props.borderLeft
  }

  // Show or hide the hit types.
  toggleShowInstrumentHitTypes = () => {
    // Update the state.
    this.setState(prevState => {
      return {
        showHitTypeEditor: !prevState.showHitTypeEditor
      }
    })
  }

  // Hide the hit types.
  hideInstrumentHitTypes = () => {
    // Update the state.
    this.setState({
      showHitTypeEditor: false
    })
  }

  // Action to perform depending on the hit type editing mode.
  _beatClick = (e, disableClick) => {
    if (!disableClick) {
      if (e.ctrlKey || this.props.hitTypeEditingMode) {
        this.toggleShowInstrumentHitTypes()
      } else {
        this.props.clicked()
      }
    }
  }

  render () {
    // Get the instrument definition from constants.
    let instrument = _.find(instruments, { label: this.props.instrument })
    // If the instrument is active.
    let active = this.props.type !== 0

    // Init the hit types.
    let hitTypes = (
      <HitTypes
        instrumentSize={this.props.instrumentSize}
        instrumentIcons={this.props.instrumentIcons}
        show={this.state.showHitTypeEditor}
        changeHit={(hitKey) => {
          this.props.changeHit(hitKey)
          this.hideInstrumentHitTypes()
        }}
        hideInstrumentHitTypes={this.hideInstrumentHitTypes}
        current={this.props.type}
        hits={instrument.hits}
      />
    )

    return (
      <Fragment>
        <div
          onClick={e => this._beatClick(e, this.props.disableClick)}
          style={{
            borderLeft: this.props.borderLeft,
            height: this.props.instrumentSize - 1,
            width: this.props.instrumentSize - 1,
            backgroundImage: active ? `url(${instrument.hits[this.props.type - 1].icons[this.props.instrumentIcons]}` : null,
            backgroundSize: this.props.instrumentSize - (this.props.instrumentSize < 22 ? 3 : 5)
          }}
          className={
            'Instrument' +
            (this.state.showHitTypeEditor ? ' EditingMode' + (this.state.showHitTypeEditor ? ' EditingModeSelected' : '') : '') +
            (this.props.disableClick ? ' disabled' : '')
          }
        />
        {hitTypes}
      </Fragment>
    )
  }
}

// Export.
export default Instrument
