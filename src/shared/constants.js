// Import instruments and export as part of constants.
export { instruments, metronome } from './instruments'

// Error messages.
export const errorMessages = {
  SERVER_DOWN: 'Server is down. Please, try again later.'
}

// Possible values of beats and bars.
export const timeSignatures = {
  '2/2': { beats: 8, bars: 4, disabledBeats: [1, 2, 3, 5, 6, 7], fillEachSteps: [4, 8], shiftBeatsSize: 4 },
  '3/4': { beats: 6, bars: 4, disabledBeats: [1, 3, 5], fillEachSteps: [2, 6], stepSize: 4, shiftBeatsSize: 2 },
  '4/4': { beats: 8, bars: 4, disabledBeats: [1, 3, 5, 7], fillEachSteps: [2, 4, 8], shiftBeatsSize: 2 },
  '5/4': { beats: 10, bars: 4, disabledBeats: [1, 3, 5, 7, 9], fillEachSteps: [2, 10], shiftBeatsSize: 2 },
  '6/8': { beats: 6, bars: 4, fillEachSteps: [1, 2, 3, 6], shiftBeatsSize: 1 },
  '7/8': { beats: 7, bars: 4, fillEachSteps: [1, 7], shiftBeatsSize: 1 },
  '8/8': { beats: 8, bars: 4, fillEachSteps: [1, 2, 4, 8], shiftBeatsSize: 1 },
  '10/8': { beats: 10, bars: 4, fillEachSteps: [1, 5, 10], shiftBeatsSize: 1 },
  '10/16': { beats: 10, bars: 2, fillEachSteps: [1, 2, 5, 10], shiftBeatsSize: 1 },
  '12/16': { beats: 12, bars: 2, fillEachSteps: [1, 3, 6, 12], shiftBeatsSize: 1 },
  '16/16': { beats: 16, bars: 2, fillEachSteps: [1, 2, 4, 8, 16], shiftBeatsSize: 1 }
}

// Default time signature.
export const defaultTimeSignature = '8/8'

// Maximum line width, in pixels.
export const maxLineWidth = 900

// Maximum instrument size, in pixels.
export const maxInstrumentSize = 24

// Instrument orders.
export const instrumentOrderTypes = {
  drumset: 'drumset',
  canonical: 'canonical'
}

// Default order of instruments.
export const defaultInstrumentOrder = instrumentOrderTypes.drumset

// Instrument icon types.
export const instrumentIconTypes = {
  drawings: 'drawings',
  canonical: 'canonical',
  pictures: 'pictures'
}

// Default images types of instruments.
export const defaultInstrumentIcons = instrumentIconTypes.drawings

// Audio libraries.
export const audioLibs = {
  lofi: 'lofi'
}

// Default audio lib.
export const defaultAudioLib = audioLibs.lofi

// Metronome audio libs.
export const metronomeAudioLibs = {
  vintage: 'vintage'
}

// Default metronome audio lib.
export const defaultMetronomeAudioLib = metronomeAudioLibs.vintage

// Default value for 'show grid'
export const defaultShowGrid = true

// Line background colors.
export const lineBgColors = {
  darkGray: { background: '#ccc', buttons: '#d3d3d3' },
  mediumGray: { background: '#ddd', buttons: '#b3b3b3' },
  lightGray: { background: '#eee', buttons: '#d3d3d3' },
  darkBlue: { background: '#c1cde6', buttons: '#b3b3b3' },
  mediumBlue: { background: '#cdd8ef', buttons: '#b3b3b3' },
  lightBlue: { background: '#dbe7ff', buttons: '#b3b3b3' },
  darkYellow: { background: '#fffacf', buttons: '#d3d3d3' },
  mediumYellow: { background: '#fffee3', buttons: '#d3d3d3' },
  lightYellow: { background: '#fffff2', buttons: '#d3d3d3' },
  darkGreen: { background: '#bed0c5', buttons: '#b3b3b3' },
  mediumGreen: { background: '#d4ded8', buttons: '#b3b3b3' },
  lightGreen: { background: '#e1e6e3', buttons: '#d3d3d3' },
  darkRed: { background: '#e0cbc8', buttons: '#b3b3b3' },
  mediumRed: { background: '#e2d5d3', buttons: '#b3b3b3' },
  lightRed: { background: '#ece4e2', buttons: '#d3d3d3' },
  darkOrange: { background: '#e8dbd7', buttons: '#b3b3b3' },
  mediumOrange: { background: '#f1e6e2', buttons: '#b3b3b3' },
  lightOrange: { background: '#f7eeeb', buttons: '#d3d3d3' }
}

// Default line background color.
export const defaultLineBgColor = lineBgColors.lightGray.background

// Separator background colors.
export const separatorBgColors = {
  darkGray: '#777',
  mediumGray: '#999',
  lightGray: '#bbb'
}

// Default separator background color.
export const defaultSeparatorBgColor = separatorBgColors.lightGray

// Line types.
export const lineTypes = {
  line: 'line',
  separator: 'separator'
}

// Key codes.
export const keyCodes = {
  enter: 13,
  space: 32,
  arrowLeft: 37,
  arrowUp: 38,
  arrowRight: 39,
  arrowDown: 40,
  insert: 45,
  supr: 46
}

// Keyboard shortcuts.
export const shortcuts = {
  saveProject: {
    func: e => e.shiftKey && e.key && e.key.toLowerCase() === 's',
    keys: ['⇧', 'S'],
    type: 'project',
    description: 'Save current project'
  },
  newProject: {
    func: e => e.shiftKey && e.key && e.key.toLowerCase() === 'n',
    keys: ['⇧', 'N'],
    type: 'project',
    description: 'Start new project. Make sure you save current project before!'
  },
  addLine: {
    func: e => e.shiftKey && e.key && e.key.toLowerCase() === 'a',
    keys: ['⇧', 'A'],
    type: 'project',
    description: 'Add new line at the end'
  },
  generateSheets: {
    func: e => e.shiftKey && e.key && e.key.toLowerCase() === 'p',
    keys: ['⇧', 'P'],
    type: 'project',
    description: 'Download sheets in PDF format'
  },
  generateWAV: {
    func: e => e.shiftKey && e.key && e.key.toLowerCase() === 'w',
    keys: ['⇧', 'W'],
    type: 'project',
    description: 'Download audio in WAV format'
  },
  generateMP3: {
    func: e => e.shiftKey && e.key && e.key.toLowerCase() === 'm',
    keys: ['⇧', 'M'],
    type: 'project',
    description: 'Download audio in MP3 format'
  },
  deleteProject: {
    func: e => e.shiftKey && e.key && e.key.toLowerCase() === 'd',
    keys: ['⇧', 'D'],
    type: 'project',
    description: 'Delete project (confirmation needed)'
  },
  toggleGrid: {
    func: e => e.shiftKey && e.key && e.key.toLowerCase() === 'g',
    keys: ['⇧', 'G'],
    type: 'project',
    description: 'Toggle visibility of the grid'
  },
  undo: {
    func: e => e.shiftKey && e.key && e.key.toLowerCase() === 'z',
    keys: ['⇧', 'Z'],
    type: 'project',
    description: 'Undo'
  },
  redo: {
    func: e => e.shiftKey && e.key && e.key.toLowerCase() === 'y',
    keys: ['⇧', 'Y'],
    type: 'project',
    description: 'Redo'
  },
  collapseAll: {
    func: e => e.shiftKey && e.key && e.key.toLowerCase() === 'c',
    keys: ['⇧', 'C'],
    type: 'project',
    description: 'Collapse all lines'
  },
  expandAll: {
    func: e => e.shiftKey && e.key && e.key.toLowerCase() === 'e',
    keys: ['⇧', 'E'],
    type: 'project',
    description: 'Expand all lines'
  },
  expandLine: {
    func: e => !e.shiftKey && e.keyCode === keyCodes.arrowDown,
    keys: ['↓'],
    type: 'line',
    description: 'Expand line below the cursor'
  },
  collapseLine: {
    func: e => !e.shiftKey && e.keyCode === keyCodes.arrowUp,
    keys: ['↑'],
    type: 'line',
    description: 'Collapse line below the cursor'
  },
  insertLineAbove: {
    func: e => !e.shiftKey && e.keyCode === keyCodes.insert,
    keys: ['INSERT'],
    type: 'line',
    description: 'Insert line before the one below the cursor'
  },
  removeLine: {
    func: e => !e.shiftKey && e.keyCode === keyCodes.supr,
    keys: ['SUPR'],
    type: 'line',
    description: 'Remove the line below the cursor'
  },
  playProject: {
    func: e => e.shiftKey && e.key && e.key.toLowerCase() === 'r',
    keys: ['⇧', 'R'],
    type: 'project',
    description: 'Play/pause project'
  }
}

// Max. history length.
export const maxHistoryLength = 10

// Step sizes, depending on the time signature divider.
export const stepSizes = {
  1: 8,
  2: 4,
  4: 2,
  8: 1,
  16: 1
}

// Minimum and maximum BPM.
export const minimumBpm = 40
export const maximumBpm = 220
