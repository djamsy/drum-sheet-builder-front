// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'

// Hit type icon labels.
import ghostHitLabel from '../../../../images/HitLabels/ghost-note.jpg'
import dragHitLabel from '../../../../images/HitLabels/drag.jpg'
import flamHitLabel from '../../../../images/HitLabels/flam.jpg'
import rimshotHitLabel from '../../../../images/HitLabels/rim-shot.jpg'
import headHitLabel from '../../../../images/HitLabels/head.jpg'
import sideStickHitLabel from '../../../../images/HitLabels/side-stick.jpg'
import bellHitLabel from '../../../../images/HitLabels/bell.jpg'
import bowHitLabel from '../../../../images/HitLabels/bow.jpg'
import pedalHitLabel from '../../../../images/HitLabels/pedal.jpg'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const section = (props) => {
  // Init the depth class.
  let depthClass = null
  if (+props.depth > 1) depthClass = `ml-${+props.depth - 1}0`

  return (
    <Fragment>
      <p className={depthClass}>The icons of the hit types follow a common pattern, based in colors:</p>

      <div className='table-container'>
        <div className='HitTypeLabelsTable'>
          <table>
            <thead>
              <tr>
                <th>Default hit</th>
                <th>Ghost note</th>
                <th>Drag</th>
                <th>Flam</th>
                <th>Rim shot</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>No icon</td>
                <td><img src={ghostHitLabel} alt='ghost' /></td>
                <td><img src={dragHitLabel} alt='drag' /></td>
                <td><img src={flamHitLabel} alt='flam' /></td>
                <td><img src={rimshotHitLabel} alt='rim-shot' /></td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className='HitTypeLabelsTable'>
          <table>
            <thead>
              <tr>
                <th>Drum head</th>
                <th>Side stick</th>
                <th>Cymbal bell</th>
                <th>Cymbal bow</th>
                <th>Hi-hat pedal</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><img src={headHitLabel} alt='head' /></td>
                <td><img src={sideStickHitLabel} alt='side-stick' /></td>
                <td><img src={bellHitLabel} alt='bell' /></td>
                <td><img src={bowHitLabel} alt='bow' /></td>
                <td><img src={pedalHitLabel} alt='pedal' /></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </Fragment>
  )
}

// Export.
export default section
