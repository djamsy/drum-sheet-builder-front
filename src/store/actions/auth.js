// ============================================================================
// Dependencies.
// ============================================================================

// Axios instance.
import axios from '../../axios'

// Action types.
import * as actions from './types'

// Functions.
import { getErrorMessage } from '../../shared/functions'

// ============================================================================
// Constants.
// ============================================================================

// Web storage variables.
const webStorageVars = {
  token: 'token',
  userId: 'userId',
  email: 'email',
  expiresIn: 'expiresIn',
  cookiePolicyAccepted: 'cookiePolicyAccepted'
}

// ============================================================================
// Action creators.
// ============================================================================

// Auth start.
export const loginStart = () => {
  return {
    type: actions.LOGIN_START
  }
}

// Auth success.
export const loginSuccess = (token, userId, email) => {
  return {
    type: actions.LOGIN_SUCCESS,
    token: token,
    userId: userId,
    email: email
  }
}

// Auth fail.
export const loginFail = (error, email) => {
  return {
    type: actions.LOGIN_FAILED,
    error: error,
    email: email
  }
}

// Log out.
export const logout = () => {
  // Remove items from web storage.
  localStorage.removeItem(webStorageVars.token)
  localStorage.removeItem(webStorageVars.userId)
  localStorage.removeItem(webStorageVars.email)
  localStorage.removeItem(webStorageVars.expiresIn)
  // Return action.
  return {
    type: actions.LOGOUT
  }
}

// Check token expiration.
export const checkAuthTimeout = (expirationTime) => {
  return dispatch => {
    setTimeout(() => {
      dispatch(logout())
    }, expirationTime)
  }
}

// Auth from server.
export const signIn = (body, isSignUp) => {
  return dispatch => {
    dispatch(loginStart())
    // Take body and endpoint depending on signup/login.
    let endpoint = '/login'
    if (isSignUp) endpoint = '/user'
    // Perform request.
    axios().post(endpoint, body)
      .then(response => {
        // Get the expiration date.
        const tokenExpirationDate = new Date(new Date().getTime() + response.data.expiresIn * 1000)
        // Save token in web storage.
        localStorage.setItem(webStorageVars.token, response.data.token)
        localStorage.setItem(webStorageVars.userId, response.data._id)
        localStorage.setItem(webStorageVars.email, response.data.email)
        localStorage.setItem(webStorageVars.expiresIn, tokenExpirationDate)
        // Dispatch actions.
        dispatch(loginSuccess(response.data.token, response.data._id, response.data.email))
        dispatch(checkAuthTimeout(+response.data.expiresIn * 1000))
      })
      .catch(error => {
        dispatch(loginFail(getErrorMessage(error), body.email))
      })
  }
}

// Change the auth redirect path.
export const setAuthRedirectPath = (path) => {
  return {
    type: actions.SET_LOGIN_REDIRECT_PATH,
    path: path
  }
}

// Check the web storage session info.
export const authCheckState = () => {
  return dispatch => {
    // Get auth items from web storage.
    const token = localStorage.getItem(webStorageVars.token)
    const userId = localStorage.getItem(webStorageVars.userId)
    const email = localStorage.getItem(webStorageVars.email)
    // If no token found, log out.
    if (!token) {
      dispatch(logout())
    } else {
      // Get expiration date.
      const expirationDate = new Date(localStorage.getItem(webStorageVars.expiresIn))
      const currentDate = new Date()
      // If not expired, same as log in.
      if (expirationDate > currentDate) {
        dispatch(loginSuccess(token, userId, email))
        dispatch(checkAuthTimeout(expirationDate.getTime() - currentDate.getTime()))
      // If expired,logout.
      } else {
        dispatch(logout())
      }
    }
  }
}

// Reset the login error.
export const resetLoginError = () => {
  return {
    type: actions.RESET_LOGIN_ERROR
  }
}

// Sign up.
// Use the same success/init/failed functions that login.
export const signUp = (body) => {
  return signIn(body, true)
}

// Accept the cookie policy.
export const acceptCookiePolicy = () => {
  // Create cookie consent flag in web storage.
  localStorage.setItem(webStorageVars.cookiePolicyAccepted, true)
  return {
    type: actions.ACCEPT_COOKIE_POLICY
  }
}

// Deny the cookie policy.
export const denyCookiePolicy = () => {
  return {
    type: actions.DENY_COOKIE_POLICY
  }
}

// Check the cookie policy consent.
export const checkCookieConsent = () => {
  return dispatch => {
    // Get the cookie consent flag from web storage.
    const cookiePolicyAccepted = localStorage.getItem(webStorageVars.cookiePolicyAccepted)
    // If not set, accept the cookie policy.
    if (!cookiePolicyAccepted) {
      dispatch(denyCookiePolicy())
    }
  }
}

// Recover user password.
export const setRecovery = email => {
  return dispatch => {
    dispatch(recoveryStart())
    // Perform request.
    axios().post('/recovery', { email: email })
      .then(response => {
        dispatch(recoverySuccess())
      })
      .catch(error => {
        dispatch(recoveryFail(getErrorMessage(error)))
      })
  }
}

// Password recovery start.
export const recoveryStart = () => {
  return {
    type: actions.RECOVERY_START
  }
}

// Password recovery success.
export const recoverySuccess = () => {
  return {
    type: actions.RECOVERY_SUCCESS
  }
}

// Password recovery fail.
export const recoveryFail = error => {
  return {
    type: actions.RECOVERY_FAILED,
    error: error
  }
}

// Reset user password.
export const resetPassword = (email, recoveryToken, newPassword) => {
  return dispatch => {
    dispatch(resetPasswordStart())
    // Perform request.
    axios().post('/recover', { email: email, recoveryToken: recoveryToken, password: newPassword })
      .then(response => {
        dispatch(resetPasswordSuccess(response.data.email))
      })
      .catch(error => {
        dispatch(resetPasswordFail(getErrorMessage(error), email))
      })
  }
}

// Password reset start.
export const resetPasswordStart = () => {
  return {
    type: actions.RESET_PASSWORD_START
  }
}

// Password reset success.
export const resetPasswordSuccess = () => {
  return {
    type: actions.RESET_PASSWORD_SUCCESS
  }
}

// Password reset error.
export const resetPasswordFail = error => {
  return {
    type: actions.RESET_PASSWORD_FAILED,
    error: error
  }
}

// Set a recovery token.
export const setRecoveryToken = recoveryToken => {
  return {
    type: actions.SET_RECOVERY_TOKEN,
    recoveryToken: recoveryToken
  }
}

// Reset recovery state.
export const resetRecoveryState = () => {
  return {
    type: actions.RESET_RECOVERY_STATE
  }
}
