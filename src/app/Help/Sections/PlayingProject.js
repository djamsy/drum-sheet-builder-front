// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlay, faStop } from '@fortawesome/free-solid-svg-icons'

// Components.
import KeyboardKey from '../../UI/KeyboardKey/KeyboardKey'
import Callout from '../../UI/Callout/Callout'
import PlayProjectButtonPreview from '../ButtonPreviews/PlayProjectButtonPreview/PlayProjectButtonPreview'
import PlayButtonPreview from '../ButtonPreviews/PlayButtonPreview/PlayButtonPreview'

// Shared.
import { shortcuts } from '../../../shared/constants'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const section = (props) => {
  // Init the depth class.
  let depthClass = null
  if (+props.depth > 1) depthClass = `ml-${+props.depth - 1}0`

  return (
    <Fragment>
      <p className={depthClass}>You can play your project anytime by clicking the play/stop buttons (<PlayProjectButtonPreview><FontAwesomeIcon icon={faPlay} /></PlayProjectButtonPreview> and <PlayProjectButtonPreview active><FontAwesomeIcon icon={faStop} /></PlayProjectButtonPreview>) on the project navbar, at the left of the loop and metronome buttons.</p>

      <p className={depthClass}>You will see that the background of the line that is being played turns a little bit darker, so that you can follow the sheets in real-time.</p>

      <div className={depthClass}>
        <Callout type='Info' label='TIP'>
          You can also start/stop playing your project by clicking {shortcuts.playProject.keys.map((key, idx) => <KeyboardKey key={idx} idx={idx}>{key}</KeyboardKey>)} once you have a project open.
        </Callout>
      </div>

      <p className={depthClass}>The audio player is based on JavaScript, so that it works on any browser. You <b>don't need</b> to install <b>Flash Player</b> nor any <b>extension</b> to play audio from this site. Also, the player is optimized so that the audio files that your browser downloads are only the ones you use in your line/project, compressed in MP3 format, cached for an hour.</p>

      <div className={depthClass}>
        <Callout type='Warning' label='WARNING'>
          Some sounds may not be played on time if your <b>CPU</b> or your <b>RAM</b> are highly loaded.
        </Callout>
      </div>

      <p className={depthClass}>You can also playplay/stop a line individually by clicking on the player icons above the line content (<PlayButtonPreview><FontAwesomeIcon icon={faPlay} /></PlayButtonPreview> and <PlayButtonPreview active><FontAwesomeIcon icon={faStop} /></PlayButtonPreview>).</p>
    </Fragment>
  )
}

// Export.
export default section
