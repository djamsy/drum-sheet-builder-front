// =============================================================================
// Dependencies.
// =============================================================================

// Vendor
import _ from 'lodash'
import React, { Component, Fragment } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faCaretDown,
  faCaretUp,
  faFile,
  faFolderOpen,
  faSave,
  faFilePdf,
  faFileAudio,
  faTrash,
  faPlus,
  faTable,
  faDrum,
  faSort,
  faBook,
  faUndo,
  faRedo,
  faSortAmountDown,
  faSortAmountUp
} from '@fortawesome/free-solid-svg-icons'

// Style.
import './GlobalNavbar.css'

// Components.
import NavbarLogo from './NavbarLogo/NavbarLogo'
import NavbarButton from './NavbarButton/NavbarButton'
import NavbarDropdown from './NavbarDropdown/NavbarDropdown'
import NavbarWatermark from './NavbarWatermark/NavbarWatermark'
import DeleteProjectModal from './DeleteProjectModal/DeleteProjectModal'
import DownloadWavModal from './DownloadWavModal/DownloadWavModal'
import DownloadMp3Modal from './DownloadMp3Modal/DownloadMp3Modal'
import DownloadProjectModal from './DownloadProjectModal/DownloadProjectModal'

// Redux actions.
import * as actions from '../../../../store/actions/index'

// Functions.
import { ucFirst } from '../../../../shared/functions'

// Constants.
import { instrumentIconTypes, instrumentOrderTypes, shortcuts } from '../../../../shared/constants'

// =============================================================================
// Component declaration.
// =============================================================================

// Steteless component declaration.
class GlobalNavbar extends Component {
  state = {
    fileMenuOpen: false,
    editMenuOpen: false,
    helpMenuOpen: false,
    deleteProjectModalOpen: false,
    configurationModalOpen: false
  }

  // Class method, inherited from Component.
  componentDidMount () {
    // Fetch the projects from the server.
    this.props.fetchProjects(this.props.userId, this.props.token)
    // Add keyboard listener for events.
    document.addEventListener('keydown', this._handleKeyboardShortcuts, false)
  }

  // Open menu section.
  openSection = section => {
    this.setState({ [section + 'MenuOpen']: true })
  }

  // Open menu section.
  closeSection = section => {
    this.setState({ [section + 'MenuOpen']: false })
  }

  // Show the 'delete' modal.
  showDeleteConfirmation = () => {
    this.setState({ deleteProjectModalOpen: true })
  }

  // Hide the 'delete' modal.
  hideDeleteConfirmation = () => {
    this.setState({ deleteProjectModalOpen: false })
  }

  // Handle the keyboard shortcuts.
  _handleKeyboardShortcuts = e => {
    // Generate sheets.
    if (shortcuts.generateSheets.func(e)) {
      if (this.props.projectId) {
        this.props.generateSheets(this.props.projectId, this.props.title, this.props.token)
      }
    // Download WAV audio.
    } else if (shortcuts.generateWAV.func(e)) {
      if (this.props.projectId) {
        this.props.generateWAV(this.props.projectId, this.props.title, this.props.token)
      }
      // Download MP3 audio.
    } else if (shortcuts.generateMP3.func(e)) {
      if (this.props.projectId) {
        this.props.generateMP3(this.props.projectId, this.props.title, this.props.token)
      }
    // Delete project.
    } else if (shortcuts.deleteProject.func(e)) {
      if (this.props.projectId) {
        this.showDeleteConfirmation()
      }
    // Save project.
    } else if (shortcuts.saveProject.func(e)) {
      if (this.props.projectId || this.props.lines.length > 0) {
        this.props.saveProject(this.props.projectId, this.props.title, this.props.bpm, this.props.timeSignature, this.props.lines, this.props.instrumentOrder, this.props.instrumentIcons, this.props.showGrid, this.props.token)
      }
    // New project.
    } else if (shortcuts.newProject.func(e)) {
      this.props.newProject()
    // Add a new line.
    } else if (shortcuts.addLine.func(e)) {
      if (!_.isEmpty(this.props.lines)) {
        this.props.addLine()
      }
    // Toggle grid.
    } else if (shortcuts.toggleGrid.func(e)) {
      if (!_.isEmpty(this.props.lines)) {
        this.props.toggleGrid()
      }
    // Undo.
    } else if (shortcuts.undo.func(e)) {
      if (!(this.props.actionHistory.length + this.props.historyPositionDelay - 1 <= 0)) {
        this.props.undo()
      }
    // Redo.
    } else if (shortcuts.redo.func(e)) {
      if (this.props.historyPositionDelay < 0) {
        this.props.redo()
      }
    // Expand all lines.
    } else if (shortcuts.expandAll.func(e)) {
      if (!_.isEmpty(this.props.lines)) {
        this.props.expandAll()
      }
    // Collapse all lines.
    } else if (shortcuts.collapseAll.func(e)) {
      if (!_.isEmpty(this.props.lines)) {
        this.props.collapseAll()
      }
    }
  }

  // Class method, inherited from Component.
  render () {
    // Init the PDF download modal.
    let downloadSheetsModal = (
      <DownloadProjectModal
        modalOpen={!!this.props.pdfDownloadLink}
        pdfDownloadLink={this.props.pdfDownloadLink}
        downloadSheets={this.props.downloadSheets}
        title={this.props.title}
      />
    )

    // Init the WAV download modal.
    let downloadWavModal = (
      <DownloadWavModal
        modalOpen={!!this.props.wavDownloadLink}
        wavDownloadLink={this.props.wavDownloadLink}
        downloadWAV={this.props.downloadWAV}
        title={this.props.title}
      />
    )

    // Init the MP3 download modal.
    let downloadMp3Modal = (
      <DownloadMp3Modal
        modalOpen={!!this.props.mp3DownloadLink}
        mp3DownloadLink={this.props.mp3DownloadLink}
        downloadMP3={this.props.downloadMP3}
        title={this.props.title}
      />
    )

    // Init the delete confirmation modal.
    let deleteConfirmationModal = (
      <DeleteProjectModal
        modalOpen={this.state.deleteProjectModalOpen}
        hideModal={this.hideDeleteConfirmation}
        title={this.props.title}
        token={this.props.token}
        projectId={this.props.projectId}
        deleteProject={this.props.deleteProject}
      />
    )

    // Init some booleans, to avoid redundancy.
    let emptyLines = _.isEmpty(this.props.lines)

    // Get the menu sections.
    let menuSections = {
      'file': [
        {
          label: 'New',
          icon: faFile,
          shortcut: shortcuts.newProject.keys,
          clicked: this.props.newProject
        },
        {
          label: 'Open',
          icon: faFolderOpen,
          disabled: _.isEmpty(this.props.projects),
          parentKey: 'projectList',
          submenu: this.props.projects.map(p => {
            return {
              label: p.title,
              clicked: () => this.props.loadProject(p._id)
            }
          })
        },
        {
          label: 'Save',
          icon: faSave,
          shortcut: shortcuts.saveProject.keys,
          disabled: emptyLines,
          clicked: () => this.props.saveProject(this.props.projectId, this.props.title, this.props.bpm, this.props.timeSignature, this.props.lines, this.props.instrumentOrder, this.props.instrumentIcons, this.props.showGrid, this.props.token)
        },
        {
          label: 'Download PDF',
          icon: faFilePdf,
          shortcut: shortcuts.generateSheets.keys,
          disabled: !this.props.projectId,
          clicked: () => this.props.generateSheets(this.props.projectId, this.props.title, this.props.token)
        },
        {
          label: 'Download WAV',
          icon: faFileAudio,
          shortcut: shortcuts.generateWAV.keys,
          disabled: !this.props.projectId,
          clicked: () => this.props.generateWAV(this.props.projectId, this.props.title, this.props.token)
        },
        {
          label: 'Download MP3',
          icon: faFileAudio,
          shortcut: shortcuts.generateMP3.keys,
          disabled: !this.props.projectId,
          clicked: () => this.props.generateMP3(this.props.projectId, this.props.title, this.props.token)
        },
        {
          label: 'Delete',
          icon: faTrash,
          shortcut: shortcuts.deleteProject.keys,
          disabled: !this.props.projectId,
          clicked: this.showDeleteConfirmation
        }
        /* SHARE */
      ],
      'edit': [
        {
          label: 'Undo',
          icon: faUndo,
          shortcut: shortcuts.undo.keys,
          disabled: this.props.actionHistory.length + this.props.historyPositionDelay - 1 <= 0,
          clicked: this.props.undo
        },
        {
          label: 'Redo',
          icon: faRedo,
          shortcut: shortcuts.redo.keys,
          disabled: this.props.historyPositionDelay >= 0,
          clicked: this.props.redo
        },
        {
          label: 'Add empty line',
          icon: faPlus,
          shortcut: shortcuts.addLine.keys,
          disabled: emptyLines,
          clicked: this.props.addLine
        },
        {
          label: 'Toggle grid',
          icon: faTable,
          shortcut: shortcuts.toggleGrid.keys,
          disabled: emptyLines,
          clicked: this.props.toggleGrid
        },
        {
          label: 'Instrument icons',
          icon: faDrum,
          disabled: emptyLines,
          parentKey: 'instrumentIcons',
          submenu: Object.keys(instrumentIconTypes).map(type => {
            return { label: ucFirst(instrumentIconTypes[type]), clicked: () => this.props[instrumentIconTypes[type] + 'Icons']() }
          })
        },
        {
          label: 'Order of instruments',
          icon: faSort,
          disabled: emptyLines,
          parentKey: 'instrumentOrder',
          submenu: Object.keys(instrumentOrderTypes).map(type => {
            return { label: ucFirst(instrumentOrderTypes[type]), clicked: () => this.props[instrumentOrderTypes[type] + 'Order']() }
          })
        },
        {
          label: 'Expand all',
          icon: faSortAmountDown,
          disabled: emptyLines,
          shortcut: shortcuts.expandAll.keys,
          clicked: this.props.expandAll
        },
        {
          label: 'Collapse all',
          icon: faSortAmountUp,
          disabled: emptyLines,
          shortcut: shortcuts.collapseAll.keys,
          clicked: this.props.collapseAll
        }
      ],
      'help': [
        {
          label: 'Documentation',
          icon: faBook,
          disabled: false,
          clicked: () => this.props.history.push('docs')
        }
      ]
    }

    return (
      <div className='GlobalNavbar'>
        {deleteConfirmationModal}
        {downloadSheetsModal}
        {downloadWavModal}
        {downloadMp3Modal}
        <NavbarLogo />
        {
          Object.keys(menuSections).map((section, sectionIdx) => {
            return (
              <Fragment key={sectionIdx}>
                <NavbarButton left clicked={() => (this.state[section + 'MenuOpen'] ? this.closeSection(section) : this.openSection(section))}>
                  {section.toUpperCase()} <FontAwesomeIcon icon={this.state[section + 'MenuOpen'] ? faCaretUp : faCaretDown} />
                </NavbarButton>
                <NavbarDropdown
                  position={sectionIdx}
                  options={menuSections[section]}
                  show={this.state[section + 'MenuOpen']}
                  onClose={() => this.closeSection(section)}
                />
              </Fragment>
            )
          })
        }
        <NavbarButton right clicked={this.props.logout}>
          Log out
        </NavbarButton>
        <NavbarWatermark content={this.props.email} />
      </div>
    )
  }
}

// ============================================================================
// Connect with Redux and export.
// ============================================================================

// State mapping.
const mapStateToProps = state => {
  return {
    instrumentIcons: state.project.instrumentIcons,
    instrumentOrder: state.project.instrumentOrder,
    showGrid: state.project.showGrid,
    token: state.auth.token,
    title: state.project.title,
    bpm: state.project.bpm,
    timeSignature: state.project.timeSignature,
    projectId: state.project.projectId,
    lines: state.project.lines,
    userId: state.auth.userId,
    email: state.auth.email,
    projects: state.project.projects,
    pdfDownloadLink: state.project.pdfDownloadLink,
    wavDownloadLink: state.project.wavDownloadLink,
    mp3DownloadLink: state.project.mp3DownloadLink,
    actionHistory: state.project.actionHistory,
    historyPositionDelay: state.project.historyPositionDelay
  }
}

// Action mapping.
const mapDispatchToProps = dispatch => {
  return {
    saveProject: (projectId, title, bpm, timeSignature, lines, instrumentOrder, instrumentIcons, showGrid, token) => dispatch(actions.saveProject(projectId, title, bpm, timeSignature, lines, instrumentOrder, instrumentIcons, showGrid, token)),
    newProject: () => dispatch(actions.newProject()),
    loadProject: (projectId) => dispatch(actions.loadProject(projectId)),
    fetchProjects: (userId, token) => dispatch(actions.fetchProjects(userId, token)),
    generateSheets: (projectId, title, token) => dispatch(actions.generateSheets(projectId, title, token)),
    downloadSheets: () => dispatch(actions.downloadSheets()),
    generateWAV: (projectId, title, token) => dispatch(actions.generateWAV(projectId, title, token)),
    downloadWAV: () => dispatch(actions.downloadWAV()),
    generateMP3: (projectId, title, token) => dispatch(actions.generateMP3(projectId, title, token)),
    downloadMP3: () => dispatch(actions.downloadMP3()),
    addLine: () => dispatch(actions.addLine()),
    canonicalOrder: () => dispatch(actions.changeInstrumentsOrder(instrumentOrderTypes.canonical)),
    drumsetOrder: () => dispatch(actions.changeInstrumentsOrder(instrumentOrderTypes.drumset)),
    deleteProject: (projectId, title, token) => dispatch(actions.deleteProject(projectId, title, token)),
    canonicalIcons: () => dispatch(actions.changeInstrumentIcons(instrumentIconTypes.canonical)),
    drawingsIcons: () => dispatch(actions.changeInstrumentIcons(instrumentIconTypes.drawings)),
    picturesIcons: () => dispatch(actions.changeInstrumentIcons(instrumentIconTypes.pictures)),
    toggleGrid: () => dispatch(actions.toggleGrid()),
    undo: () => dispatch(actions.undo()),
    redo: () => dispatch(actions.redo()),
    collapseAll: () => dispatch(actions.collapseAll()),
    expandAll: () => dispatch(actions.expandAll()),
    logout: () => {
      dispatch(actions.logout())
      dispatch(actions.newProject())
      dispatch(actions.hideAlert())
    }
  }
}

// Export.
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(GlobalNavbar))
