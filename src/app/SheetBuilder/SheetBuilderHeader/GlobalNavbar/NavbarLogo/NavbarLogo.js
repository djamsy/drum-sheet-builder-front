// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Style.
import './NavbarLogo.css'

// Logo image.
import * as logo from '../../../../../logo.sm.png'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const navbarLogo = (props) => (
  <div className='NavbarLogo'>
    <a href='/'>
      <img src={logo} alt='Drum sheet builder' />
    </a>
  </div>
)

// Export.
export default navbarLogo
