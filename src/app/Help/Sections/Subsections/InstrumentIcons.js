// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'

// Components.
import NavbarButtonPreview from '../../ButtonPreviews/NavbarButtonPreview/NavbarButtonPreview'
import Image from '../../../UI/Image/Image'

// Screenshots.
import iconsDrawingsScreenshot from '../../../../images/Screenshots/icons-drawings.jpg'
import iconsCanonicalScreenshot from '../../../../images/Screenshots/icons-canonical.jpg'
import iconsPcituresScreenshots from '../../../../images/Screenshots/icons-pictures.jpg'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const section = (props) => {
  // Init the depth class.
  let depthClass = null
  if (+props.depth > 1) depthClass = `ml-${+props.depth - 1}0`

  return (
    <Fragment>
      <p className={depthClass}>You can configure the instrument icons of your project by clicking <NavbarButtonPreview>Edit</NavbarButtonPreview> - <NavbarButtonPreview>Instrument icons</NavbarButtonPreview>, on the main navbar. The order type of icons <b>will be saved</b> within the information about your project, and will remain when you re-open the project or download it in PDF.</p>

      <ul className={depthClass}>
        <li><b>Drawings</b>: drawings of the instruments (see section <i>Types of hits</i> to view them all).</li>
        <li><b>Canonical</b>: an approximation to the canonical representation of drums in music sheets (see section <i>Types of hits</i> to view them all).</li>
        <li><b>Pictures</b>: pictures of the icons (see section <i>Types of hits</i> to view them all).</li>
      </ul>

      <table style={{ marginTop: 30, width: '100%' }}>
        <tbody>
          <tr>
            <td>
              <div className='img-container'>
                <Image src={iconsDrawingsScreenshot} alt='Drawing icons (8/8 sample)' />
                <div className='img-legend'>Drawing icons (8/8 sample).</div>
              </div>
            </td>
            <td>
              <div className='img-container'>
                <Image src={iconsCanonicalScreenshot} alt='Canonical icons (8/8 sample)' />
                <div className='img-legend'>Canonical icons (8/8 sample).</div>
              </div>
            </td>
            <td>
              <div className='img-container'>
                <Image src={iconsPcituresScreenshots} alt='Picture icons (8/8 sample)' />
                <div className='img-legend'>Picture icons (8/8 sample).</div>
              </div>
            </td>
          </tr>
        </tbody>
      </table>

      <p className={depthClass}>The default instrument icons are <b>Drawings</b>.</p>
    </Fragment>
  )
}

// Export.
export default section
