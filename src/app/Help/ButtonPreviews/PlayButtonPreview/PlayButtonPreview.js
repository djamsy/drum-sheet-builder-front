// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Style.
import './PlayButtonPreview.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const playButtonPreview = (props) => {
  return (
    <span className='PlayButtonPreview'>
      <button type='button' disabled className={props.active ? 'active' : null}>
        {props.children}
      </button>
    </span>
  )
}

// Export.
export default playButtonPreview
