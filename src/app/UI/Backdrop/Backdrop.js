// ============================================================================
// Dependencies.
// ============================================================================

// Vendor.
import React from 'react'

// Style.
import './Backdrop.css'

// ============================================================================
// Stateless component declaration.
// ============================================================================

const backdrop = (props) => (
  props.show ? <div onClick={props.clicked} className={'Backdrop' + (props.transparent ? ' transparent' : '')} /> : null
)

// Export.
export default backdrop
