// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faInfoCircle,
  faBan,
  faExclamationTriangle,
  faCheck,
  faTimes
} from '@fortawesome/free-solid-svg-icons'

// Style.
import './Alert.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const alert = props => {
  let icon = null
  switch (props.type) {
    case 'Success': icon = faCheck; break
    case 'Danger': icon = faBan; break
    case 'Warning': icon = faExclamationTriangle; break
    case 'Info': icon = faInfoCircle; break
    default:
  }

  return (
    <div className='Alert'>
      <div className={'AlertContent Alert' + (props.type ? props.type : 'Info')}>
        <button className='AlertDismiss' onClick={() => props.hideAlert()}>
          <FontAwesomeIcon icon={faTimes} />
        </button>
        <FontAwesomeIcon icon={icon} /> {props.message}
      </div>
    </div>
  )
}

// Export.
export default alert
