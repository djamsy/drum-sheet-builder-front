// ============================================================================
// Dependencies.
// ============================================================================

// Actions.
import * as actions from '../actions/types'

// Shared.
import { updateObject } from '../../shared/functions'

// ============================================================================
// Initial state.
// ============================================================================

// Initial state.
const initialState = {
  // Alert type.
  type: null,
  // Alert message.
  message: null
}

// ============================================================================
// Functionality.
// ============================================================================

// Show an alert message.
const showAlert = (state, action) => {
  return updateObject(state, {
    type: action.alertType,
    message: action.message
  })
}

// Hide an alert message.
const hideAlert = (state, action) => {
  return updateObject(state, {
    ...initialState
  })
}

// ============================================================================
// Reducer definition.
// ============================================================================

// Reducer definition.
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.SHOW_ALERT:
      return showAlert(state, action)
    case actions.HIDE_ALERT:
      return hideAlert(state, action)

    default:
      return state
  }
}

// Export.
export default reducer
