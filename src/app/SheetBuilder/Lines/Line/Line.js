// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Component, Fragment } from 'react'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import _ from 'lodash'
import {
  faCaretLeft,
  faCaretDown,
  faFillDrip,
  faTimes,
  faSync,
  faCopy,
  faPlus,
  faMinus,
  faCaretUp,
  faHeading
} from '@fortawesome/free-solid-svg-icons'

// Style.
import './Line.css'

// Components.
import Bar from './Bar/Bar'
import ColorMenu from './ColorMenu/ColorMenu'
import Legend from './Bar/Legend/Legend'
import LineTools from './LineTools/LineTools'
import LinePlayer from './LinePlayer/LinePlayer'
import IconButton from '../../../UI/IconButton/IconButton'
import InstrumentTools from './Bar/InstrumentTools/InstrumentTools'

// Shared.
import {
  getButtonsColor,
  makeDarker,
  calculateHitDepth,
  compressLine
} from '../../../../shared/functions'
import {
  shortcuts,
  lineTypes
} from '../../../../shared/constants'
import log from '../../../../shared/logger'
import Player from '../../../../shared/player'

// Redux actions.
import * as actions from '../../../../store/actions/index'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateful component declaration.
class Line extends Component {
  state = {
    hitTypeEditingMode: false,
    bgColorMenuOpen: false
  }

  constructor (props) {
    super(props)
    // Assign line ref.
    this.lineRef = null
  }

  // Class method, inherited from Component.
  shouldComponentUpdate (nextProps, nextState) {
    let shouldUpdate = !_.isEqual(nextProps.line.content, this.props.line.content) ||
      nextProps.line.bgColor !== this.props.line.bgColor ||
      this.state.hitTypeEditingMode !== nextState.hitTypeEditingMode ||
      this.props.lineNo !== nextProps.lineNo ||
      nextProps.instrumentIcons !== this.props.instrumentIcons ||
      nextProps.instrumentOrder !== this.props.instrumentOrder ||
      nextProps.instrumentSize !== this.props.instrumentSize ||
      nextProps.line.collapsed !== this.props.line.collapsed ||
      nextState.bgColorMenuOpen !== this.state.bgColorMenuOpen ||
      (
        nextProps.linePlaying === null &&
        this.props.linePLaying !== null
      ) /* when player stopped */ ||
      (
        nextProps.stepPlaying !== this.props.stepPlaying && /* next step playing is different than current playing */
        nextProps.linePlaying === this.props.lineIdx && /* current line playing */
        (
          (!this.props.line.collapsed && !nextProps.line.collapsed) || /* when line not collapsed */
          (!this.props.linePlaying && nextProps.linePlaying === nextProps.lineIdx) || /* when line starts to play */
          (this.props.linePlaying === this.props.lineIdx && !nextProps.linePlaying) /* when line ends play */
        )
      ) ||
      (
        this.props.linePlaying === this.props.lineIdx && /* line was playing previously */
        nextProps.linePlaying !== this.props.linePlaying && /* next line playing is different than current */
        nextProps.line.collapsed && this.props.line.collapsed /* when line is collapsed */
      ) ||
      (
        this.props.linePlaying === this.props.lineIdx &&
        nextProps.linePLaying !== this.props.lineIdx
      ) /* when line is not playing anymore */
    if (shouldUpdate) {
      log(`Re-rendering line ${nextProps.lineIdx}`)
    }
    return shouldUpdate
  }

  // Class method, inherited from Component.
  componentDidMount () {
    // Add keyboard listener for events.
    document.addEventListener('keydown', this._handleKeyboardShortcuts, false)
  }

  // Class method, inherited from Component.
  componentDidUpdate (prevProps) {
    // Scroll to the line that is playing, if the project is playing.
    if (this.props.projectPlaying && this.props.linePlaying === this.props.lineIdx) {
      // TODO: re-enable automatic scrolling when efficienfy is optimized.
      this.scrollToLine()
    }
  }

  // Toggle the hit type editing mode.
  toggleHitTypeEditingMode = () => {
    let newHitTypeEditingMode = !this.state.hitTypeEditingMode
    this.setState({
      hitTypeEditingMode: newHitTypeEditingMode
    })
  }

  // Open the background color menu.
  toggleBgColorMenu = () => {
    this.setState(prevState => {
      return {
        bgColorMenuOpen: !prevState.bgColorMenuOpen
      }
    })
  }

  // Return true if the line should have a darker theme.
  shouldLineBeDarker = () => {
    return this.state.hitTypeEditingMode || (this.props.linePlaying === this.props.lineIdx)
  }

  // Shortcut handling.
  _handleKeyboardShortcuts = (e) => {
    // Only if mouse is over this line.
    if (this.props.lineIdx === this.props.lineHover && this.props.line.type === lineTypes.line) {
      // Collapse line.
      if (shortcuts.collapseLine.func(e)) {
        e.preventDefault()
        this.props.collapseLine(this.props.lineIdx)
      // Expand line.
      } else if (shortcuts.expandLine.func(e)) {
        e.preventDefault()
        this.props.expandLine(this.props.lineIdx)
      // Insert line above.
      } else if (shortcuts.insertLineAbove.func(e)) {
        e.preventDefault()
        this.props.insertLineAbove(this.props.lineIdx)
      // Remove line.
      } else if (shortcuts.removeLine.func(e)) {
        e.preventDefault()
        this.props.removeLine(this.props.lineIdx)
      }
    }
  }

  // Stop playing.
  stop = () => {
    // Stop playing music.
    this.props.stopPlaying()
  }

  // Play function.
  play = () => {
    // Start loading.
    this.props.startLoading()
    // Init player object.
    let player = new Player({
      compressedLine: compressLine(this.props.line.content, this.props.timeSignature),
      audioLib: this.props.audioLib,
      bpm: this.props.bpm,
      timeSignature: this.props.timeSignature,
      onStepPlayed: stepIdx => this.props.playStep(stepIdx, this.hitDepth),
      onStopPlaying: this.stop,
      infinite: this.props.playLoop,
      metronome: this.props.metronome
    })
    // Play and change the state.
    player.play()
      .then(player => {
        // Update the state.
        this.props.startPlayingLine(this.props.lineIdx, player)
        // Calculate the hit depth and assign to class attribute.
        this.hitDepth = calculateHitDepth(this.props.timeSignature)
      })
  }

  // On mouse over line.
  mouseOverLine = () => {
    if (this.props.lineHover !== this.props.lineIdx) {
      this.props.mouseOverLine(this.props.lineIdx)
    }
  }

  // Function to move the window scroll to the line that is currently playing.
  scrollToLine = () => {
    // Get the target position.
    let to = ReactDOM.findDOMNode(this.lineRef).offsetTop - 62 /* menu height (32px) + extra 30px */
    // Go to that position.
    window.scrollTo(0, to)
  }

  // Class method, inherited from Component.
  render () {
    // Get the element.
    let line = this.props.line

    // Get the bars and the beats from the lines array, to keep the order.
    let instrumentNames = Object.keys(line.content[0])

    // Get the color of the buttons depending on the line background color.
    let buttonsColor = getButtonsColor(line.bgColor)
    let buttonsBorderColor = makeDarker(buttonsColor, 1)

    // Init the line style.
    let lineStyle = {
      backgroundColor: (this.shouldLineBeDarker() ? makeDarker(line.bgColor, 0.5) : line.bgColor),
      borderBottomColor: makeDarker(line.bgColor, 1)
    }

    // Init the HTEM button.
    let headerButton = null
    if (!line.collapsed) {
      headerButton = (
        <button
          style={{ backgroundColor: this.state.hitTypeEditingMode ? null : buttonsColor }}
          className={'HitTypeEditingModeButton' + (this.state.hitTypeEditingMode ? ' active' : '')}
          onClick={this.toggleHitTypeEditingMode}
        >
          HTEM {this.state.hitTypeEditingMode ? 'ON' : 'OFF'}
        </button>
      )
    } else {
      headerButton = (
        <button
          className='CollapsedLineButton'
          onClick={() => this.props.expandLine(this.props.lineIdx)}
          style={{ backgroundColor: this.shouldLineBeDarker() ? makeDarker(buttonsColor, 0.5) : buttonsColor }}
        >...</button>
      )
    }

    // Init the line tools content.
    let lineTools = [
      {
        label: 'Reset line',
        labelShort: 'Reset',
        icon: faSync,
        clicked: this.props.resetLine
      },
      {
        label: 'Copy below',
        labelShort: 'Below',
        icon: faCopy,
        clicked: this.props.copyLineBelow
      },
      {
        label: 'Copy at the end',
        labelShort: 'End',
        icon: faCopy,
        clicked: this.props.copyLineAtTheEnd
      },
      {
        label: 'Insert below',
        labelShort: 'Below',
        icon: faPlus,
        clicked: this.props.insertLineBelow
      },
      {
        label: 'Insert above',
        labelShort: 'Above',
        icon: faPlus,
        clicked: this.props.insertLineAbove
      },
      {
        label: 'Add separator above',
        labelShort: 'Above',
        icon: faHeading,
        clicked: this.props.insertSeparatorAbove
      },
      {
        label: 'Remove bar',
        labelShort: 'Bar',
        icon: faMinus,
        clicked: this.props.removeBar
      },
      {
        label: 'Move down',
        labelShort: 'Down',
        icon: faCaretDown,
        clicked: this.props.moveLineDown
      },
      {
        label: 'Move up',
        labelShort: 'Up',
        icon: faCaretUp,
        clicked: this.props.moveLineUp
      }
    ]

    // Init the line content, that depends on the collapsed property.
    let lineContent = null
    if (!line.collapsed) {
      lineContent = (
        <Fragment>
          <LineTools
            lineIdx={this.props.lineIdx}
            elements={lineTools}
            hitTypeEditingMode={this.state.hitTypeEditingMode}
            buttonsColor={buttonsColor}
            buttonsBorderColor={buttonsBorderColor}
          />
          <div className='LineContent'>
            <div className='Pentagram'>
              {
                line.content.map((bar, barIdx) => {
                  // Calculate the bar step range.
                  let barWidth = bar[_.head(Object.keys(bar))].length
                  let stepRangeMin = barIdx * barWidth
                  let stepRangeMax = (barIdx + 1) * barWidth - 1

                  return (
                    <div className='block-sm' key={barIdx}>
                      <div className='BarHeader'>Bar {barIdx + 1}</div>
                      <Legend
                        instrumentSize={this.props.instrumentSize}
                        instruments={instrumentNames}
                        firstElement={barIdx === 0}
                        backgroundColor={buttonsColor}
                        borderColor={buttonsBorderColor}
                      />
                      <Bar
                        hitTypeEditingMode={this.state.hitTypeEditingMode}
                        idx={barIdx}
                        bar={bar}
                        lineIdx={this.props.lineIdx}
                        lastElement={barIdx === line.content.length - 1}
                        barWidth={barWidth}
                        stepRangeMin={stepRangeMin}
                        stepRangeMax={stepRangeMax}
                      />
                      <InstrumentTools
                        instrumentSize={this.props.instrumentSize}
                        lineIdx={this.props.lineIdx}
                        instruments={instrumentNames}
                        lastElement={barIdx === line.content.length - 1}
                        backgroundColor={buttonsColor}
                        borderColor={buttonsBorderColor}
                      />
                    </div>
                  )
                })
              }
            </div>
          </div>
        </Fragment>
      )
    }

    // Init the 'collapse' button.
    let collapseButton = <IconButton icon={faCaretDown} clicked={() => this.props.collapseLine(this.props.lineIdx)} right='10' />
    if (line.collapsed) collapseButton = <IconButton icon={faCaretLeft} clicked={() => this.props.expandLine(this.props.lineIdx)} right='10' />

    // Init the 'change background' button.
    let changeBgButton = <IconButton icon={faFillDrip} clicked={this.toggleBgColorMenu} right='30' />

    // Init the 'remove' button.
    let removeButton = <IconButton icon={faTimes} clicked={() => this.props.removeLine(this.props.lineIdx)} left='10' />

    // Init the background color menu.
    let bgColorMenu = null
    if (this.state.bgColorMenuOpen) {
      bgColorMenu = <ColorMenu changeColor={(color, lineIdx) => {
        this.toggleBgColorMenu()
        this.props.changeBgColor(color, lineIdx)
      }
      } toggleShow={this.toggleBgColorMenu} lineIdx={this.props.lineIdx} />
    }

    return (
      <div className='Line' style={lineStyle} onMouseOver={this.mouseOverLine} ref={ref => { this.lineRef = ref }}>
        {collapseButton}
        {changeBgButton}
        {removeButton}
        {bgColorMenu}
        <div className='LineHeader' style={{ marginBottom: line.collapsed ? null : 10 }}>
          <LinePlayer
            stopPlaying={this.stop}
            lineIdx={this.props.lineIdx}
            projectPlaying={this.props.projectPlaying}
            linePlaying={this.props.linePlaying}
            player={this.props.player}
            play={this.play}
          />
          <span>Line {this.props.lineNo}</span>
          {headerButton}
        </div>
        {lineContent}
      </div>
    )
  }
}

// ============================================================================
// Connect with Redux and export.
// ============================================================================

// State mapping.
const mapStateToProps = state => {
  return {
    bpm: state.project.bpm,
    timeSignature: state.project.timeSignature,
    instrumentOrder: state.project.instrumentOrder,
    instrumentSize: state.project.instrumentSize,
    linePlaying: state.project.linePlaying,
    stepPlaying: state.project.stepPlaying,
    audioLib: state.project.audioLib,
    player: state.project.player,
    playLoop: state.project.playLoop,
    lineHover: state.project.lineHover,
    projectPlaying: state.project.projectPlaying,
    metronome: state.project.metronome
  }
}

// Action mapping.
const mapDispatchToProps = dispatch => {
  return {
    copyLineBelow: (lineIdx) => dispatch(actions.copyLineBelow(lineIdx)),
    copyLineAtTheEnd: (lineIdx) => dispatch(actions.copyLineAtTheEnd(lineIdx)),
    removeLine: (lineIdx) => dispatch(actions.removeLine(lineIdx)),
    resetLine: (lineIdx) => dispatch(actions.resetLine(lineIdx)),
    insertLineBelow: (lineIdx) => dispatch(actions.insertLineBelow(lineIdx)),
    insertLineAbove: (lineIdx) => dispatch(actions.insertLineAbove(lineIdx)),
    insertSeparatorAbove: (lineIdx) => dispatch(actions.insertSeparatorAbove(lineIdx)),
    expandLine: (lineIdx) => dispatch(actions.expandLine(lineIdx)),
    collapseLine: (lineIdx) => dispatch(actions.collapseLine(lineIdx)),
    changeBgColor: (color, lineIdx) => dispatch(actions.changeBgColor(color, lineIdx)),
    removeBar: (lineIdx) => dispatch(actions.removeBar(lineIdx)),
    startPlayingLine: (lineIdx, player) => dispatch(actions.startPlayingLine(lineIdx, player)),
    playStep: (stepIdx, hitDepth) => dispatch(actions.playStep(stepIdx, hitDepth)),
    stopPlaying: () => dispatch(actions.stopPlaying()),
    mouseOverLine: (lineIdx) => dispatch(actions.mouseOverLine(lineIdx)),
    moveLineUp: (lineIdx) => dispatch(actions.moveLineUp(lineIdx)),
    moveLineDown: (lineIdx) => dispatch(actions.moveLineDown(lineIdx)),
    startLoading: () => dispatch(() => actions.startLoading())
  }
}

// Export.
export default connect(mapStateToProps, mapDispatchToProps)(Line)
