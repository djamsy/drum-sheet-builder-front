// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import _ from 'lodash'

// Shared.
import {
  instruments,
  metronome,
  errorMessages,
  maxLineWidth,
  maxInstrumentSize,
  defaultSeparatorBgColor,
  defaultLineBgColor,
  lineTypes,
  lineBgColors,
  timeSignatures,
  stepSizes
} from './constants'

// =============================================================================
// Functions.
// =============================================================================

// Get a full unreferenced copy of the state lines object.
export const copyStateLines = (lines, order) => {
  // Init the full lines array.
  let newLines = []
  // For every line.
  for (let i = 0; i < lines.length; i++) {
    // Init the new line.
    let newLine
    // If it's a separator.
    if (lines[i].type === lineTypes.separator) {
      // Copy the exact content.
      newLine = {
        ...lines[i]
      }
    } else {
      // If object, it means it is music data.
      newLine = {
        ...lines[i],
        content: copyStateLineContent(lines[i].content, order)
      }
    }
    // Push the line to the full lines array.
    newLines.push(newLine)
  }
  // Return the full lines array.
  return newLines
}

// Get a full unreferenced copy of a state line from the lines attribute.
export const copyStateLineContent = (lineContent, order) => {
  if (typeof lineContent === 'string') return lineContent
  // Init the new line.
  let newLineContent = []
  // For every bar on the line.
  for (let j = 0; j < lineContent.length; j++) {
    // Init the new bar object.
    let newBar = {}
    // For every instrument.
    for (let instr in lineContent[j]) {
      // Create attribute for that instrument in the bar.
      if (lineContent[j].hasOwnProperty(instr)) {
        newBar[instr] = [...lineContent[j][instr]]
      }
    }
    // Push the bar to the line.
    newLineContent.push(orderInstruments(order, newBar))
  }
  // Return the line.
  return newLineContent
}

// Order the instruments of a line.
export const orderInstruments = (orderType, bar) => {
  // Get the ordered instrument labels.
  let orderedInstruments = _.orderBy(instruments, [`${orderType}Order`], ['asc'])
  // Init result.
  let r = {}
  // Create objects of .
  for (let i in orderedInstruments) {
    if (orderedInstruments.hasOwnProperty(i)) {
      let label = orderedInstruments[i].label
      // Push to result.
      if (bar[label]) {
        r[label] = bar[label]
      }
    }
  }
  // Return.
  return r
}

// Function that builds a new state-like line array, from the bars and beats configured in the state object.
export const buildLine = (bars, beats, order) => {
  // Init the line array.
  let content = []
  // For every bar.
  for (let b = 0; b < bars; b++) {
    // Init the bar object.
    let bar = {}
    // For every instrument name.
    for (let i in instruments) {
      let instrument = instruments[i].label
      // Init the array for this instrument and fill with '0'.
      bar[instrument] = _.fill(Array(beats), 0)
    }
    // Push the built bar to the whole line.
    content.push(orderInstruments(order, bar))
  }
  // Return the line.
  return buildStateLine(lineTypes.line, defaultLineBgColor, content)
}

// Function that builds a new line-like object, for the metronome.
export const buildMetronomeContent = (timeSignature) => {
  // Init the content object.
  let content = {}
  // Get the separation between high and low notes.
  let separationBetweenLowAndHighNotes = Math.floor(timeSignatures[timeSignature].beats / 2)
  // For every bar.
  for (let b = 0; b < timeSignatures[timeSignature].bars; b++) {
    // For every instrument name.
    for (let i in metronome) {
      let instrument = metronome[i].label
      // Fill with zeros.
      content[instrument] = _.fill(Array(timeSignatures[timeSignature].beats), 0)
      // Fill correctly.
      for (let j in content[instrument]) {
        if (content[instrument].hasOwnProperty(j)) {
          if (instrument === 'high') {
            if (j % separationBetweenLowAndHighNotes === 0) content[instrument][j] = 1
          } else {
            if (j % separationBetweenLowAndHighNotes !== 0) content[instrument][j] = 1
          }
        }
      }
    }
  }
  // Return the content.
  return content
}

// Function that builds a separator.
export const buildSeparator = content => {
  return buildStateLine(lineTypes.separator, defaultSeparatorBgColor, content)
}

// Function that builds a line-type object.
const buildStateLine = (type, bgColor, content) => {
  return {
    type: type,
    bgColor: bgColor,
    content: content,
    collapsed: false
  }
}

// Build an array of alternated values of 1 and 0 every N steps.
export const buildLineEachSteps = (stepSize, beats) => {
  // Init result.
  let r = _.fill(Array(beats), 0)
  // Populate with filled values.
  for (let i = 0; i < beats; i++) {
    if (i % stepSize === 0) {
      r[i] = 1
    }
  }
  // Return.
  return r
}

// Shift all the values from an array to the left or the right.
export const shiftValuesTo = (direction, lineContent, instrument, shiftBeatsSize) => {
  // Get the bar length.
  let barLength = lineContent[0][instrument].length
  // Concatenate values of that instrument from every bar into single array.
  let concatenatedBeats = []
  // For every bar on the line.
  for (let bar in lineContent) {
    if (lineContent.hasOwnProperty(bar)) {
      concatenatedBeats = concatenatedBeats.concat(lineContent[bar][instrument])
    }
  }
  // Shift values as many times as told by shiftBeatsSize.
  for (let i = 1; i <= shiftBeatsSize; i++) {
    // Shift values.
    switch (direction) {
      case 'left':
        // Remove first element of the array and push it to the end.
        let firstElement = concatenatedBeats.shift()
        concatenatedBeats.push(firstElement)
        break
      case 'right':
        // Remove last element of the array and push it to the start.
        let lastElement = concatenatedBeats.pop()
        concatenatedBeats = [lastElement].concat(concatenatedBeats)
        break
      default:
    }
  }
  // Rebuild array.
  for (let bar in lineContent) {
    if (lineContent.hasOwnProperty(bar)) {
      // Take as many elements as the bar length.
      lineContent[bar][instrument] = _.take(concatenatedBeats, barLength)
      // Splice the concatenatedBeats array.
      concatenatedBeats = _.drop(concatenatedBeats, barLength)
    }
  }
  // Return.
  return lineContent
}

// Create a full unreferenced copy of the state project list.
export const copyProjectList = (projects, order) => {
  // Initialize result.
  let newProjects = []
  // For every project.
  for (let i in projects) {
    if (projects.hasOwnProperty(i)) {
      // Get project.
      let p = {
        ...projects[i],
        lines: copyStateLines(projects[i].lines, order)
      }
      // Push value to result array.
      newProjects.push(p)
    }
  }
  // Return.
  return newProjects
}

// Update an object with new one.
export const updateObject = (oldObject, updatedProps) => {
  return {
    ...oldObject,
    ...updatedProps
  }
}

// Validate an e-mail.
export const validateEmail = (email) => {
  return /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)
}

// Validate a password.
export const validatePassword = password => password.length > 5

// Get the error message from an error object/string.
export const getErrorMessage = error => {
  if (typeof error === 'string') return error
  else if (error.response && error.response.data) return error.response.data.message
  else return errorMessages.SERVER_DOWN
}

// Returns true if a number is a power of 2.
export const isPowerOf2 = x => (Math.log(x) / Math.log(2)) % 1 === 0

// Returns true if a number is multiplier of another.
export const isMultiplierOf = (x, n) => n % x === 0

// Returns true if a number is multiplier of 3.
export const isMultiplierOf3 = n => isMultiplierOf(3, n)

// Calculate the width of each instrument of the line to make it spread to the end of the DOM.
export const calculateInstrumentSize = (beats, bars) => {
  // Calculate the reserved space.
  let availableSpace = maxLineWidth -
    35 /* instrument label */ -
    55 /* instrument tools */ -
    bars * beats /* right border of each cell */
  // Substract the available space to the maximum line width and divide by beats * bars.
  let instrumentSize = Math.floor(availableSpace / (bars * beats))
  // Return.
  return Math.min(instrumentSize, maxInstrumentSize)
}

// Check if a number is prime.
export const isPrime = num => {
  for (let i = 2; i < num; i++) {
    if (num % i === 0) return false
  }
  return num !== 1 && num !== 0
}

// Get the maximum prime divisor of a number.
export const getMaxPrimeDivider = beats => {
  // If number is prime, return.
  if (isPrime(beats)) return beats
  // Get the prime dividers.
  let primeDividers = _.times(Math.floor(beats / 2), Number)
    .map(n => n + 1)
    .filter(n => isPrime(n) && beats % n === 0)
  // Return the maximum.
  return (primeDividers.length ? _.max(primeDividers) : beats)
}

// Get the cell left border.
export const getCellBorder = (beatIdx, timeSignature, showGrid) => {
  // Find the maximum prime divisor.
  let maxPrimeDivider = getMaxPrimeDivider(timeSignatures[timeSignature].beats)
  // Change the right border accordingly, depending on beatIdx.
  if (!showGrid || beatIdx === 0 || (timeSignatures[timeSignature].disabledBeats && timeSignatures[timeSignature].disabledBeats.indexOf(beatIdx) !== -1)) {
    return '1px solid #fff'
  } else {
    if (beatIdx % maxPrimeDivider === 0) return '1px solid #ccc'
    else return '1px solid #eee'
  }
}

// Returns true if a whole section is collapsed.
export const isSectionCollapsed = (lines, separatorIdx) => {
  // Return if the line index is greater or equal to the lines length.
  if (lines.length <= +separatorIdx) return false
  // Loop through every line.
  for (let i = +separatorIdx + 1; i < lines.length; i++) {
    // If we arrived to another separator, break the loop.
    if (lines[i].type === lineTypes.separator) break
    // If current line is expanded, return false.
    if (!lines[i].collapsed) return false
  }
  // Return default true.
  return true
}

// Returns true if a whole section is expanded.
export const isSectionExpanded = (lines, separatorIdx) => {
  // Return if the line index is greater or equal to the lines length.
  if (lines.length <= +separatorIdx) return false
  // Loop through every line.
  for (let i = +separatorIdx + 1; i < lines.length; i++) {
    // If we arrived to another separator, break the loop.
    if (lines[i].type === lineTypes.separator) break
    // If current line is collapsed, return false.
    if (lines[i].collapsed) return false
  }
  // Return default true.
  return true
}

// Capitalize first letter of a string.
export const ucFirst = str => str.charAt(0).toUpperCase() + str.slice(1)

// Calculate a slightly darker color.
export const makeDarker = (color, factor = 1) => {
  // Remove the asterisk.
  color = color.replace(/^#/, '')
  // If color has 3 digits, make it have 6.
  if (color.length === 3) color = color.split('').map(letter => letter + letter).join('')
  // Init result.
  let r = '#'
  // Loop to get every RGB component.
  for (let i = 0; i < 6; i = i + 2) {
    // Get the color component.
    let component = color.substr(i, 2)
    // Transform to decimal and substract (32).
    let decimalComponent = parseInt(component, 16) - (factor * 16)
    // If the result is negative, append a double 0 to result.
    if (decimalComponent < 0) {
      r += '00'
    } else {
      // Transform to hexadecimal.
      let hexComponent = decimalComponent.toString(16)
      // Append to result, forcing it to have 2 digits.
      r += (hexComponent.length === 2 ? hexComponent : '0' + hexComponent)
    }
  }
  // Return the composed color.
  return r
}

// Calculate a slightly lighter color.
export const makeLighter = (color, factor = 1) => {
  // Remove the asterisk.
  color = color.replace(/^#/, '')
  // If color has 3 digits, make it have 6.
  if (color.length === 3) color = color.split('').map(letter => letter + letter).join('')
  // Init result.
  let r = '#'
  // Loop to get every RGB component.
  for (let i = 0; i < 6; i = i + 2) {
    // Get the color component.
    let component = color.substr(i, 2)
    // Transform to decimal and substract (32).
    let decimalComponent = parseInt(component, 16) + (factor * 16)
    // If the result is negative, append a double 0 to result.
    if (decimalComponent > 255) {
      decimalComponent = 255
    }
    // Transform to hexadecimal. It will always have 2 digits.
    r += decimalComponent.toString(16)
  }
  // Return the composed color.
  return r
}

// Get the clor of the buttons that corresponds to a line background color.
export const getButtonsColor = color => _.find(lineBgColors, { background: color }).buttons

// Create a copy of the state history.
export const copyStateHistory = (stateHistory, pointer) => {
  // Init result.
  let r = []
  // Loop through every element, until the pointer.
  for (let i = 0; i < Math.min(pointer, stateHistory.length); i++) {
    r.push({ ...stateHistory[i] })
  }
  // Return result.
  return r
}

// Get complete list of instrument icons.
export const getAllInstrumentAssets = type => {
  // Init result.
  let r = []
  // Loop thoruh every instrument, every hit and every icon.
  for (let i in instruments) {
    if (instruments.hasOwnProperty(i)) {
      for (let j in instruments[i].hits) {
        if (instruments[i].hits.hasOwnProperty(j)) {
          for (let k in instruments[i].hits[j][type]) {
            if (instruments[i].hits[j][type].hasOwnProperty(k)) {
              r.push(fetch(instruments[i].hits[j][type][k]))
            }
          }
        }
      }
    }
  }
  // Return.
  return r
}

// Get the instrument labels of a line.
export const getLineInstrumentLabels = line => {
  return _.keys(line[0])
}

// Get instruments as object.
export const getInstrumentsAsObject = (compressedLine, metronomeEnabled) => {
  // Get the type of instruments to load.
  let instrumentsToLoad = instruments
  // If metronome is enabled.
  if (metronomeEnabled) {
    // If only metronome instruments should be loaded.
    if (_.isEmpty(_.keys(compressedLine))) instrumentsToLoad = metronome
    // If normal instruments plus metronome instruments must be loaded.
    else instrumentsToLoad = instruments.concat(metronome)
  }
  // Init the result.
  let r = {}
  // Loop through every instrument.
  for (let i = 0; i < instrumentsToLoad.length; i++) {
    let instrument = instrumentsToLoad[i]
    // Init the instrument attribute.
    r[instrument.label] = instrument.hits
  }
  return r
}

// Calculate the Time Between Beats (TBB).
export const calculateTBB = bpm => 60 / +bpm

// Calculate the hit depth.
export const calculateHitDepth = timeSignature => +timeSignature.split('/').pop()

// Calculate the Duration Of a Beat (DOB), in milliseconds.
export const calculateDOB = (tbb, hitDepth) => (tbb / hitDepth) * 1000 * 2

// Convert line to object of instrument hits.
export const compressLine = (line, timeSignature) => {
  // Calculate the hit depth.
  let hitDepth = calculateHitDepth(timeSignature)
  // Init result.
  let r = []
  // Loop through every bar on the line.
  for (let i in line) {
    if (line.hasOwnProperty(i)) {
      // Loop through every instrument on the bar.
      for (let label in line[i]) {
        if (line[i].hasOwnProperty(label)) {
          // Ensure the instrument has been initialized.
          if (!r[label]) r[label] = []
          // Push to result.
          for (let j = 0; j < line[i][label].length; j = j + stepSizes[hitDepth]) {
            r[label].push(line[i][label][j])
          }
        }
      }
    }
  }
  // Return the compressed line.
  return r
}

// Convert the whole lines array to object of instrument hits.
export const compressLines = (lines, timeSignature) => {
  // Calculate the hit depth.
  let hitDepth = calculateHitDepth(timeSignature)
  // Init result.
  let r = []
  // Loop through every line on the lines array.
  for (let k in lines) {
    if (lines.hasOwnProperty(k)) {
      // If the line is a separator, skip.
      if (lines[k].type !== lineTypes.line) continue
      // Init the line.
      let line = lines[k].content
      // Loop through every bar on the line.
      for (let i in line) {
        if (line.hasOwnProperty(i)) {
          // Loop through every instrument on the bar.
          for (let label in line[i]) {
            if (line[i].hasOwnProperty(label)) {
              // Ensure the instrument has been initialized.
              if (!r[label]) r[label] = []
              // Push to result.
              for (let j = 0; j < line[i][label].length; j = j + stepSizes[hitDepth]) {
                r[label].push(line[i][label][j])
              }
            }
          }
        }
      }
    }
  }
  // Return the compressed line.
  return r
}

// Build an object of step index - line correspondence.
export const getLineIdxFromSteps = (lines, timeSignature) => {
  // Init result.
  let r = {}
  // Init the step index.
  let currentIdx = 0
  // Loop through every line.
  for (let lineIdx = 0; lineIdx < lines.length; lineIdx++) {
    if (lines[lineIdx].type === lineTypes.line) {
      // Compress the line.
      let compressedLine = compressLine(lines[lineIdx].content, timeSignature)
      let compressedLineBdHits = compressedLine[_.head(_.keys(compressedLine))]
      // Loop through every hit.
      for (let realStepIdx = 0; realStepIdx < compressedLineBdHits.length; realStepIdx++) {
        r[currentIdx++] = {
          lineIdx: lineIdx,
          realStepIdx: realStepIdx
        }
      }
    }
  }
  // Return result.
  return r
}

// Get the first line index from the lines array.
export const getFirstLineIdx = lines => {
  // Loop through every line.
  for (let lineIdx = 0; lineIdx < lines.length; lineIdx++) {
    if (lines[lineIdx].type === lineTypes.line) return lineIdx
  }
  return null
}
