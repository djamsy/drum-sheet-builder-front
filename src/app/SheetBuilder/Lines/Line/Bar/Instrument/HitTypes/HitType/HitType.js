// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Constants.
import { maxInstrumentSize } from '../../../../../../../../shared/constants'

// Style.
import './HitType.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const hitType = props => {
  return (
    <button
      type='button'
      onClick={() => props.changeHit(props.hitKey + 1)}
      className={'HitType' + (props.active ? ' active' : '')}
    >
      <div className='HitTypeLabel'>{props.hitName}</div>
      <div className='HitTypeIcon' style={{
        height: maxInstrumentSize,
        width: maxInstrumentSize
      }}>
        <div style={{
          height: maxInstrumentSize,
          width: maxInstrumentSize,
          backgroundImage: `url(${props.icon}`,
          backgroundSize: (maxInstrumentSize - 5)
        }} className='Instrument' />
      </div>
    </button>
  )
}

// Export.
export default hitType
