// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Style.
import './Button.css'

// Components.
import Tooltip from '../Tooltip/Tooltip'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const button = (props) => {
  // Init the button code.
  let buttonCode = (
    <button
      type='button'
      className={'btn btn-' + (props.type ? props.type : 'default') + (props.onlyIcon ? ' icon-btn' : '')}
      onClick={props.clicked}
      disabled={props.disable}
      style={props.width ? { width: props.width } : null}
    >
      {props.children}
    </button>
  )

  // Return the button code as is, or inside a tooltip.
  if (props.help) {
    return (
      <Tooltip content={props.help} placement='bottom'>
        {buttonCode}
      </Tooltip>
    )
  } else {
    return buttonCode
  }
}

// Export.
export default button
