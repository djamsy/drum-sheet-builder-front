// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faHeading,
  faCaretDown,
  faCaretLeft,
  faFillDrip,
  faTimes
} from '@fortawesome/free-solid-svg-icons'

// Components.
import Image from '../../../UI/Image/Image'
import Callout from '../../../UI/Callout/Callout'
import KeyboardKey from '../../../UI/KeyboardKey/KeyboardKey'
import LineToolsButtonPreview from '../../ButtonPreviews/LineToolsButtonPreview/LineToolsButtonPreview'
import SeparatorButtonPreview from '../../ButtonPreviews/SeparatorButtonPreview/SeparatorButtonPreview'

// Screenshots.
import separatorScreenshot from '../../../../images/Screenshots/separator.jpg'

// Constants.
import { shortcuts } from '../../../../shared/constants'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const section = (props) => {
  // Init the depth class.
  let depthClass = null
  if (+props.depth > 1) depthClass = `ml-${+props.depth - 1}0`

  return (
    <Fragment>
      <p className={depthClass}>Separators are special text lines that can be set up on top of each line by clicking the <LineToolsButtonPreview><FontAwesomeIcon icon={faHeading} /> Add separator above</LineToolsButtonPreview> button on top of each line. They act as <b>section containers</b>, and whenever a separator is created, all the lines below are assigned to a new section whose title is the separator title.</p>

      <p className={depthClass}>The buttons at the left and right of a separator let you perform actions over the separator itself or the lines that belong to the section:</p>

      <ul className={depthClass}>
        <li><b>Deleting</b> a separator: You can delete a separator by clicking the delete button (<SeparatorButtonPreview><FontAwesomeIcon icon={faTimes} /></SeparatorButtonPreview>) at the top left of it. When you do so, all the lines below will happen to belong to the section above.</li>
        <li><b>Collapsing/expanding</b> a whole <u>section</u>: You can collapse or expand all the lines under a separator by clicking the arrow buttons at the top right of it (<SeparatorButtonPreview><FontAwesomeIcon icon={faCaretLeft} /></SeparatorButtonPreview> and <SeparatorButtonPreview><FontAwesomeIcon icon={faCaretDown} /></SeparatorButtonPreview>).</li>
        <li><b>Changing the background color</b> of a whole <u>section</u>: You can change the background color of all the lines under a separator by clicking the paint button at the top right of it <SeparatorButtonPreview><FontAwesomeIcon icon={faFillDrip} /></SeparatorButtonPreview>). A color selector will be opened, and you can choose a color from there.</li>
      </ul>

      <div className='img-container'>
        <Image src={separatorScreenshot} alt='Separator (desktop view)' />
        <div className='img-legend'>Separator (desktop view).</div>
      </div>

      <div className={depthClass}>
        <Callout type='Info' label='TIP'>
          You can expand or collapse <b>all</b> the lines of your project by clicking {shortcuts.expandAll.keys.map((key, idx) => <KeyboardKey key={idx} idx={idx}>{key}</KeyboardKey>)} or {shortcuts.collapseAll.keys.map((key, idx) => <KeyboardKey key={idx} idx={idx}>{key}</KeyboardKey>)}, respectively.
        </Callout>
      </div>
    </Fragment>
  )
}

// Export.
export default section
