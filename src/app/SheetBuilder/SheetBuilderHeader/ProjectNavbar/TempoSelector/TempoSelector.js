// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Style.
import './TempoSelector.css'

// Components.
import Label from '../../../../UI/Label/Label'
import Select from '../../../../UI/Select/Select'

// Constants.
import { timeSignatures } from '../../../../../shared/constants'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const tempoSelector = (props) => (
  <div className='TempoSelector'>
    <Label help={props.help}>{props.label}</Label>
    <Select
      width='60'
      changed={event => props.updateTimeSignature(event.target.value)}
      disable={props.disabled}
      value={props.timeSignature}
      options={Object.keys(timeSignatures)}
    />
  </div>
)

// Export.
export default tempoSelector
