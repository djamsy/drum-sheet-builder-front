// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// Components.
import Label from '../Label/Label'

// Style.
import './Input.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const input = (props) => {
  // Init the addon.
  let addon = null
  if (props.addon) {
    addon = (
      <button className={'InputAddon' + (props.addonActive ? ' active' : '')} onClick={props.addonClick}>
        <FontAwesomeIcon icon={props.addon} />
      </button>
    )
  }

  return (
    (
      <Fragment>
        {
          props.label
            ? <Label help={props.help}>{props.label}</Label>
            : null
        }
        <div
          className='Input'
          style={{ width: (props.width ? +props.width + 12 + 'px' : '100%'), marginRight: (props.addon ? 20 : null) }}
        >
          <input
            maxLength={props.maxlength}
            onFocus={props.onFocus}
            onKeyPress={props.keyPress}
            value={props.value}
            placeholder={props.placeholder || 'Type something here'}
            onChange={props.changed}
            type={props.type || 'text'}
            style={{ width: (props.width ? props.width + 'px' : '100%') }}
            disabled={props.disable}
          />
          {addon}
        </div>
      </Fragment>
    )
  )
}

// Export.
export default input
