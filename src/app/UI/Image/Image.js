// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Component } from 'react'

// Components.
import ImageLoader from '../ImageLoader/ImageLoader'

// Style.
import './Image.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
class Image extends Component {
  state = {
    loaded: false
  }

  handleImageLoaded = () => {
    // Update the state.
    this.setState({
      loaded: true
    })
  }

  render () {
    return (
      <div className='ImageContainer'>
        <ImageLoader hidden={this.state.loaded} />
        <img className={this.state.loaded ? '' : 'hidden'} src={this.props.src} alt={this.props.alt} onLoad={this.handleImageLoaded} />
      </div>
    )
  }
}

// Export.
export default Image
