// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'

// Components.
import Image from '../../../UI/Image/Image'
import Callout from '../../../UI/Callout/Callout'
import KeyboardKey from '../../../UI/KeyboardKey/KeyboardKey'
import HTEMButtonPreview from '../../ButtonPreviews/HTEMButtonPreview/HTEMButtonPreview'

// Screenshots.
import hitTypeEditingModeScreenshot from '../../../../images/Screenshots/hit-type-editing-mode.jpg'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const section = (props) => {
  // Init the depth class.
  let depthClass = null
  if (+props.depth > 1) depthClass = `ml-${+props.depth - 1}0`

  return (
    <Fragment>
      <p className={depthClass}>As a drummer, you may know that the type of hit of an instrument is essential in a drum composition. You can edit the hit type of a beat by enabling the <i>Hit Type Editing Mode</i> (from now on, <b>HTEM</b>), which can be toggled by clicking on the buttons at the right of the line number (<HTEMButtonPreview>HTEM OFF</HTEMButtonPreview> and <HTEMButtonPreview active>HTEM ON</HTEMButtonPreview>).</p>

      <div className={depthClass}>
        <Callout type='Info' label='TIP'>
          You can also select the hit type of a beat <b>once</b> by pressing the <KeyboardKey idx={0}>CTRL</KeyboardKey> key when clicking a beat.
        </Callout>
      </div>

      <p className={depthClass}>When <b>HTEM</b> is <b>enabled</b>, you will be shown a simple dropdown when clicking over a beat, where you can select the hit type. You will also see that the line background and its button at the top turn slightly darker, and the HTEM button will change its style and content.</p>

      <div className='img-container'>
        <Image src={hitTypeEditingModeScreenshot} alt='Hit type editing mode' />
        <div className='img-legend'>Hit type editing mode, with icons of type <i>drawings</i> (desktop view).</div>
      </div>

      <p className={depthClass}>To <b>disable HTEM</b>, just click over the button again. Its color will be reset to the original one, and you will now be able to toggle the value of a beat the same way as initially.</p>
    </Fragment>
  )
}

// Export.
export default section
