// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'

// Components.
import KeyboardKey from '../../UI/KeyboardKey/KeyboardKey'
import Image from '../../UI/Image/Image'
import Callout from '../../UI/Callout/Callout'
import NavbarButtonPreview from '../ButtonPreviews/NavbarButtonPreview/NavbarButtonPreview'

// Screenshots.
import downloadWavScreenshot from '../../../images/Screenshots/download-wav.jpg'

// Constants.
import { shortcuts } from '../../../shared/constants'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const section = (props) => {
  // Init the depth class.
  let depthClass = null
  if (+props.depth > 1) depthClass = `ml-${+props.depth - 1}0`

  return (
    <Fragment>
      <p className={depthClass}>You can download the audio version of your project in WAV format by clicking <NavbarButtonPreview>File</NavbarButtonPreview> - <NavbarButtonPreview>Download WAV</NavbarButtonPreview>, on the main navbar. A new modal will be opened, showing you the download link once your audio file has been generated.</p>

      <div className={depthClass}>
        <Callout type='Info' label='TIP'>
          You can also download the audio of your project by clicking {shortcuts.generateWAV.keys.map((key, idx) => <KeyboardKey key={idx} idx={idx}>{key}</KeyboardKey>)} once you have saved your project.
        </Callout>
      </div>

      <div className={depthClass}>
        <Callout type='Warning' label='WARNING'>
          The audio files fil will be generated from the <b>last saved version</b> of your project.
        </Callout>
      </div>

      <div className='img-container' style={{ marginTop: 20 }}>
        <Image src={downloadWavScreenshot} alt='Downloading a project audio in WAV format (desktop view)' />
        <div className='img-legend'>Downloading a project audio in WAV format (desktop view).</div>
      </div>
    </Fragment>
  )
}

// Export.
export default section
