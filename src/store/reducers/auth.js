// ============================================================================
// Dependencies.
// ============================================================================

// Actions.
import * as actions from '../actions/types'

// Utility.
import { updateObject } from '../../shared/functions'

// ============================================================================
// Initial state.
// ============================================================================

// Initial state.
const initialState = {
  token: null,
  userId: null,
  email: null,
  error: null,
  loading: false,
  authRedirectPath: '/',
  cookiePolicyAccepted: true, /* true by default */
  recoveryState: false,
  recoveryToken: null
}

// ============================================================================
// Functionality.
// ============================================================================

// Auth start.
const loginStart = (state) => {
  // Update the state.
  return updateObject(state, {
    error: null,
    loading: true
  })
}

// Auth success.
const loginSuccess = (state, action) => {
  // Update the state.
  return updateObject(state, {
    token: action.token,
    userId: action.userId,
    email: action.email,
    error: null,
    loading: false
  })
}

// Auth fail.
const loginFail = (state, action) => {
  // Update the state.
  return updateObject(state, {
    error: action.error,
    loading: false
  })
}

// Auth logout.
const authLogout = (state) => {
  // Update the state.
  return updateObject(state, {
    token: null,
    userId: null,
    email: null
  })
}

// Change the auth redirect path.
const setLoginRedirectPath = (state, action) => {
  // Update the state.
  return updateObject(state, {
    authRedirectPath: action.path
  })
}

// Reset the login error.
const resetLoginError = (state, action) => {
  // Update the state.
  return updateObject(state, {
    error: null
  })
}

// Accept the cookie policy.
const acceptCookiePolicy = (state, action) => {
  // Update the state.
  return updateObject(state, {
    cookiePolicyAccepted: true
  })
}

// Deny the cookie policy.
const denyCookiePolicy = (state, action) => {
  // Update the state.
  return updateObject(state, {
    cookiePolicyAccepted: false
  })
}

// Password recovery start.
const recoveryStart = (state, action) => {
  // Update the state.
  return updateObject(state, {
    error: null,
    loading: true,
    recoveryState: false
  })
}

// Password recovery success.
const recoverySuccess = (state, action) => {
  // Update the state.
  return updateObject(state, {
    error: null,
    loading: false,
    recoveryState: true
  })
}

// Password recovery fail.
const recoveryFail = (state, action) => {
  // Update the state.
  return updateObject(state, {
    error: action.error,
    loading: false,
    recoveryState: false
  })
}

// Password reset start.
const resetPasswordStart = (state, action) => {
  // Update the state.
  return updateObject(state, {
    error: null,
    loading: true
  })
}

// Password reset success.
const resetPasswordSuccess = (state, action) => {
  // Update the state.
  return updateObject(state, {
    error: null,
    loading: false,
    recoveryState: false
  })
}

// Password reset error.
const resetPasswordFail = (state, action) => {
  // Update the state.
  return updateObject(state, {
    error: action.error,
    loading: false
  })
}

// Set recovery state.
const setRecoveryToken = (state, action) => {
  // Update the state.
  return updateObject(state, {
    error: null,
    loading: false,
    recoveryState: true,
    recoveryToken: action.recoveryToken
  })
}

// Reset the recovery state.
const resetRecoveryState = (state, action) => {
  // Update the state.
  return updateObject(state, {
    error: null,
    loading: false,
    recoveryState: false,
    recoveryToken: null
  })
}

// ============================================================================
// Reducer definition.
// ============================================================================

// Reducer definition.
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.LOGIN_START:
      return loginStart(state, action)
    case actions.LOGIN_SUCCESS:
      return loginSuccess(state, action)
    case actions.LOGIN_FAILED:
      return loginFail(state, action)
    case actions.LOGOUT:
      return authLogout(state, action)
    case actions.SET_LOGIN_REDIRECT_PATH:
      return setLoginRedirectPath(state, action)
    case actions.RESET_LOGIN_ERROR:
      return resetLoginError(state, action)
    case actions.ACCEPT_COOKIE_POLICY:
      return acceptCookiePolicy(state, action)
    case actions.DENY_COOKIE_POLICY:
      return denyCookiePolicy(state, action)
    case actions.RECOVERY_START:
      return recoveryStart(state, action)
    case actions.RECOVERY_SUCCESS:
      return recoverySuccess(state, action)
    case actions.RECOVERY_FAILED:
      return recoveryFail(state, action)
    case actions.RESET_PASSWORD_START:
      return resetPasswordStart(state, action)
    case actions.RESET_PASSWORD_SUCCESS:
      return resetPasswordSuccess(state, action)
    case actions.RESET_PASSWORD_FAILED:
      return resetPasswordFail(state, action)
    case actions.SET_RECOVERY_TOKEN:
      return setRecoveryToken(state, action)
    case actions.RESET_RECOVERY_STATE:
      return resetRecoveryState(state, action)
    default:
      return state
  }
}

export default reducer
