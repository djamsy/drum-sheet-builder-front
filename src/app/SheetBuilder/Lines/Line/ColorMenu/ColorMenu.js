// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'
import _ from 'lodash'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCaretRight } from '@fortawesome/free-solid-svg-icons'

// Style.
import './ColorMenu.css'

// Components.
import Backdrop from '../../../../UI/Backdrop/Backdrop'

// Constants.
import { lineBgColors } from '../../../../../shared/constants'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const colorMenu = props => {
  // Build the color palette.
  let colorPalette = _.values(lineBgColors).map((color, colorIdx) => {
    return (
      <button key={colorIdx} onClick={() => props.changeColor(color.background, props.lineIdx)}>
        <div className='ColorMenuColor' style={{ backgroundColor: color.background }} />
      </button>
    )
  })

  return (
    <Fragment>
      <Backdrop show transparent clicked={props.toggleShow} />
      <div className='ColorMenu'>
        <FontAwesomeIcon icon={faCaretRight} />
        <div className='ColorMenuList'>
          {colorPalette}
        </div>
      </div>
    </Fragment>
  )
}

// Export.
export default colorMenu
