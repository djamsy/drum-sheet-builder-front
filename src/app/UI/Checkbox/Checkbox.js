// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Style.
import './Checkbox.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const checkbox = (props) => {
  // Init the tip.
  let tip = null
  if (props.tip) {
    tip = (
      <small>{props.tip}</small>
    )
  }

  // Default color.
  let color = props.color || 'blue'

  return (
    <label className='CheckboxContainer'>{props.label}{tip}
      <input type='checkbox' checked={props.active} onChange={props.clicked} />
      <span className={`Checkmark checkmark-${color}`} />
    </label>
  )
}

// Export.
export default checkbox
