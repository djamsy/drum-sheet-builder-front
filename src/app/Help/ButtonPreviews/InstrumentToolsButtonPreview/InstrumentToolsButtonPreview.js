// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Style.
import './InstrumentToolsButtonPreview.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const instrumentToolsButtonPreview = (props) => {
  return (
    <span className={'InstrumentToolsButtonPreview' + (props.child ? ' child' : '')}>
      {props.children}
    </span>
  )
}

// Export.
export default instrumentToolsButtonPreview
