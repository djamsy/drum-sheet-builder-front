// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Component } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'

// Style.
import './Lines.css'

// Components.
import Line from './Line/Line'
import Loader from '../../UI/Loader/Loader'
import Separator from './Separator/Separator'
import LinesPlaceholder from './LinesPlaceholder/LinesPlaceholder'

// Constants.
import { lineTypes } from '../../../shared/constants'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateful component declaration.
class Lines extends Component {
  // Class method, inherited from Component.
  shouldComponentUpdate (nextProps) {
    return !_.isEqual(nextProps.lines, this.props.lines) ||
      nextProps.loading !== this.props.loading ||
      nextProps.instrumentOrder !== this.props.instrumentOrder
  }

  // Class method, inherited from Component.
  render () {
    // Init the lines.
    let lines = <LinesPlaceholder />

    // Init the line counter.
    let lineNo = 1

    // Populate the lines.
    if (this.props.lines.length) {
      lines = []
      for (let i = 0; i < this.props.lines.length; i++) {
        if (this.props.lines[i].type === lineTypes.separator) {
          // If string, it's a separator.
          lines.push(<Separator key={i} lineIdx={i} />)
        } else {
          // If object, it's a normal line.
          lines.push(<Line key={i} lineIdx={i} lineNo={lineNo++} line={this.props.lines[i]} />)
        }
      }
    }

    // Init the loader.
    let loader = null
    if (this.props.loading) {
      loader = <Loader top='50%' />
    }

    return (
      <div className='Lines'>
        {loader}
        <div className='LinesContainer' style={this.props.loading ? { opacity: '0.3' } : null}>
          {lines}
        </div>
      </div>
    )
  }
}

// ============================================================================
// Connect with Redux and export.
// ============================================================================

// State mapping.
const mapStateToProps = state => {
  return {
    instrumentOrder: state.project.instrumentOrder,
    loading: state.project.loading,
    lines: state.project.lines
  }
}

// Export.
export default connect(mapStateToProps)(Lines)
