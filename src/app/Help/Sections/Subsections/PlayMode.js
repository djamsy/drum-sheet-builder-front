// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'

// Components.
import ProjectNavbarButtonPreview from '../../ButtonPreviews/ProjectNavbarButtonPreview/ProjectNavbarButtonPreview'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const section = (props) => {
  // Init the depth class.
  let depthClass = null
  if (+props.depth > 1) depthClass = `ml-${+props.depth - 1}0`

  return (
    <Fragment>
      <p className={depthClass}>You can toggle the player loop mode by clicking the button with the loop icon on the project navbar (<ProjectNavbarButtonPreview icon='loop' />). When it's enabled, you will see that its color turns darker (<ProjectNavbarButtonPreview icon='loop' active />).</p>

      <p className={depthClass}>When the loop mode is enabled, the player will play lines in loop, until you stop it manually. The loop mode is designed to work only when playing <b>individual lines</b> and not the whole project.</p>

      <p className={depthClass}>The player loop mode will not be saved between sessions.</p>
    </Fragment>
  )
}

// Export.
export default section
