// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'

// Constants.
import KeyboardKey from '../../UI/KeyboardKey/KeyboardKey'
import Image from '../../UI/Image/Image'
import Callout from '../../UI/Callout/Callout'
import NavbarButtonPreview from '../ButtonPreviews/NavbarButtonPreview/NavbarButtonPreview'

// Constants.
import { shortcuts } from '../../../shared/constants'

// Screenshots.
import deleteModalScreenshot from '../../../images/Screenshots/delete-modal.jpg'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const section = (props) => {
  // Init the depth class.
  let depthClass = null
  if (+props.depth > 1) depthClass = `ml-${+props.depth - 1}0`

  return (
    <Fragment>
      <p className={depthClass}>You can delete projects by clicking <NavbarButtonPreview>File</NavbarButtonPreview> - <NavbarButtonPreview>Delete</NavbarButtonPreview>, on the main navbar. A new modal will be opened, showing you a confirmation button.</p>

      <div className={depthClass}>
        <Callout type='Info' label='TIP'>
          You can also delete your projects by clicking {shortcuts.deleteProject.keys.map((key, idx) => <KeyboardKey key={idx} idx={idx}>{key}</KeyboardKey>)} when having a project open.
        </Callout>
      </div>

      <div className='img-container' style={{ marginTop: 20 }}>
        <Image src={deleteModalScreenshot} alt='Deleting a project (desktop view)' />
        <div className='img-legend'>Deleting a project (desktop view).</div>
      </div>
    </Fragment>
  )
}

// Export.
export default section
