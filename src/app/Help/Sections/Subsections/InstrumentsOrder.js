// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'

// Components.
import Image from '../../../UI/Image/Image'
import NavbarButtonPreview from '../../ButtonPreviews/NavbarButtonPreview/NavbarButtonPreview'

// Screenshots.
import drumsetOrderScreenshot from '../../../../images/Screenshots/drumset-order.png'
import canonicalOrderScreenshot from '../../../../images/Screenshots/canonical-order.png'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const section = (props) => {
  // Init the depth class.
  let depthClass = null
  if (+props.depth > 1) depthClass = `ml-${+props.depth - 1}0`

  return (
    <Fragment>
      <p className={depthClass}>You can configure the order of the instruments of your project by clicking <NavbarButtonPreview>Edit</NavbarButtonPreview> - <NavbarButtonPreview>Order of instruments</NavbarButtonPreview>, on the main navbar. The order of the instruments <b>will be saved</b> within the information about your project, and will remain when you re-open the project or download it in PDF.</p>

      <p className={depthClass}>There are two different orders available:</p>

      <ul className={depthClass}>
        <li><b>Drum set</b>: the instruments will appear in the same order than they are located in the drum set, starting from the kick drum and going on a spiral from left to right.</li>
      </ul>

      <div className='img-container'>
        <Image src={drumsetOrderScreenshot} alt='Drum set order' />
        <div className='img-legend'><i>Drum set</i> order type.</div>
      </div>

      <ul className={depthClass}>
        <li><b>Canonical</b>: instruments will appear ordered by frequencies, as in canonical music sheets. The lowest position corresponds to the kick drum, and the highest, to the hi-hat.</li>
      </ul>

      <div className='img-container'>
        <Image src={canonicalOrderScreenshot} alt='Canonical order' />
        <div className='img-legend'><i>Canonical</i> order type.</div>
      </div>

      <p className={depthClass}>The default order of instruments is <b>Drum set</b>.</p>
    </Fragment>
  )
}

// Export.
export default section
