// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Style.
import './SeparatorButtonPreview.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const separatorButtonPreview = (props) => {
  return (
    <span className='SeparatorButtonPreview'>
      {props.children}
    </span>
  )
}

// Export.
export default separatorButtonPreview
