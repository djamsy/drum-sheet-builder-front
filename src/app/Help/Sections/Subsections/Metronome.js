// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'

// Components.
import ProjectNavbarButtonPreview from '../../ButtonPreviews/ProjectNavbarButtonPreview/ProjectNavbarButtonPreview'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const section = (props) => {
  // Init the depth class.
  let depthClass = null
  if (+props.depth > 1) depthClass = `ml-${+props.depth - 1}0`

  return (
    <Fragment>
      <p className={depthClass}>You can toggle the metronome by clicking the button with the metronome icon on the project navbar (<ProjectNavbarButtonPreview icon='metronome' />). When it's enabled, you will see that its color turns darker (<ProjectNavbarButtonPreview icon='metronome' active />).</p>

      <p className={depthClass}>When the metronome is enabled, every time you play a line or the whole project, the metronome will play along with it.</p>

      <p className={depthClass}>The metronome settings will not be saved between sessions.</p>
    </Fragment>
  )
}

// Export.
export default section
