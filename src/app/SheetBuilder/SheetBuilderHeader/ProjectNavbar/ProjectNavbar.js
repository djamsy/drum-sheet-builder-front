// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInfoCircle, faPlay, faStop } from '@fortawesome/free-solid-svg-icons'
import _ from 'lodash'

// Components.
import TempoSelector from './TempoSelector/TempoSelector'
import Input from '../../../UI/Input/Input'
import Button from '../../../UI/Button/Button'
import ProjectPlayer from './ProjectPlayer/ProjectPlayer'
import ProjectNavbarButton from './ProjectNavbarButton/ProjectNavbarButton'

// Style.
import './ProjectNavbar.css'

// Redux actions.
import * as actions from '../../../../store/actions/index'

// Shared.
import {
  defaultMetronomeAudioLib, shortcuts
} from '../../../../shared/constants'
import {
  compressLines,
  getLineIdxFromSteps,
  getFirstLineIdx, calculateHitDepth
} from '../../../../shared/functions'
import Player from '../../../../shared/player'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateful component declaration.
class ProjectNavbar extends Component {
  // Class method, inherited from Component.
  shouldComponentUpdate (nextProps) {
    return nextProps.timeSignature !== this.props.timeSignature ||
      nextProps.title !== this.props.title ||
      nextProps.bpm !== this.props.bpm ||
      // After clicking RESET.
      _.isEmpty(nextProps.lines) ||
      // After clicking START PROJECT.
      (nextProps.lines.length === 1 && this.props.lines.length === 0) ||
      // When project is saved.
      nextProps.projectId !== this.props.projectId ||
      // When metronome is playing.
      nextProps.player !== this.props.player ||
      // When project is playing.
      nextProps.projectPlaying !== this.props.projectPlaying ||
      // When loop mode is toggled.
      nextProps.playLoop !== this.props.playLoop ||
      // When metronome is toggled.
      nextProps.metronome !== this.props.metronome
  }

  // Return true if the 'create' button should be disabled.
  shouldCreateButtonBeDisabled = () => {
    return !this.props.title.length ||
      !this.props.timeSignature ||
      !this.props.bpm
  }

  // Start project on ENTER key pressed.
  _handleKeyPress = (e) => {
    this.props.resetMouseOverLine()
    if (e.key === 'Enter') {
      if (!this.shouldCreateButtonBeDisabled() && _.isEmpty(this.props.lines)) {
        this.startProject()
      }
    }
  }

  // Function to handle the 'project start' event.
  // Protect against wrong values of the BPM input.
  startProject = () => {
    if (this.props.bpm > 0 && this.props.bpm <= 400) return this.props.addLine()
    else return this.props.alertDanger(`Invalid BPM. Please, type a value between 1 and 400.`)
  }

  // Play metronome function.
  playMetronome = () => {
    // Init player object.
    let player = new Player({
      compressedLine: {},
      audioLib: defaultMetronomeAudioLib,
      bpm: this.props.bpm,
      timeSignature: this.props.timeSignature,
      infinite: true,
      metronome: true
    })
    // Play and change the state.
    player.play()
      .then(player => {
        this.props.playMetronome(player)
      })
  }

  // Start playing project.
  playProject = () => {
    // Start loading.
    this.props.startLoading()
    // Init player object.
    let player = new Player({
      compressedLine: compressLines(this.props.lines, this.props.timeSignature),
      audioLib: this.props.audioLib,
      bpm: this.props.bpm,
      timeSignature: this.props.timeSignature,
      onStepPlayed: stepIdx => {
        // Change the line that is playing.
        if (this.props.linePlaying !== this.stepIndexLines[stepIdx].lineIdx) {
          this.props.changeLinePlaying(this.stepIndexLines[stepIdx].lineIdx)
        }
        // Change the step that is playing
        this.props.playStep(this.stepIndexLines[stepIdx].realStepIdx, this.hitDepth)
      },
      onStopPlaying: this.props.stopPlaying,
      infinite: false,
      metronome: this.props.metronome
    })
    // Play and change the state.
    player.play()
      .then(player => {
        // Build object of stepIndexes.
        this.stepIndexLines = getLineIdxFromSteps(this.props.lines, this.props.timeSignature)
        // Calculate hit depth.
        this.hitDepth = calculateHitDepth(this.props.timeSignature)
        // Update the state.
        this.props.startPlayingProject()
        // Start playing line.
        this.props.startPlayingLine(getFirstLineIdx(this.props.lines), player)
      })
  }

  // Handle the keyboard shortcuts.
  _handleKeyboardShortcuts = e => {
    // Generate sheets.
    if (shortcuts.playProject.func(e)) {
      if (this.props.projectId) {
        if (this.props.projectPlaying) this.props.stopPlaying()
        else this.playProject()
      }
    }
  }

  // Class method, inherited from Component.
  componentDidMount () {
    // Add keyboard listener for events.
    document.addEventListener('keydown', this._handleKeyboardShortcuts, false)
  }

  // Class method, inherited from Component.
  render () {
    // Help depending on state.
    let help = (
      <div className='header-help'>
        <FontAwesomeIcon icon={faInfoCircle} /> Please, provide the basic information about your project and then click <i>Start project</i>.
      </div>
    )

    // Buttons depending on state.
    let leftButtons = (
      <div className='header-element'>
        <Button type='success' disable={this.shouldCreateButtonBeDisabled()} clicked={this.startProject}>
          Start project
        </Button>
      </div>
    )

    // Init the play button.
    let rightButtons = null

    // Init the empty lines boolean.
    let emptyLines = _.isEmpty(this.props.lines)

    if (!_.isEmpty(this.props.lines)) {
      help = null
      leftButtons = null
      rightButtons = (
        <div className='header-element buttons-right'>
          <ProjectPlayer
            stopPlaying={this.props.stopPlaying}
            projectPlaying={this.props.projectPlaying}
            player={this.props.player}
            play={this.playProject}
          />
          <ProjectNavbarButton
            icon='metronome'
            active={this.props.metronome}
            disabled={emptyLines}
            onClick={this.props.toggleMetronome}
            help='Enable metronome'
          />
          <ProjectNavbarButton
            icon='loop'
            active={this.props.playLoop}
            disabled={emptyLines}
            onClick={this.props.togglePlayLoop}
            help='Enable loop mode'
          />
        </div>
      )
    }

    return (
      <div className='ProjectNavbar'>
        {help}
        <div className='header-element'>
          <TempoSelector
            updateTimeSignature={this.props.updateTimeSignature}
            timeSignature={this.props.timeSignature}
            disabled={!_.isEmpty(this.props.lines)}
            label='Time signature'
            help='How many beats (pulses) are contained in each measure (bar).'
          />
          <Input
            onFocus={this.props.resetMouseOverLine}
            label='BPM'
            help='Beats per minute.'
            changed={this.props.updateBPM}
            value={this.props.bpm}
            placeholder='-'
            width='22'
            maxlength='3'
            addon={this.props.linePlaying === -1 ? faStop : faPlay}
            addonClick={this.props.linePlaying === -1 ? this.props.stopPlaying : this.playMetronome}
            addonActive={this.props.linePlaying === -1}
          />
        </div>
        <div className='header-element'>
          <Input
            onFocus={this.props.resetMouseOverLine}
            keyPress={this._handleKeyPress}
            label='Title'
            changed={this.props.updateTitle}
            value={this.props.title}
            placeholder='Artist - Title'
            width='220'
          />
        </div>
        {leftButtons}
        {rightButtons}
      </div>
    )
  }
}

// ============================================================================
// Connect with Redux and export.
// ============================================================================

// State mapping.
const mapStateToProps = state => {
  return {
    token: state.auth.token,
    title: state.project.title,
    bpm: state.project.bpm,
    projectId: state.project.projectId,
    audioLib: state.project.audioLib,
    timeSignature: state.project.timeSignature,
    lines: state.project.lines,
    player: state.project.player,
    linePlaying: state.project.linePlaying,
    projectPlaying: state.project.projectPlaying,
    playLoop: state.project.playLoop,
    metronome: state.project.metronome
  }
}

// Action mapping.
const mapDispatchToProps = dispatch => {
  return {
    updateTitle: (event) => dispatch(actions.updateTitle(event.target.value)),
    updateBPM: (event) => dispatch(actions.updateBPM(event.target.value)),
    updateTimeSignature: (timeSignature) => dispatch(actions.updateTimeSignature(timeSignature)),
    addLine: () => dispatch(actions.addLine()),
    alertDanger: (message) => dispatch(actions.alertDanger(message)),
    resetMouseOverLine: () => dispatch(actions.mouseOverLine(null)),
    playMetronome: player => dispatch(actions.playMetronome(player)),
    stopPlaying: () => dispatch(actions.stopPlaying()),
    startPlayingLine: (lineIdx, player) => dispatch(actions.startPlayingLine(lineIdx, player)),
    startPlayingProject: () => dispatch(actions.startPlayingProject()),
    changeLinePlaying: (lineIdx) => dispatch(actions.changeLinePlaying(lineIdx)),
    playStep: (stepIdx, hitDepth) => dispatch(actions.playStep(stepIdx, hitDepth)),
    togglePlayLoop: () => dispatch(actions.togglePlayLoop()),
    toggleMetronome: () => dispatch(actions.toggleMetronome()),
    startLoading: () => dispatch(actions.startLoading()),
    stopLoading: () => dispatch(actions.stopLoading())
  }
}

// Export.
export default connect(mapStateToProps, mapDispatchToProps)(ProjectNavbar)
