// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Component } from 'react'
import { Redirect, withRouter, Link } from 'react-router-dom'
import { connect } from 'react-redux'
import queryString from 'query-string'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope, faTimes, faCheck, faLock } from '@fortawesome/free-solid-svg-icons'

// Components.
import Button from '../UI/Button/Button'
import Loader from '../UI/Loader/Loader'
import FormRow from '../Auth/FormRow/FormRow'

// Redux actions.
import * as actions from '../../store/actions/index'

// Logo image.
import * as logo from '../../logo.sm.png'

// Functions.
import { validateEmail, validatePassword } from '../../shared/functions'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateful component declaration.
class Recovery extends Component {
  state = {
    fields: {
      email: {
        label: 'E-mail',
        icon: faEnvelope,
        type: 'email',
        value: '',
        error: null,
        valid: false,
        required: true,
        touched: false,
        validation: validateEmail,
        errMsg: 'Your e-mail must be a valid e-mail'
      },
      password: {
        label: 'Password',
        icon: faLock,
        type: 'password',
        value: '',
        error: null,
        required: false,
        touched: false,
        validation: validatePassword,
        errMsg: 'Your password must be at least 5 characters long'
      }
    },
    termsAccepted: false
  }

  // Class method, inherited from Component.
  componentDidMount () {
    // Set the recovery token by taking it from the URL.
    this._setRecoveryToken()
  }

  // Return true if the submit button must be disabled.
  shouldSubmitBeDisabled = () => {
    // If the recovery state has just been set or the password has just been reset, return true.
    if ((this.props.recoveryState && !this.props.recoveryToken) || (!this.props.recoveryState && this.props.recoveryToken)) {
      return true
    }
    // Base condition: if valid e-mail.
    let baseCondition = this.state.fields.email.error !== null || !this.state.fields.email.touched
    // If recovery token, also check valid password.
    if (this.props.recoveryToken) {
      baseCondition = baseCondition || this.state.fields.password.error !== null || !this.state.fields.password.touched
    }
    return baseCondition
  }

  // What to do when the value of an input changes.
  valueChangedHandler = (elemKey, newValue) => {
    // Create a copy of the state object.
    let newElement = {
      ...this.state.fields[elemKey]
    }
    // Modify the element value.
    newElement.value = newValue
    // Also, set touched to true.
    newElement.touched = true
    // Check element validity.
    if (newElement.required && !newValue.length && newElement.touched) {
      newElement.error = `Field ${elemKey} is required`
    } else if (newElement.validation && !newElement.validation(newValue)) {
      newElement.error = newElement.errMsg
    } else {
      newElement.error = null
    }
    // Update the state.
    this.setState({
      fields: {
        ...this.state.fields,
        [elemKey]: newElement
      }
    })
    // Reset the login error.
    this.props.resetLoginError()
  }

  // Set the recovery token by parsing the page URL.
  _setRecoveryToken = () => {
    // Get the query parameters.
    let params = queryString.parse(window.location.search)
    // If recovery token found, set it.
    if (params.token && params.token.length && params.email && params.email.length) {
      // Set the recovery token.
      this.props.setRecoveryToken(params.token)
      // Update this state.
      this.setState({
        fields: {
          email: {
            ...this.state.fields.email,
            value: params.email,
            touched: true
          },
          password: {
            ...this.state.fields.password,
            required: true
          }
        }
      })
    } else {
      // Reset the recovery state.
      this.props.resetRecoveryState()
    }
  }

  // On ENTER key pressed.
  _handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      if (!this.shouldSubmitBeDisabled()) {
        this.getSubmitAction()
      }
    }
  }

  // Returns the action to perform on submit.
  getSubmitAction = () => {
    if (this.props.recoveryToken) {
      return this.props.resetPassword(this.state.fields.email.value, this.props.recoveryToken, this.state.fields.password.value)
    } else {
      return this.props.setRecovery(this.state.fields.email.value)
    }
  }

  // Class method, inherited from Component.
  render () {
    // If user is authenticated, redirect to home.
    if (this.props.isAuth) {
      return <Redirect to='/' />
    }

    // Init the component as a loader.
    let recovery = <Loader top='50%' />

    // Init the header message.
    let headerMessage = 'Please, type your e-mail address so that we can send you a recovery link.'
    if (this.props.recoveryState && this.props.recoveryToken) headerMessage = 'You\'re about to reset your password. Please, type your new password below.'

    // If not loading, render normally.
    if (!this.props.loading) {
      recovery = (
        <div className='AuthBox'>
          <div className='AuthBoxHeader'>
            {headerMessage}
          </div>
          {
            Object.keys(this.state.fields).sort().map(elem => {
              if (!this.state.fields[elem].required) return null
              return (
                <FormRow
                  disable={(this.props.recoveryState && !this.props.recoveryToken) || (!this.props.recoveryState && this.props.recoveryToken)}
                  keyPress={this._handleKeyPress}
                  key={elem}
                  validationMsg={this.state.fields[elem].errMsg}
                  showValidationMsg
                  label={this.state.fields[elem].label}
                  type={this.state.fields[elem].type}
                  value={this.state.fields[elem].value}
                  icon={this.state.fields[elem].icon}
                  error={this.state.fields[elem].error}
                  touched={this.state.fields[elem].touched}
                  changed={(event) => this.valueChangedHandler(elem, event.target.value)}
                />
              )
            })
          }
          <div className='AuthBoxLogin'>
            <Button
              type='primary'
              width='100%'
              disable={this.shouldSubmitBeDisabled()}
              clicked={this.getSubmitAction}
            >
              {this.props.recoveryToken ? 'Reset' : 'Recover'} password
            </Button>
            <Button
              type='default'
              width='100%'
              clicked={() => this.props.history.push('login')}>
              Back to login
            </Button>
          </div>
        </div>
      )
    }

    // Init the error.
    let footerMsg = null
    if (this.props.error) {
      footerMsg = (
        <div className='AuthFooter FooterError'>
          <FontAwesomeIcon icon={faTimes} /> {this.props.error}
        </div>
      )
    } else {
      if (this.props.recoveryState && !this.props.recoveryToken) {
        footerMsg = (
          <div className='AuthFooter FooterSuccess'>
            <FontAwesomeIcon icon={faCheck} /> We have sent an e-mail to <b>{this.state.fields.email.value}</b> with a link to restore your password. Check your inbox!
          </div>
        )
      } else if (!this.props.recoveryState && this.props.recoveryToken) {
        footerMsg = (
          <div className='AuthFooter FooterSuccess'>
            <FontAwesomeIcon icon={faCheck} /> Password successfully reset for <b>{this.state.fields.email.value}</b>. You can now <Link to='/login'>sign in</Link> with your new password.
          </div>
        )
      }
    }

    return (
      <div className='AuthContainer'>
        <div className='Auth'>
          <div className='AuthHeader'>
            <div>
              <a href='/'>
                <img src={logo} alt='Drum sheet builder' />
              </a>
            </div>
            <div className='AuthHeaderText'>
              DrumSheet<b>Builder</b>
            </div>
          </div>
          {recovery}
          {footerMsg}
        </div>
      </div>
    )
  }
}

// ============================================================================
// Connect with Redux and export.
// ============================================================================

// State mapping.
const mapStateToProps = state => {
  return {
    error: state.auth.error,
    loading: state.auth.loading,
    isAuth: state.auth.token !== null,
    recoveryState: state.auth.recoveryState,
    recoveryToken: state.auth.recoveryToken
  }
}

// Action mapping.
const mapDispatchToProps = dispatch => {
  return {
    resetLoginError: () => dispatch(actions.resetLoginError()),
    setRecovery: email => dispatch(actions.setRecovery(email)),
    resetPassword: (email, recoveryToken, newPassword) => dispatch(actions.resetPassword(email, recoveryToken, newPassword)),
    setRecoveryToken: recoveryToken => dispatch(actions.setRecoveryToken(recoveryToken)),
    resetRecoveryState: () => dispatch(actions.resetRecoveryState())
  }
}

// Export.
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Recovery))
