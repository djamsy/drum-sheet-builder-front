// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Styles.
import './ImageLoader.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const imageLoader = (props) => (
  <div className='ImageLoader' style={{ display: props.hidden ? 'none' : 'block' }}>
    <div className='ImageLoaderAnimation'><div /><div /><div /></div>
  </div>
)

// Export.
export default imageLoader
