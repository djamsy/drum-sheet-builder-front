// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'
import { faFileAudio, faInfoCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// Components.
import Modal from '../../../../UI/Modal/Modal'
import FileButton from '../../../../UI/FileButton/FileButton'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
export const downloadMp3Modal = props => {
  return (
    <Modal
      title='Downloading MP3'
      show={props.modalOpen}
      modalClosed={props.downloadMP3}
    >
      <p>Your MP3 audio file for project <b>{props.title}</b> has been generated successfully.</p>
      <p>Here's your download link (MP3 format, 192 kbps):</p>
      <FileButton
        type='mp3'
        icon={faFileAudio}
        fileName={`${props.title}.mp3`}
        clicked={() => {
          props.downloadMP3()
          window.open(props.mp3DownloadLink)
        }}
      />
      <p className='ModalTip'>
        <FontAwesomeIcon icon={faInfoCircle} /> Note that the downloaded version will correspond to the <u>last saved version of your project</u>.
      </p>
    </Modal>
  )
}

// Export.
export default downloadMp3Modal
