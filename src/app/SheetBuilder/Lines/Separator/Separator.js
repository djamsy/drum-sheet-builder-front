// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { faTimes, faCaretDown, faCaretLeft, faFillDrip } from '@fortawesome/free-solid-svg-icons'

// Style.
import './Separator.css'

// Components.
import IconButton from '../../../UI/IconButton/IconButton'
import ColorMenu from '../Line/ColorMenu/ColorMenu'

// Redux actions.
import * as actions from '../../../../store/actions'

// Functions.
import { isSectionCollapsed } from '../../../../shared/functions'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateful component declaration.
class Separator extends Component {
  state = {
    bgColorMenuOpen: false
  }

  // Class method, inherited from Component.
  shouldComponentUpdate (nextProps, nextState) {
    return this.props.lines[this.props.lineIdx] !== nextProps.lines[nextProps.lineIdx] ||
      isSectionCollapsed(this.props.lines, this.props.lineIdx) !== isSectionCollapsed(nextProps.lines, nextProps.lineIdx) ||
      nextState.bgColorMenuOpen !== this.state.bgColorMenuOpen
  }

  // Update the reference of the input element.
  _updateInputRef = e => {
    this.nameInput = e
  }

  // Open the backgroudn color menu.
  toggleBgColorMenu = () => {
    this.setState(prevState => {
      return {
        bgColorMenuOpen: !prevState.bgColorMenuOpen
      }
    })
  }

  // Handle key press over separator input.
  _handleKeyPress = e => {
    if (e.key === 'Enter') {
      if (this.nameInput) this.nameInput.blur()
    }
  }

  // Class method, inherited from Component.
  render () {
    // Get the element.
    let separator = this.props.lines[this.props.lineIdx]

    // Init the 'collapse' button.
    let collapseButton = <IconButton icon={faCaretDown} clicked={() => this.props.collapseSection(this.props.lineIdx)} right='10' />
    if (isSectionCollapsed(this.props.lines, this.props.lineIdx)) collapseButton = <IconButton icon={faCaretLeft} clicked={() => this.props.expandSection(this.props.lineIdx)} right='10' />

    // Init the background color menu.
    let bgColorMenu = null
    if (this.state.bgColorMenuOpen) {
      bgColorMenu = <ColorMenu changeColor={(color, lineIdx) => {
        this.toggleBgColorMenu()
        this.props.changeSectionBgColor(lineIdx, color)
      }
      } toggleShow={this.toggleBgColorMenu} lineIdx={this.props.lineIdx} />
    }

    return (
      <div className='Separator' onMouseOver={this.props.resetMouseOverLine}>
        <IconButton icon={faTimes} clicked={() => this.props.removeLine(this.props.lineIdx)} left='10' />
        <IconButton icon={faFillDrip} clicked={this.toggleBgColorMenu} right='30' />
        {collapseButton}
        {bgColorMenu}
        <input
          ref={this._updateInputRef}
          onKeyPress={this._handleKeyPress}
          maxLength='27'
          placeholder='Separator name'
          value={separator.content}
          onFocus={event => event.target.select()}
          onChange={event => this.props.editSeparatorLabel(this.props.lineIdx, event.target.value)}
        />
      </div>
    )
  }
}

// ============================================================================
// Connect with Redux and export.
// ============================================================================

// State mapping.
const mapStateToProps = state => {
  return {
    lines: state.project.lines
  }
}

// Action mapping.
const mapDispatchToProps = dispatch => {
  return {
    removeLine: (lineIdx) => dispatch(actions.removeLine(lineIdx)),
    editSeparatorLabel: (lineIdx, label) => dispatch(actions.editSeparatorLabel(lineIdx, label)),
    collapseSection: lineIdx => dispatch(actions.collapseSection(lineIdx)),
    expandSection: lineIdx => dispatch(actions.expandSection(lineIdx)),
    changeSectionBgColor: (lineIdx, color) => dispatch(actions.changeSectionBgColor(lineIdx, color)),
    resetMouseOverLine: () => dispatch(actions.mouseOverLine(null))
  }
}

// Export.
export default connect(mapStateToProps, mapDispatchToProps)(Separator)
