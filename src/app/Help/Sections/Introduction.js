// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'
import Image from '../../UI/Image/Image'

// Screenshots.
import projectDesktopScreenshot from '../../../images/Screenshots/project-desktop.jpg'
import projectTabletScreenshot from '../../../images/Screenshots/project-tablet.jpg'
import projectMobileScreenshot from '../../../images/Screenshots/project-mobile.jpg'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const section = (props) => {
  // Init the depth class.
  let depthClass = null
  if (+props.depth > 1) depthClass = `ml-${+props.depth - 1}0`

  return (
    <Fragment>
      <p className={depthClass}>DrumSheet<b>Builder</b> was designed for writing and reading music for drummers easily, with no previous music knowledge required. Conceived for low-medium level drummers, it makes music writing and reading <b>fun</b> and <b>fast</b>.</p>

      <p className={depthClass}>We have carefully designed a simple user interface that lets you write your compositions, drum solos, patterns and fills in seconds by providing tools like the following:</p>

      <ul className={depthClass}>
        <li>A live <b>player</b> that lets you listen to your compositions by pieces.</li>
        <li>A <b>responsive design</b> that you can use from within any device with an internet browser.</li>
        <li>Lots of <b>action buttons</b> to duplicate, move, fill, reset, shift or insert music lines or instruments.</li>
        <li>A list of <b>hit types</b> that you can configure for every instrument of the drum set.</li>
        <li>A bunch of <b>UI-friendly</b> tools such as background colors, buttons to collapse the content, sections separators, customizable instrument icons, customizable order of instruments...</li>
        <li>A huge list of easy-to-learn <b>shortcuts</b> that will fasten the music writing process.</li>
      </ul>

      <table style={{ marginTop: 30, width: '100%' }}>
        <tbody>
          <tr>
            <td>
              <div className='img-container'>
                <Image src={projectDesktopScreenshot} alt='Project view (desktop)' />
                <div className='img-legend'>Project preview (desktop).</div>
              </div>
            </td>
            <td>
              <div className='img-container'>
                <Image src={projectMobileScreenshot} alt='Project view (mobile)' />
                <div className='img-legend'>Project preview (mobile).</div>
              </div>
            </td>
            <td>
              <div className='img-container'>
                <Image src={projectTabletScreenshot} alt='Project view (tablet)' />
                <div className='img-legend'>Project preview (tablet).</div>
              </div>
            </td>
          </tr>
        </tbody>
      </table>

      <p className={depthClass}>DrumSheet<b>Builder</b> was also designed for reading music sheets easily, by providing a more graphic way to represent music. It lets you print your projects, so that you can take them with you for practice.</p>
    </Fragment>
  )
}

// Export.
export default section
