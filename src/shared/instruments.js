// =============================================================================
// Import images. Icon type: drawings.
// =============================================================================

// BD images.
import drawingIconBdDefault from '../images/Instruments/BD/drawings/BD-default.png'
import drawingIconBdGhost from '../images/Instruments/BD/drawings/BD-ghost.png'

// CL images.
import drawingIconClEdge from '../images/Instruments/CL/drawings/CL-edge.png'
import drawingIconClBow from '../images/Instruments/CL/drawings/CL-bow.png'
import drawingIconClBell from '../images/Instruments/CL/drawings/CL-bell.png'

// CR images.
import drawingIconCrEdge from '../images/Instruments/CR/drawings/CR-edge.png'
import drawingIconCrBow from '../images/Instruments/CR/drawings/CR-bow.png'
import drawingIconCrBell from '../images/Instruments/CR/drawings/CR-bell.png'

// RI images.
import drawingIconRiEdge from '../images/Instruments/RI/drawings/RI-edge.png'
import drawingIconRiBow from '../images/Instruments/RI/drawings/RI-bow.png'
import drawingIconRiBell from '../images/Instruments/RI/drawings/RI-bell.png'

// HH images.
import drawingIconHhClosedBow from '../images/Instruments/HH/drawings/HH-closed-bow.png'
import drawingIconHhClosedPedal from '../images/Instruments/HH/drawings/HH-closed-pedal.png'
import drawingIconHhClosedBell from '../images/Instruments/HH/drawings/HH-closed-bell.png'
import drawingIconHhOpenBow from '../images/Instruments/HH/drawings/HH-open-bow.png'
import drawingIconHhOpenPedal from '../images/Instruments/HH/drawings/HH-open-pedal.png'
import drawingIconHhOpenBell from '../images/Instruments/HH/drawings/HH-open-bell.png'

// SN images.
import drawingIconSnDefault from '../images/Instruments/SN/drawings/SN-default.png'
import drawingIconSnGhost from '../images/Instruments/SN/drawings/SN-ghost.png'
import drawingIconSnDrag from '../images/Instruments/SN/drawings/SN-drag.png'
import drawingIconSnFlam from '../images/Instruments/SN/drawings/SN-flam.png'
import drawingIconSnRimShot from '../images/Instruments/SN/drawings/SN-rim-shot.png'
import drawingIconSnSideStick from '../images/Instruments/SN/drawings/SN-side-stick.png'

// TB images.
import drawingIconTbDefault from '../images/Instruments/TB/drawings/TB-default.png'
import drawingIconTbFlam from '../images/Instruments/TB/drawings/TB-flam.png'
import drawingIconTbHead from '../images/Instruments/TB/drawings/TB-head.png'
import drawingIconTbRimShot from '../images/Instruments/TB/drawings/TB-rim-shot.png'

// TL images.
import drawingIconTlDefault from '../images/Instruments/TL/drawings/TL-default.png'
import drawingIconTlFlam from '../images/Instruments/TL/drawings/TL-flam.png'
import drawingIconTlHead from '../images/Instruments/TL/drawings/TL-head.png'
import drawingIconTlRimShot from '../images/Instruments/TL/drawings/TL-rim-shot.png'

// TR images.
import drawingIconTrDefault from '../images/Instruments/TR/drawings/TR-default.png'
import drawingIconTrFlam from '../images/Instruments/TR/drawings/TR-flam.png'
import drawingIconTrHead from '../images/Instruments/TR/drawings/TR-head.png'
import drawingIconTrRimShot from '../images/Instruments/TR/drawings/TR-rim-shot.png'

// =============================================================================
// Import images. Icon type: pictures.
// =============================================================================

// BD images.
import pictureIconBdDefault from '../images/Instruments/BD/pictures/BD-default.png'
import pictureIconBdGhost from '../images/Instruments/BD/pictures/BD-ghost.png'

// CL images.
import pictureIconClEdge from '../images/Instruments/CL/pictures/CL-edge.png'
import pictureIconClBow from '../images/Instruments/CL/pictures/CL-bow.png'
import pictureIconClBell from '../images/Instruments/CL/pictures/CL-bell.png'

// CR images.
import pictureIconCrEdge from '../images/Instruments/CR/pictures/CR-edge.png'
import pictureIconCrBow from '../images/Instruments/CR/pictures/CR-bow.png'
import pictureIconCrBell from '../images/Instruments/CR/pictures/CR-bell.png'

// RI images.
import pictureIconRiEdge from '../images/Instruments/RI/pictures/RI-edge.png'
import pictureIconRiBow from '../images/Instruments/RI/pictures/RI-bow.png'
import pictureIconRiBell from '../images/Instruments/RI/pictures/RI-bell.png'

// HH images.
import pictureIconHhClosedBow from '../images/Instruments/HH/pictures/HH-closed-bow.png'
import pictureIconHhClosedPedal from '../images/Instruments/HH/pictures/HH-closed-pedal.png'
import pictureIconHhClosedBell from '../images/Instruments/HH/pictures/HH-closed-bell.png'
import pictureIconHhOpenBow from '../images/Instruments/HH/pictures/HH-open-bow.png'
import pictureIconHhOpenPedal from '../images/Instruments/HH/pictures/HH-open-pedal.png'
import pictureIconHhOpenBell from '../images/Instruments/HH/pictures/HH-open-bell.png'

// SN images.
import pictureIconSnDefault from '../images/Instruments/SN/pictures/SN-default.png'
import pictureIconSnGhost from '../images/Instruments/SN/pictures/SN-ghost.png'
import pictureIconSnDrag from '../images/Instruments/SN/pictures/SN-drag.png'
import pictureIconSnFlam from '../images/Instruments/SN/pictures/SN-flam.png'
import pictureIconSnRimShot from '../images/Instruments/SN/pictures/SN-rim-shot.png'
import pictureIconSnSideStick from '../images/Instruments/SN/pictures/SN-side-stick.png'

// TB images.
import pictureIconTbDefault from '../images/Instruments/TB/pictures/TB-default.png'
import pictureIconTbFlam from '../images/Instruments/TB/pictures/TB-flam.png'
import pictureIconTbHead from '../images/Instruments/TB/pictures/TB-head.png'
import pictureIconTbRimShot from '../images/Instruments/TB/pictures/TB-rim-shot.png'

// TL images.
import pictureIconTlDefault from '../images/Instruments/TL/pictures/TL-default.png'
import pictureIconTlFlam from '../images/Instruments/TL/pictures/TL-flam.png'
import pictureIconTlHead from '../images/Instruments/TL/pictures/TL-head.png'
import pictureIconTlRimShot from '../images/Instruments/TL/pictures/TL-rim-shot.png'

// TR images.
import pictureIconTrDefault from '../images/Instruments/TR/pictures/TR-default.png'
import pictureIconTrFlam from '../images/Instruments/TR/pictures/TR-flam.png'
import pictureIconTrHead from '../images/Instruments/TR/pictures/TR-head.png'
import pictureIconTrRimShot from '../images/Instruments/TR/pictures/TR-rim-shot.png'

// =============================================================================
// Import images. Icon type: canonical.
// =============================================================================

// BD images.
import canonicalIconBdDefault from '../images/Instruments/BD/canonical/BD-default.png'
import canonicalIconBdGhost from '../images/Instruments/BD/canonical/BD-ghost.png'

// CL images.
import canonicalIconClEdge from '../images/Instruments/CL/canonical/CL-edge.png'
import canonicalIconClBow from '../images/Instruments/CL/canonical/CL-bow.png'
import canonicalIconClBell from '../images/Instruments/CL/canonical/CL-bell.png'

// CR images.
import canonicalIconCrEdge from '../images/Instruments/CR/canonical/CR-edge.png'
import canonicalIconCrBow from '../images/Instruments/CR/canonical/CR-bow.png'
import canonicalIconCrBell from '../images/Instruments/CR/canonical/CR-bell.png'

// RI images.
import canonicalIconRiEdge from '../images/Instruments/RI/canonical/RI-edge.png'
import canonicalIconRiBow from '../images/Instruments/RI/canonical/RI-bow.png'
import canonicalIconRiBell from '../images/Instruments/RI/canonical/RI-bell.png'

// HH images.
import canonicalIconHhClosedBow from '../images/Instruments/HH/canonical/HH-closed-bow.png'
import canonicalIconHhClosedPedal from '../images/Instruments/HH/canonical/HH-closed-pedal.png'
import canonicalIconHhClosedBell from '../images/Instruments/HH/canonical/HH-closed-bell.png'
import canonicalIconHhOpenBow from '../images/Instruments/HH/canonical/HH-open-bow.png'
import canonicalIconHhOpenPedal from '../images/Instruments/HH/canonical/HH-open-pedal.png'
import canonicalIconHhOpenBell from '../images/Instruments/HH/canonical/HH-open-bell.png'

// SN images.
import canonicalIconSnDefault from '../images/Instruments/SN/canonical/SN-default.png'
import canonicalIconSnGhost from '../images/Instruments/SN/canonical/SN-ghost.png'
import canonicalIconSnDrag from '../images/Instruments/SN/canonical/SN-drag.png'
import canonicalIconSnFlam from '../images/Instruments/SN/canonical/SN-flam.png'
import canonicalIconSnRimShot from '../images/Instruments/SN/canonical/SN-rim-shot.png'
import canonicalIconSnSideStick from '../images/Instruments/SN/canonical/SN-side-stick.png'

// TB images.
import canonicalIconTbDefault from '../images/Instruments/TB/canonical/TB-default.png'
import canonicalIconTbFlam from '../images/Instruments/TB/canonical/TB-flam.png'
import canonicalIconTbHead from '../images/Instruments/TB/canonical/TB-head.png'
import canonicalIconTbRimShot from '../images/Instruments/TB/canonical/TB-rim-shot.png'

// TL images.
import canonicalIconTlDefault from '../images/Instruments/TL/canonical/TL-default.png'
import canonicalIconTlFlam from '../images/Instruments/TL/canonical/TL-flam.png'
import canonicalIconTlHead from '../images/Instruments/TL/canonical/TL-head.png'
import canonicalIconTlRimShot from '../images/Instruments/TL/canonical/TL-rim-shot.png'

// TR images.
import canonicalIconTrDefault from '../images/Instruments/TR/canonical/TR-default.png'
import canonicalIconTrFlam from '../images/Instruments/TR/canonical/TR-flam.png'
import canonicalIconTrHead from '../images/Instruments/TR/canonical/TR-head.png'
import canonicalIconTrRimShot from '../images/Instruments/TR/canonical/TR-rim-shot.png'

// =============================================================================
// Import audio files. Library: Lo-fi.
// =============================================================================

// BD audio.
import audioLofiBdDefault from '../audio/drumkits/lofi/BD/BD-default.mp3'
import audioLofiBdGhost from '../audio/drumkits/lofi/BD/BD-ghost.mp3'

// CL audio.
import audioLofiClEdge from '../audio/drumkits/lofi/CL/CL-edge.mp3'
import audioLofiClBow from '../audio/drumkits/lofi/CL/CL-bow.mp3'
import audioLofiClBell from '../audio/drumkits/lofi/CL/CL-bell.mp3'

// CR audio.
import audioLofiCrEdge from '../audio/drumkits/lofi/CR/CR-edge.mp3'
import audioLofiCrBow from '../audio/drumkits/lofi/CR/CR-bow.mp3'
import audioLofiCrBell from '../audio/drumkits/lofi/CR/CR-bell.mp3'

// RI audio.
import audioLofiRiEdge from '../audio/drumkits/lofi/RI/RI-edge.mp3'
import audioLofiRiBow from '../audio/drumkits/lofi/RI/RI-bow.mp3'
import audioLofiRiBell from '../audio/drumkits/lofi/RI/RI-bell.mp3'

// HH audio.
import audioLofiHhClosedBow from '../audio/drumkits/lofi/HH/HH-closed-bow.mp3'
import audioLofiHhClosedPedal from '../audio/drumkits/lofi/HH/HH-closed-pedal.mp3'
import audioLofiHhClosedBell from '../audio/drumkits/lofi/HH/HH-closed-bell.mp3'
import audioLofiHhOpenBow from '../audio/drumkits/lofi/HH/HH-open-bow.mp3'
import audioLofiHhOpenPedal from '../audio/drumkits/lofi/HH/HH-open-pedal.mp3'
import audioLofiHhOpenBell from '../audio/drumkits/lofi/HH/HH-open-bell.mp3'

// SN audio.
import audioLofiSnDefault from '../audio/drumkits/lofi/SN/SN-default.mp3'
import audioLofiSnGhost from '../audio/drumkits/lofi/SN/SN-ghost.mp3'
import audioLofiSnFlam from '../audio/drumkits/lofi/SN/SN-flam.mp3'
import audioLofiSnRimShot from '../audio/drumkits/lofi/SN/SN-rim-shot.mp3'
import audioLofiSnSideStick from '../audio/drumkits/lofi/SN/SN-side-stick.mp3'

// TB audio.
import audioLofiTbDefault from '../audio/drumkits/lofi/TB/TB-default.mp3'
import audioLofiTbFlam from '../audio/drumkits/lofi/TB/TB-flam.mp3'
import audioLofiTbHead from '../audio/drumkits/lofi/TB/TB-head.mp3'
import audioLofiTbRimShot from '../audio/drumkits/lofi/TB/TB-rim-shot.mp3'

// TL audio.
import audioLofiTlDefault from '../audio/drumkits/lofi/TL/TL-default.mp3'
import audioLofiTlFlam from '../audio/drumkits/lofi/TL/TL-flam.mp3'
import audioLofiTlHead from '../audio/drumkits/lofi/TL/TL-head.mp3'
import audioLofiTlRimShot from '../audio/drumkits/lofi/TL/TL-rim-shot.mp3'

// TR audio.
import audioLofiTrDefault from '../audio/drumkits/lofi/TR/TR-default.mp3'
import audioLofiTrFlam from '../audio/drumkits/lofi/TR/TR-flam.mp3'
import audioLofiTrHead from '../audio/drumkits/lofi/TR/TR-head.mp3'
import audioLofiTrRimShot from '../audio/drumkits/lofi/TR/TR-rim-shot.mp3'

// =============================================================================
// Import metronome audio files.
// =============================================================================

// Library: vintage.
import metronomeVintageHigh from '../audio/metronome/vintage/high.wav'
import metronomeVintageLow from '../audio/metronome/vintage/low.wav'

// =============================================================================
// Instruments.
// =============================================================================

// Instrument details.
export const instruments = [
  {
    label: 'BD',
    name: 'Kick',
    drumsetOrder: 0,
    canonicalOrder: 8,
    hits: [
      {
        name: 'Default',
        label: 'default',
        cutPrevious: false,
        icons: {
          drawings: drawingIconBdDefault,
          canonical: canonicalIconBdDefault,
          pictures: pictureIconBdDefault
        },
        audio: {
          lofi: audioLofiBdDefault
        }
      },
      {
        name: 'Ghost note',
        label: 'ghost',
        cutPrevious: false,
        icons: {
          drawings: drawingIconBdGhost,
          canonical: canonicalIconBdGhost,
          pictures: pictureIconBdGhost
        },
        audio: {
          lofi: audioLofiBdGhost
        }
      }
    ]
  },
  {
    label: 'CL',
    name: 'Left Crash',
    drumsetOrder: 3,
    canonicalOrder: 0,
    hits: [
      {
        name: 'Edge',
        label: 'edge',
        cutPrevious: false,
        icons: {
          drawings: drawingIconClEdge,
          canonical: canonicalIconClEdge,
          pictures: pictureIconClEdge
        },
        audio: {
          lofi: audioLofiClEdge
        }
      },
      {
        name: 'Bow',
        label: 'bow',
        cutPrevious: false,
        icons: {
          drawings: drawingIconClBow,
          canonical: canonicalIconClBow,
          pictures: pictureIconClBow
        },
        audio: {
          lofi: audioLofiClBow
        }
      },
      {
        name: 'Bell',
        label: 'bell',
        cutPrevious: false,
        icons: {
          drawings: drawingIconClBell,
          canonical: canonicalIconClBell,
          pictures: pictureIconClBell
        },
        audio: {
          lofi: audioLofiClBell
        }
      }
    ]
  },
  {
    label: 'CR',
    name: 'Right Crash',
    drumsetOrder: 6,
    canonicalOrder: 1,
    hits: [
      {
        name: 'Edge',
        label: 'edge',
        cutPrevious: false,
        icons: {
          drawings: drawingIconCrEdge,
          canonical: canonicalIconCrEdge,
          pictures: pictureIconCrEdge
        },
        audio: {
          lofi: audioLofiCrEdge
        }
      },
      {
        name: 'Bow',
        label: 'bow',
        cutPrevious: false,
        icons: {
          drawings: drawingIconCrBow,
          canonical: canonicalIconCrBow,
          pictures: pictureIconCrBow
        },
        audio: {
          lofi: audioLofiCrBow
        }
      },
      {
        name: 'Bell',
        label: 'bell',
        cutPrevious: false,
        icons: {
          drawings: drawingIconCrBell,
          canonical: canonicalIconCrBell,
          pictures: pictureIconCrBell
        },
        audio: {
          lofi: audioLofiCrBell
        }
      }
    ]
  },
  {
    label: 'RI',
    name: 'Ride',
    drumsetOrder: 7,
    canonicalOrder: 2,
    hits: [
      {
        name: 'Edge',
        label: 'edge',
        cutPrevious: false,
        icons: {
          drawings: drawingIconRiEdge,
          canonical: canonicalIconRiEdge,
          pictures: pictureIconRiEdge
        },
        audio: {
          lofi: audioLofiRiEdge
        }
      },
      {
        name: 'Bow',
        label: 'bow',
        cutPrevious: false,
        icons: {
          drawings: drawingIconRiBow,
          canonical: canonicalIconRiBow,
          pictures: pictureIconRiBow
        },
        audio: {
          lofi: audioLofiRiBow
        }
      },
      {
        name: 'Bell',
        label: 'bell',
        cutPrevious: false,
        icons: {
          drawings: drawingIconRiBell,
          canonical: canonicalIconRiBell,
          pictures: pictureIconRiBell
        },
        audio: {
          lofi: audioLofiRiBell
        }
      }
    ]
  },
  {
    label: 'HH',
    name: 'Hi-Hat',
    drumsetOrder: 2,
    canonicalOrder: 3,
    hits: [
      {
        name: 'Bow, closed',
        label: 'closed-bow',
        cutPrevious: true,
        icons: {
          drawings: drawingIconHhClosedBow,
          canonical: canonicalIconHhClosedBow,
          pictures: pictureIconHhClosedBow
        },
        audio: {
          lofi: audioLofiHhClosedBow
        }
      },
      {
        name: 'Bow, open',
        label: 'open-bow',
        cutPrevious: true,
        icons: {
          drawings: drawingIconHhOpenBow,
          canonical: canonicalIconHhOpenBow,
          pictures: pictureIconHhOpenBow
        },
        audio: {
          lofi: audioLofiHhOpenBow
        }
      },
      {
        name: 'Pedal, closed',
        label: 'closed-pedal',
        cutPrevious: true,
        icons: {
          drawings: drawingIconHhClosedPedal,
          canonical: canonicalIconHhClosedPedal,
          pictures: pictureIconHhClosedPedal
        },
        audio: {
          lofi: audioLofiHhClosedPedal
        }
      },
      {
        name: 'Pedal, open',
        label: 'open-pedal',
        cutPrevious: true,
        icons: {
          drawings: drawingIconHhOpenPedal,
          canonical: canonicalIconHhOpenPedal,
          pictures: pictureIconHhOpenPedal
        },
        audio: {
          lofi: audioLofiHhOpenPedal
        }
      },
      {
        name: 'Bell, closed',
        label: 'closed-bell',
        cutPrevious: true,
        icons: {
          drawings: drawingIconHhClosedBell,
          canonical: canonicalIconHhClosedBell,
          pictures: pictureIconHhClosedBell
        },
        audio: {
          lofi: audioLofiHhClosedBell
        }
      },
      {
        name: 'Bell, open',
        label: 'open-bell',
        cutPrevious: true,
        icons: {
          drawings: drawingIconHhOpenBell,
          canonical: canonicalIconHhOpenBell,
          pictures: pictureIconHhOpenBell
        },
        audio: {
          lofi: audioLofiHhOpenBell
        }
      }
    ]
  },
  {
    label: 'SN',
    name: 'Snare',
    drumsetOrder: 1,
    canonicalOrder: 4,
    hits: [
      {
        name: 'Default',
        label: 'default',
        cutPrevious: false,
        icons: {
          drawings: drawingIconSnDefault,
          canonical: canonicalIconSnDefault,
          pictures: pictureIconSnDefault
        },
        audio: {
          lofi: audioLofiSnDefault
        }
      },
      {
        name: 'Ghost note',
        label: 'ghost',
        cutPrevious: false,
        icons: {
          drawings: drawingIconSnGhost,
          canonical: canonicalIconSnGhost,
          pictures: pictureIconSnGhost
        },
        audio: {
          lofi: audioLofiSnGhost
        }
      },
      {
        name: 'Rim shot',
        label: 'rim-shot',
        cutPrevious: false,
        icons: {
          drawings: drawingIconSnRimShot,
          canonical: canonicalIconSnRimShot,
          pictures: pictureIconSnRimShot
        },
        audio: {
          lofi: audioLofiSnRimShot
        }
      },
      {
        name: 'Flam',
        label: 'flam',
        cutPrevious: false,
        icons: {
          drawings: drawingIconSnFlam,
          canonical: canonicalIconSnFlam,
          pictures: pictureIconSnFlam
        },
        audio: {
          lofi: audioLofiSnFlam
        }
      },
      {
        name: 'Drag',
        label: 'drag',
        cutPrevious: false,
        icons: {
          drawings: drawingIconSnDrag,
          canonical: canonicalIconSnDrag,
          pictures: pictureIconSnDrag
        },
        audio: {
          lofi: audioLofiSnGhost
        },
        drag: true
      },
      {
        name: 'Side stick',
        label: 'side-stick',
        cutPrevious: false,
        icons: {
          drawings: drawingIconSnSideStick,
          canonical: canonicalIconSnSideStick,
          pictures: pictureIconSnSideStick
        },
        audio: {
          lofi: audioLofiSnSideStick
        }
      }
    ]
  },
  {
    label: 'TL',
    name: 'Left Tom',
    drumsetOrder: 4,
    canonicalOrder: 5,
    hits: [
      {
        name: 'Default',
        label: 'default',
        cutPrevious: false,
        icons: {
          drawings: drawingIconTlDefault,
          canonical: canonicalIconTlDefault,
          pictures: pictureIconTlDefault
        },
        audio: {
          lofi: audioLofiTlDefault
        }
      },
      {
        name: 'Rim shot',
        label: 'rim-shot',
        cutPrevious: false,
        icons: {
          drawings: drawingIconTlRimShot,
          canonical: canonicalIconTlRimShot,
          pictures: pictureIconTlRimShot
        },
        audio: {
          lofi: audioLofiTlRimShot
        }
      },
      {
        name: 'Flam',
        label: 'flam',
        cutPrevious: false,
        icons: {
          drawings: drawingIconTlFlam,
          canonical: canonicalIconTlFlam,
          pictures: pictureIconTlFlam
        },
        audio: {
          lofi: audioLofiTlFlam
        }
      },
      {
        name: 'Head',
        label: 'head',
        cutPrevious: false,
        icons: {
          drawings: drawingIconTlHead,
          canonical: canonicalIconTlHead,
          pictures: pictureIconTlHead
        },
        audio: {
          lofi: audioLofiTlHead
        }
      }
    ]
  },
  {
    label: 'TR',
    name: 'Right Tom',
    drumsetOrder: 5,
    canonicalOrder: 6,
    hits: [
      {
        name: 'Default',
        label: 'default',
        cutPrevious: false,
        icons: {
          drawings: drawingIconTrDefault,
          canonical: canonicalIconTrDefault,
          pictures: pictureIconTrDefault
        },
        audio: {
          lofi: audioLofiTrDefault
        }
      },
      {
        name: 'Rim shot',
        label: 'rim-shot',
        cutPrevious: false,
        icons: {
          drawings: drawingIconTrRimShot,
          canonical: canonicalIconTrRimShot,
          pictures: pictureIconTrRimShot
        },
        audio: {
          lofi: audioLofiTrRimShot
        }
      },
      {
        name: 'Flam',
        label: 'flam',
        cutPrevious: false,
        icons: {
          drawings: drawingIconTrFlam,
          canonical: canonicalIconTrFlam,
          pictures: pictureIconTrFlam
        },
        audio: {
          lofi: audioLofiTrFlam
        }
      },
      {
        name: 'Head',
        label: 'head',
        cutPrevious: false,
        icons: {
          drawings: drawingIconTrHead,
          canonical: canonicalIconTrHead,
          pictures: pictureIconTrHead
        },
        audio: {
          lofi: audioLofiTrHead
        }
      }
    ]
  },
  {
    label: 'TB',
    name: 'Base Tom',
    drumsetOrder: 8,
    canonicalOrder: 7,
    hits: [
      {
        name: 'Default',
        label: 'default',
        cutPrevious: false,
        icons: {
          drawings: drawingIconTbDefault,
          canonical: canonicalIconTbDefault,
          pictures: pictureIconTbDefault
        },
        audio: {
          lofi: audioLofiTbDefault
        }
      },
      {
        name: 'Rim shot',
        label: 'rim-shot',
        cutPrevious: false,
        icons: {
          drawings: drawingIconTbRimShot,
          canonical: canonicalIconTbRimShot,
          pictures: pictureIconTbRimShot
        },
        audio: {
          lofi: audioLofiTbRimShot
        }
      },
      {
        name: 'Flam',
        label: 'flam',
        cutPrevious: false,
        icons: {
          drawings: drawingIconTbFlam,
          canonical: canonicalIconTbFlam,
          pictures: pictureIconTbFlam
        },
        audio: {
          lofi: audioLofiTbFlam
        }
      },
      {
        name: 'Head',
        label: 'head',
        cutPrevious: false,
        icons: {
          drawings: drawingIconTbHead,
          canonical: canonicalIconTbHead,
          pictures: pictureIconTbHead
        },
        audio: {
          lofi: audioLofiTbHead
        }
      }
    ]
  }
]

// Metronome.
export const metronome = [
  {
    name: 'Hight note',
    label: 'high',
    hits: [
      {
        name: 'Default',
        label: 'default',
        cutPrevious: false,
        audio: {
          vintage: metronomeVintageHigh
        }
      }
    ]
  },
  {
    name: 'Low note',
    label: 'low',
    hits: [
      {
        name: 'Default',
        label: 'default',
        cutPrevious: false,
        audio: {
          vintage: metronomeVintageLow
        }
      }
    ]
  }
]
