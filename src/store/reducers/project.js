// ============================================================================
// Dependencies.
// ============================================================================

// Actions.
import _ from 'lodash'

// Shared.
import {
  updateObject,
  copyStateLines,
  copyStateLineContent,
  buildLine,
  buildLineEachSteps,
  shiftValuesTo,
  copyProjectList,
  calculateInstrumentSize,
  buildSeparator,
  copyStateHistory
} from '../../shared/functions'

// Constants.
import {
  maxInstrumentSize,
  defaultInstrumentOrder,
  defaultInstrumentIcons,
  defaultShowGrid,
  lineTypes,
  maxHistoryLength,
  defaultTimeSignature,
  timeSignatures,
  defaultAudioLib,
  stepSizes
} from '../../shared/constants'

// Custom logger.
import log from '../../shared/logger'

// Action types.
import * as actions from '../actions/types'

// ============================================================================
// Constants.
// ============================================================================

// Project info.
const projectInfo = {
  title: '',
  bpm: 90,
  timeSignature: defaultTimeSignature,
  lines: [],
  instrumentOrder: defaultInstrumentOrder,
  instrumentIcons: defaultInstrumentIcons,
  showGrid: defaultShowGrid
}

// ============================================================================
// Initial state.
// ============================================================================

// Initial state.
const initialState = {
  // Current project details.
  ...projectInfo,
  // ID of the loaded project (if one loaded).
  projectId: null,
  // Complete list of projects, retrieved from Firebase.
  projects: [],
  // Loading state.
  loading: false,
  // Download link of the project sheets (PDF).
  pdfDownloadLink: null,
  // Download links of the project audio (WAV and MP3).
  wavDownloadLink: null,
  mp3DownloadLink: null,
  // Size of the project instruments, in pixels.
  instrumentSize: maxInstrumentSize,
  // History.
  actionHistory: [],
  // History pointer,
  historyPositionDelay: 0,
  // Booleans that tell if the assets have been loaded.
  instrumentImagesLoaded: false,
  instrumentAudiosLoaded: false,
  // Index of the line that is playing.
  // -1 means that the metronome is playing.
  linePlaying: null,
  // Index of the line step that is playing.
  stepPlaying: null,
  // Unique audio player object.
  player: null,
  // Selected audio lib.
  audioLib: defaultAudioLib,
  // Loop mode, for player.
  playLoop: false,
  // Whether the metronome is enabled or not.
  metronome: false,
  // Line with the cursor over it.
  lineHover: null,
  // Whether the whole project is being played or not.
  projectPlaying: false
}

// ============================================================================
// Functionality.
// ============================================================================

// New project.
const newProject = (state, action) => {
  // Get the updated state.
  let updatedState = updateObject(state, {
    ...projectInfo,
    projectId: null
  })
  // Stop the music.
  if (state.player) clearInterval(state.player)
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: [],
    historyPositionDelay: 0,
    linePlaying: null,
    player: null
  })
}

// Fetch projects success.
const fetchProjectsSuccess = (state, action) => {
  // Update the state.
  return updateObject(state, {
    projects: action.projects
  })
}

// Fetch projects failed.
const fetchProjectsFailed = (state, action) => {
  // Do nothing.
  return state
}

// Stop the application being in loading state.
const stopLoading = (state, action) => {
  // Update the state.
  return updateObject(state, {
    loading: false
  })
}

// Set the application in loading state.
const startLoading = (state, action) => {
  // Update the state.
  return updateObject(state, {
    loading: true
  })
}

// Save new project success.
const saveNewProjectSuccess = (state, action) => {
  // Get the previous project list.
  let projectList = copyProjectList(state.projects, state.instrumentOrder)
  // Push to the project lsit.
  projectList.push({
    ...action.savedProject
  })
  // Update the state.
  return updateObject(state, {
    projects: projectList,
    projectId: action.savedProject._id,
    loading: false
  })
}

// Save existing project success.
const saveExistingProjectSuccess = (state, action) => {
  // Get the previous project list.
  let projectList = copyProjectList(state.projects, state.instrumentOrder)
  // Find the project and replace with new one.
  let projectIndex = projectList.findIndex(p => p._id === state.projectId)
  // Replace the project with new one.
  projectList[projectIndex] = {
    ...action.savedProject
  }
  // Update the state.
  return updateObject(state, {
    projects: projectList,
    loading: false
  })
}

// Save project failed.
const saveProjectFailed = (state, action) => {
  // Remove the loading state.
  return updateObject(state, {
    loading: false
  })
}

// Delete project success.
const deleteProjectSuccess = (state, action) => {
  // Get the previous project list.
  let projectList = copyProjectList(state.projects, state.instrumentOrder)
  // Remove the element with the specified ID.
  _.remove(projectList, { _id: action.projectId })
  // Stop the music.
  if (state.player) clearInterval(state.player)
  // Update the state.
  return updateObject(state, {
    projects: projectList,
    loading: false,
    actionHistory: [],
    historyPositionDelay: 0,
    linePlaying: null,
    player: null
  })
}

// Delete project failed.
const deleteProjectFailed = (state, action) => {
  // Remove the loading state.
  return updateObject(state, {
    loading: false
  })
}

// Generate sheets success.
const generateSheetsSuccess = (state, action) => {
  // Stop the music.
  if (state.player) clearInterval(state.player)
  // Remove the loading state.
  return updateObject(state, {
    pdfDownloadLink: action.pdfDownloadLink,
    loading: false,
    linePlaying: null,
    player: null
  })
}

// Generate sheets failed.
const generateSheetsFailed = (state, action) => {
  // Remove the loading state.
  return updateObject(state, {
    loading: false
  })
}
// Download sheets.
const downloadSheets = (state, action) => {
  // Remove the loading state.
  return updateObject(state, {
    pdfDownloadLink: null
  })
}

// Load project.
const loadProject = (state, action) => {
  // Get the project to load.
  let projectToLoad = state.projects.find(p => p._id === action.projectId)
  // Get the lines to load.
  let linesToLoad = copyStateLines(projectToLoad.lines, projectToLoad.instrumentOrder || defaultInstrumentOrder)
  // Get the updated state.
  let updatedState = updateObject(state, {
    title: projectToLoad.title,
    bpm: projectToLoad.bpm,
    timeSignature: projectToLoad.timeSignature,
    lines: linesToLoad,
    projectId: action.projectId,
    loading: false,
    pdfDownloadLink: null,
    wavDownloadLink: null,
    instrumentSize: calculateInstrumentSize(timeSignatures[projectToLoad.timeSignature].beats, timeSignatures[projectToLoad.timeSignature].bars),
    instrumentOrder: projectToLoad.instrumentOrder || defaultInstrumentOrder,
    instrumentIcons: projectToLoad.instrumentIcons || defaultInstrumentIcons,
    showGrid: projectToLoad.showGrid + '' === 'true'
  })
  // Stop the music.
  if (state.player) clearInterval(state.player)
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Load project`, updatedState, true),
    historyPositionDelay: 0,
    linePlaying: null,
    player: null
  })
}

// Insert registry in the history.
const updateHistory = (action, state, forceEmpty = false) => {
  // Default pointer.
  let pointer = state.actionHistory.length + state.historyPositionDelay /* negative number */
  // Copy current state history.
  let newHistory = (forceEmpty ? [] : copyStateHistory(state.actionHistory, pointer))
  // If history length is the maximum, remove first element.
  if (newHistory.length === maxHistoryLength) newHistory.splice(0, 1)
  // Create copy of the state object.
  let stateCopy = {
    ...state,
    lines: copyStateLines(state.lines, state.instrumentOrder)
  }
  // Push new element to history.
  newHistory.push({
    action: action,
    state: {
      title: stateCopy.title,
      bpm: stateCopy.bpm,
      timeSignature: stateCopy.timeSignature,
      lines: stateCopy.lines,
      instrumentOrder: stateCopy.instrumentOrder,
      instrumentIcons: stateCopy.instrumentIcons,
      showGrid: stateCopy.showGrid
    },
    date: new Date()
  })
  // Return the new history.
  return newHistory
}

// Executed when clicking the 'reset' button.
const resetProject = (state, action) => {
  // Get the updated state.
  let updatedState = updateObject(state, {
    lines: initialState.lines
  })
  // Stop the music.
  if (state.player) clearInterval(state.player)
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Reset project`, updatedState),
    historyPositionDelay: 0,
    linePlaying: null,
    player: null
  })
}

// Change the order of the instruments.
const changeInstrumentsOrder = (state, action) => {
  // Get the new lines as a copy from existing ones, to re-order them.
  let newLines = copyStateLines(state.lines, action.order)
  // Get the updated state.
  let updatedState = updateObject(state, {
    instrumentOrder: action.order,
    lines: newLines
  })
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Change order of instruments to ${action.order}`, updatedState),
    historyPositionDelay: 0
  })
}

// Change the instrument images.
const changeInstrumentIcons = (state, action) => {
  // Get the updated state.
  let updatedState = updateObject(state, {
    instrumentIcons: action.icons
  })
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Change instrument icons to ${action.icons}`, updatedState),
    historyPositionDelay: 0
  })
}

// Add a new line to the project.
const addLine = (state, action) => {
  // Build new line and push to rpevious object.
  let newLine = buildLine(timeSignatures[state.timeSignature].bars, timeSignatures[state.timeSignature].beats, state.instrumentOrder)
  // Get the updated state.
  let updatedState = updateObject(state, {
    lines: [
      ...state.lines,
      newLine
    ]
  })
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Add new empty line to the bottom`, updatedState),
    historyPositionDelay: 0
  })
}

// Executed when changing the value of the time signature.
const updateTimeSignature = (state, action) => {
  // Get the updated state.
  let updatedState = updateObject(state, {
    timeSignature: action.timeSignature || null,
    instrumentSize: calculateInstrumentSize(timeSignatures[action.timeSignature].beats, timeSignatures[action.timeSignature].bars)
  })
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Update project time signature`, updatedState),
    historyPositionDelay: 0
  })
}

// Executed when changing the value of the 'title' input.
const updateTitle = (state, action) => {
  // Get the updated state.
  let updatedState = updateObject(state, {
    title: action.title
  })
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Change project title`, updatedState),
    historyPositionDelay: 0
  })
}

// Updated when changing the value of the 'bpm' input.
const updateBPM = (state, action) => {
  if (!action.bpm || !isNaN(action.bpm)) {
    // Get the updated state.
    let updatedState = updateObject(state, {
      bpm: parseInt(action.bpm) || null
    })
    // Stop the music.
    if (state.player) clearInterval(state.player)
    // Update the state.
    return updateObject(updatedState, {
      actionHistory: updateHistory(`Change project bpm to ${action.bpm}`, updatedState),
      historyPositionDelay: 0,
      linePlaying: null,
      player: null
    })
  } else return state
}

// Executed when clicking a beat of any line.
const clickBeat = (state, action) => {
  log(`Setting hit type to default for instrument ${action.instrument} of line ${action.lineIdx} on position ${action.beatIdx} of bar ${action.barIdx}`)
  // Init the copy of the current state lines array.
  let newLines = copyStateLines(state.lines, state.instrumentOrder)
  // Toggle the value of the desired position.
  newLines[action.lineIdx].content[action.barIdx][action.instrument][action.beatIdx] = +!state.lines[action.lineIdx].content[action.barIdx][action.instrument][action.beatIdx]
  // Get the updated state.
  let updatedState = updateObject(state, {
    lines: newLines
  })
  // Stop the music.
  if (state.player) clearInterval(state.player)
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Click ${action.instrument} on line ${action.lineIdx}`, updatedState),
    historyPositionDelay: 0,
    linePlaying: null,
    player: null
  })
}

// Change the type of hit of an instrument.
const setHitType = (state, action) => {
  log(`Setting hit type to ${action.hitType} for instrument ${action.instrument} of line ${action.lineIdx} on position ${action.beatIdx} of bar ${action.barIdx}`)
  // Init the copy of the current state lines array.
  let newLines = copyStateLines(state.lines, state.instrumentOrder)
  // Set the value of the desired position.
  newLines[action.lineIdx].content[action.barIdx][action.instrument][action.beatIdx] = action.hitType
  // Get the updated state.
  let updatedState = updateObject(state, {
    lines: newLines
  })
  // Stop the music.
  if (state.player) clearInterval(state.player)
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Change hit type of instrument ${action.instrument} on line ${action.lineIdx}`, updatedState),
    historyPositionDelay: 0,
    linePlaying: null,
    player: null
  })
}

// Insert a line in a given position of the lines object, and update the
// state afterwards.
const insertLine = (state, lineIdx, line, action) => {
  // Init the copy of the current state lines array.
  let newLines = copyStateLines(state.lines, state.instrumentOrder)
  // Insert element in current position, and shift the old elements one
  // position to the right.
  newLines.splice(lineIdx, 0, line)
  // Get the updated state.
  let updatedState = updateObject(state, {
    lines: newLines
  })
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(action, updatedState),
    historyPositionDelay: 0
  })
}

// Copy a line and insert it after.
const copyLineBelow = (state, action) => {
  log('Copying line %d after it.', action.lineIdx)
  // Get the element at the specified index.
  let lineToCopy = {
    ...state.lines[action.lineIdx],
    content: copyStateLineContent(state.lines[action.lineIdx].content, state.instrumentOrder)
  }
  // Insert element in position next to the element on the specified index,
  // and shift the old elements one position to the right.
  return insertLine(state, action.lineIdx + 1, lineToCopy, `Copy line ${action.lineIdx} below`)
}

// Copy a line at the end of the project.
const copyLineAtTheEnd = (state, action) => {
  log(`Copying line ${action.lineIdx} at the end.`)
  // Get the element at the specified index.
  let lineToCopy = {
    ...state.lines[action.lineIdx],
    content: copyStateLineContent(state.lines[action.lineIdx].content, state.instrumentOrder)
  }
  // Insert element in position next to the element on the specified index,
  // and shift the old elements one position to the right.
  return insertLine(state, state.lines.length, lineToCopy, `Copy line ${action.lineIdx} at the end`)
}

// Insert a new line after a given one.
const insertLineBelow = (state, action) => {
  log(`Inserting empty line after line ${action.lineIdx}.`)
  // Get an empty line.
  let lineToInsert = buildLine(timeSignatures[state.timeSignature].bars, timeSignatures[state.timeSignature].beats, state.instrumentOrder)
  // Insert element in position next to the element on the specified index,
  // and shift the old elements one position to the right.
  return insertLine(state, action.lineIdx + 1, lineToInsert, `Insert empty line after line ${action.lineIdx}`)
}

// Insert a new line before a given one.
const insertLineAbove = (state, action) => {
  log(`Inserting empty line before line ${action.lineIdx}.`)
  // Get an empty line.
  let lineToInsert = buildLine(timeSignatures[state.timeSignature].bars, timeSignatures[state.timeSignature].beats, state.instrumentOrder)
  // Insert element in same position as the element on the specified index,
  // and shift the old elements one position to the right.
  return insertLine(state, action.lineIdx, lineToInsert, `Insert empty line before line ${action.lineIdx}`)
}

// Insert a new separator after a given line.
const insertSeparatorBelow = (state, action) => {
  log(`Inserting empty separator after line ${action.lineIdx}.`)
  // Build the separator.
  let separator = buildSeparator(action.label || 'Section')
  // Insert element in position next to the element on the specified index,
  // and shift the old elements one position to the right.
  return insertLine(state, action.lineIdx + 1, separator, `Insert separator after line ${action.lineIdx}`)
}

// Insert a new separator before a given line.
const insertSeparatorAbove = (state, action) => {
  log(`Inserting empty separator before line ${action.lineIdx}.`)
  // Build the separator.
  let separator = buildSeparator(action.label || 'Section')
  // Insert element in same position as the element on the specified index,
  // and shift the old elements one position to the right.
  return insertLine(state, action.lineIdx, separator, `Insert separator before line ${action.lineIdx}`)
}

// Remove line at a specific index.
const removeLine = (state, action) => {
  log('Removing line %d.', action.lineIdx)
  // Init the copy of the current state lines array.
  let newLines = copyStateLines(state.lines, state.instrumentOrder)
  // Remove element at current position.
  newLines.splice(action.lineIdx, 1)
  // Get the updated state.
  let updatedState = updateObject(state, {
    lines: newLines
  })
  // Stop the music.
  if (state.player) clearInterval(state.player)
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Remove line ${action.lineIdx}`, updatedState),
    historyPositionDelay: 0,
    linePlaying: null,
    player: null
  })
}

// Reset line at a specific index.
const resetLine = (state, action) => {
  log('Resetting line %d.', action.lineIdx)
  // Init the copy of the current state lines array.
  let newLines = copyStateLines(state.lines, state.instrumentOrder)
  // Get an empty line.
  let emptyLine = buildLine(timeSignatures[state.timeSignature].bars, timeSignatures[state.timeSignature].beats, state.instrumentOrder)
  // Replace element at current position.
  newLines[action.lineIdx] = emptyLine
  // Get the updated state.
  let updatedState = updateObject(state, {
    lines: newLines
  })
  // Stop the music.
  if (state.player) clearInterval(state.player)
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Reset line ${action.lineIdx}`, updatedState),
    historyPositionDelay: 0,
    linePlaying: null,
    player: null
  })
}

// Edit the label of a separator.
const editSeparatorLabel = (state, action) => {
  log('Editing separator %d.', action.lineIdx)
  // Init the copy of the current state lines array.
  let newLines = copyStateLines(state.lines, state.instrumentOrder)
  // Change the value of that separator.
  newLines[action.lineIdx] = buildSeparator(action.label)
  // Get the updated state.
  let updatedState = updateObject(state, {
    lines: newLines
  })
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Edit separator ${action.lineIdx}`, updatedState),
    historyPositionDelay: 0
  })
}

// Fill values of a line each N steps.
const fillEachSteps = (state, action) => {
  log('Filling each %d steps instument %s from line %d.', action.stepSize, action.instrument, action.lineIdx)
  // Init the copy of the current state lines array.
  let newLines = copyStateLines(state.lines, state.instrumentOrder)
  // For every bar on the line.
  for (let bar in newLines[action.lineIdx].content) {
    if (newLines[action.lineIdx].content.hasOwnProperty(bar)) {
      // Make the array have alternate values every N steps.
      newLines[action.lineIdx].content[bar][action.instrument] = buildLineEachSteps(action.stepSize, timeSignatures[state.timeSignature].beats)
    }
  }
  // Get the updated state.
  let updatedState = updateObject(state, {
    lines: newLines
  })
  // Stop the music.
  if (state.player) clearInterval(state.player)
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Fill ${action.instrument} on line ${action.lineIdx} every ${action.stepSize} steps`, updatedState),
    historyPositionDelay: 0,
    linePlaying: null,
    player: null
  })
}

// Shift values of a line to the left or to the right.
const shiftSteps = (state, direction, lineIdx, instrument) => {
  log(`Shifting to the ${direction} instument ${instrument} from line ${lineIdx}.`)
  // Init the copy of the current state lines array.
  let newLines = copyStateLines(state.lines, state.instrumentOrder)
  // Shift the values of the array to a specific direction.
  newLines[lineIdx].content = shiftValuesTo(direction, newLines[lineIdx].content, instrument, timeSignatures[state.timeSignature].shiftBeatsSize)
  // Get the updated state.
  let updatedState = updateObject(state, {
    lines: newLines
  })
  // Stop the music.
  if (state.player) clearInterval(state.player)
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Shift ${instrument} on line ${lineIdx} to the ${direction}`, updatedState),
    historyPositionDelay: 0,
    linePlaying: null,
    player: null
  })
}

// Shift values of a line to the left.
const shiftStepsLeft = (state, action) => {
  return shiftSteps(state, 'left', action.lineIdx, action.instrument)
}

// Shift values of a line to the rigth.
const shiftStepsRight = (state, action) => {
  return shiftSteps(state, 'right', action.lineIdx, action.instrument)
}

// Remove an instrument from a line.
const removeInstrument = (state, action) => {
  log(`Removing instrument ${action.instrument} from line ${action.lineIdx}.`)
  // Init the copy of the current state lines array.
  let newLines = copyStateLines(state.lines, state.instrumentOrder)
  for (let bar in newLines[action.lineIdx].content) {
    if (newLines[action.lineIdx].content.hasOwnProperty(bar)) {
      // Remove that instrument.
      delete newLines[action.lineIdx].content[bar][action.instrument]
    }
  }
  // Get the updated state.
  let updatedState = updateObject(state, {
    lines: newLines
  })
  // Stop the music.
  if (state.player) clearInterval(state.player)
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Remove ${action.instrument} from line ${action.lineIdx}`, updatedState),
    historyPositionDelay: 0,
    linePlaying: null,
    player: null
  })
}

// Reset all the beats of a line.
const resetSteps = (state, action) => {
  log(`Resetting instrument ${action.instrument} from line ${action.lineIdx}.`)
  // Init the copy of the current state lines array.
  let newLines = copyStateLines(state.lines, state.instrumentOrder)
  // For every bar on the line.
  for (let bar in newLines[action.lineIdx].content) {
    if (newLines[action.lineIdx].content.hasOwnProperty(bar)) {
      // Make the array have alternate values every N steps.
      newLines[action.lineIdx].content[bar][action.instrument] = _.fill(newLines[action.lineIdx].content[bar][action.instrument], 0)
    }
  }
  // Get the updated state.
  let updatedState = updateObject(state, {
    lines: newLines
  })
  // Stop the music.
  if (state.player) clearInterval(state.player)
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Reset ${action.instrument} on line ${action.lineIdx}`, updatedState),
    historyPositionDelay: 0,
    linePlaying: null,
    player: null
  })
}

// Toggle show grid.
const toggleGrid = (state, action) => {
  // Get the previous value.
  let newShowGrid = !state.showGrid
  // Get the updated state.
  let updatedState = updateObject(state, {
    showGrid: newShowGrid
  })
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Toggle visibility of grid`, updatedState),
    historyPositionDelay: 0
  })
}

// Collapse a line.
const collapseLine = (state, action) => {
  // Copy the state lines.
  let newLines = copyStateLines(state.lines, state.instrumentOrder)
  // Set current line as collapsed.
  newLines[action.lineIdx].collapsed = true
  // Get the updated state.
  let updatedState = updateObject(state, {
    lines: newLines
  })
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Collapse line ${action.lineIdx}`, updatedState),
    historyPositionDelay: 0
  })
}

// Expand a collapsed line.
const expandLine = (state, action) => {
  // Copy the state lines.
  let newLines = copyStateLines(state.lines, state.instrumentOrder)
  // Set current line as collapsed.
  newLines[action.lineIdx].collapsed = false
  // Get the updated state.
  let updatedState = updateObject(state, {
    lines: newLines
  })
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Expand line ${action.lineIdx}`, updatedState),
    historyPositionDelay: 0
  })
}

// Change the background color of a line.
const changeBgColor = (state, action) => {
  // Copy the state lines.
  let newLines = copyStateLines(state.lines, state.instrumentOrder)
  // Set the background of current line.
  newLines[action.lineIdx].bgColor = action.color
  // Get the updated state.
  let updatedState = updateObject(state, {
    lines: newLines
  })
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Change background color of line ${action.lineIdx}`, updatedState),
    historyPositionDelay: 0
  })
}

// Apply props to all lines in section.
const applyToSection = (lines, newProps, separatorIdx = 0) => {
  // Return if the line index is greater or equal to the lines length.
  if (lines.length <= +separatorIdx) return
  // Loop through every line.
  for (let i = +separatorIdx + 1; i < lines.length; i++) {
    // If we arrived to another separator, break the loop.
    if (lines[i].type === lineTypes.separator) break
    // Extend the new line.
    for (let key in newProps) {
      if (newProps.hasOwnProperty(key)) {
        lines[i][key] = newProps[key]
      }
    }
  }
  // Return.
  return lines
}

// Apply props to all lines.
const applyToAllLines = (lines, newProps) => {
  // Loop through every line.
  for (let i = 0; i < lines.length; i++) {
    // Apply only to lines.
    if (lines[i].type !== lineTypes.separator) {
      // Extend the new line.
      for (let key in newProps) {
        if (newProps.hasOwnProperty(key)) {
          lines[i][key] = newProps[key]
        }
      }
    }
  }
  // Return.
  return lines
}

// Collapse all lines below a separator.
const collapseSection = (state, action) => {
  // Copy the state lines.
  let newLines = copyStateLines(state.lines, state.instrumentOrder)
  // Get the updated state.
  let updatedState = updateObject(state, {
    lines: applyToSection(newLines, { collapsed: true }, action.lineIdx)
  })
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Collapse lines under separator ${action.lineIdx}`, updatedState),
    historyPositionDelay: 0
  })
}

// Expand all lines below a separator.
const expandSection = (state, action) => {
  // Copy the state lines.
  let newLines = copyStateLines(state.lines, state.instrumentOrder)
  // Get the updated state.
  let updatedState = updateObject(state, {
    lines: applyToSection(newLines, { collapsed: false }, action.lineIdx)
  })
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Expand lines under separator ${action.lineIdx}`, updatedState),
    historyPositionDelay: 0
  })
}

// Change the background color of a section.
const changeSectionBgColor = (state, action) => {
  // Copy the state lines.
  let newLines = copyStateLines(state.lines, state.instrumentOrder)
  // Get the updated state.
  let updatedState = updateObject(state, {
    lines: applyToSection(newLines, { bgColor: action.bgColor }, action.lineIdx)
  })
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Change background color of lines under separator ${action.lineIdx}`, updatedState),
    historyPositionDelay: 0
  })
}

// Collapse all lines.
const collapseAll = (state, action) => {
  // Copy the state lines.
  let newLines = copyStateLines(state.lines, state.instrumentOrder)
  // Get the updated state.
  let updatedState = updateObject(state, {
    lines: applyToAllLines(newLines, { collapsed: true })
  })
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Collapse all lines`, updatedState),
    historyPositionDelay: 0
  })
}

// Expand all lines.
const expandAll = (state, action) => {
  // Copy the state lines.
  let newLines = copyStateLines(state.lines, state.instrumentOrder)
  // Get the updated state.
  let updatedState = updateObject(state, {
    lines: applyToAllLines(newLines, { collapsed: false })
  })
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Collapse all lines`, updatedState),
    historyPositionDelay: 0
  })
}

// Remove last bar of a line.
const removeBar = (state, action) => {
  // If only one bar left in that line, remove the line.
  if (state.lines[action.lineIdx].content.length === 1) return removeLine(state, action)
  // Copy the state lines.
  let newLines = copyStateLines(state.lines, state.instrumentOrder)
  // Remove last bar of this line.
  newLines[action.lineIdx].content.pop()
  // Get the updated state.
  let updatedState = updateObject(state, {
    lines: newLines
  })
  // Stop the music.
  if (state.player) clearInterval(state.player)
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Removing last bar from line ${action.lineIdx}`, updatedState),
    historyPositionDelay: 0,
    linePlaying: null,
    player: null
  })
}

// Undo.
const undo = (state, action) => {
  // Calculate the current pointer.
  let pointer = state.actionHistory.length + state.historyPositionDelay - 1
  // If the pointer is negative, return current state.
  if (pointer <= 0) return state
  // Create a copy of the history object.
  let historyCopy = copyStateHistory(state.actionHistory, pointer)
  // Get last element of history.
  let lastHistory = historyCopy.pop()
  // Get the state to recover.
  let stateToRecover = {
    title: lastHistory.state.title,
    bpm: lastHistory.state.bpm,
    timeSignature: lastHistory.state.timeSignature,
    lines: copyStateLines(lastHistory.state.lines, lastHistory.state.instrumentOrder),
    instrumentOrder: lastHistory.state.instrumentOrder,
    instrumentIcons: lastHistory.state.instrumentIcons,
    showGrid: lastHistory.state.showGrid
  }
  // Create a copy of the state object.
  let stateCopy = { ...state }
  // Stop the music.
  if (state.player) clearInterval(state.player)
  // Update the state.
  return updateObject(state, {
    ...stateToRecover,
    historyPositionDelay: stateCopy.historyPositionDelay - 1,
    linePlaying: null,
    player: null
  })
}

// Redo.
const redo = (state, action) => {
  // If the state delay is 0, return.
  if (state.historyPositionDelay >= 0) return state
  // Calculate the current pointer.
  let pointer = state.actionHistory.length + (state.historyPositionDelay /* negative number */) + 1
  // Create a copy of the history object.
  let historyCopy = copyStateHistory(state.actionHistory, pointer)
  // Get last element of history, that has one more element to current state.
  let lastHistory = historyCopy[historyCopy.length - 1]
  // Get the state to recover.
  let stateToRecover = {
    title: lastHistory.state.title,
    bpm: lastHistory.state.bpm,
    timeSignature: lastHistory.state.timeSignature,
    lines: copyStateLines(lastHistory.state.lines, lastHistory.state.instrumentOrder),
    instrumentOrder: lastHistory.state.instrumentOrder,
    instrumentIcons: lastHistory.state.instrumentIcons,
    showGrid: lastHistory.state.showGrid
  }
  // Create a copy of the state object.
  let stateCopy = { ...state }
  // Stop the music.
  if (state.player) clearInterval(state.player)
  // Update the state.
  return updateObject(state, {
    ...stateToRecover,
    historyPositionDelay: stateCopy.historyPositionDelay + 1,
    linePlaying: null,
    player: null
  })
}

// When instrument icons have finished loading.
const loadInstrumentImagesSuccess = (state, action) => {
  // Update the state.
  return updateObject(state, {
    instrumentImagesLoaded: true
  })
}

// When instrument audios have finished loading.
const loadInstrumentAudiosSuccess = (state, action) => {
  // Update the state.
  return updateObject(state, {
    instrumentAudiosLoaded: true
  })
}

// Start playing a line.
const startPlayingLine = (state, action) => {
  log(`Playing line ${action.lineIdx}. Interval: ${action.player}.`)
  // Stop the music.
  if (state.player) clearInterval(state.player)
  // Update the state.
  return updateObject(state, {
    linePlaying: action.lineIdx,
    player: action.player,
    loading: false
  })
}

// Play a step.
const playStep = (state, action) => {
  // Don't, if no player.
  if (!state.player) return state
  // Update the state.
  return updateObject(state, {
    stepPlaying: action.stepIdx * stepSizes[action.hitDepth]
  })
}

// Stop playing a line.
const stopPlaying = (state, action) => {
  // Stop the music.
  if (state.player) clearInterval(state.player)
  log(`Player stopped!`)
  // Update the state.
  return updateObject(state, {
    linePlaying: null,
    stepPlaying: null,
    projectPlaying: false,
    player: null
  })
}

// Toggle play loop.
const togglePlayLoop = (state, action) => {
  // Stop the music.
  if (state.player) clearInterval(state.player)
  // Get the previous value.
  let newPlayLoop = !state.playLoop
  // Update the state.
  return updateObject(state, {
    playLoop: newPlayLoop,
    linePlaying: null,
    player: null
  })
}

// Update the line with the cursor over it.
const mouseOverLine = (state, action) => {
  // Update the state.
  return updateObject(state, {
    lineHover: action.lineIdx
  })
}

// Move line up.
const moveLineUp = (state, action) => {
  // If line is at the top, do nothing.
  if (action.lineIdx === 0) return state
  // Init the copy of the current state lines array.
  let newLines = copyStateLines(state.lines, state.instrumentOrder)
  // Save line to move.
  let lineToMoveDown = { ...newLines[action.lineIdx - 1], content: copyStateLineContent(newLines[action.lineIdx - 1].content, state.instrumentOrder) }
  // Move line up.
  newLines[action.lineIdx - 1] = { ...newLines[action.lineIdx], content: copyStateLineContent(newLines[action.lineIdx].content, state.instrumentOrder) }
  newLines[action.lineIdx] = lineToMoveDown
  // Get the updated state.
  let updatedState = updateObject(state, {
    lines: newLines
  })
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Move line up`, updatedState),
    historyPositionDelay: 0
  })
}

// Move line down.
const moveLineDown = (state, action) => {
  // If line is at the bottom, do nothing.
  if (action.lineIdx === state.lines.length - 1) return state
  // Init the copy of the current state lines array.
  let newLines = copyStateLines(state.lines, state.instrumentOrder)
  // Save line to move.
  let lineToMoveUp = { ...newLines[action.lineIdx + 1], content: copyStateLineContent(newLines[action.lineIdx + 1].content, state.instrumentOrder) }
  // Move line up.
  newLines[action.lineIdx + 1] = { ...newLines[action.lineIdx], content: copyStateLineContent(newLines[action.lineIdx].content, state.instrumentOrder) }
  newLines[action.lineIdx] = lineToMoveUp
  // Get the updated state.
  let updatedState = updateObject(state, {
    lines: newLines
  })
  // Update the state.
  return updateObject(updatedState, {
    actionHistory: updateHistory(`Move line down`, updatedState),
    historyPositionDelay: 0
  })
}

// Play metronome.
const playMetronome = (state, action) => {
  // Stop the music.
  if (state.player) clearInterval(state.player)
  // Update the state.
  return updateObject(state, {
    linePlaying: -1,
    player: action.player
  })
}

// Generate WAV success.
const generateWAVSuccess = (state, action) => {
  // Stop the music.
  if (state.player) clearInterval(state.player)
  // Remove the loading state.
  return updateObject(state, {
    wavDownloadLink: action.wavDownloadLink,
    loading: false,
    linePlaying: null,
    player: null
  })
}

// Generate WAV failed.
const generateWAVFailed = (state, action) => {
  // Remove the loading state.
  return updateObject(state, {
    loading: false
  })
}
// Download WAV.
const downloadWAV = (state, action) => {
  // Remove the loading state.
  return updateObject(state, {
    wavDownloadLink: null
  })
}

// Generate MP3 success.
const generateMP3Success = (state, action) => {
  // Stop the music.
  if (state.player) clearInterval(state.player)
  // Remove the loading state.
  return updateObject(state, {
    mp3DownloadLink: action.mp3DownloadLink,
    loading: false,
    linePlaying: null,
    player: null
  })
}

// Generate MP3 failed.
const generateMP3Failed = (state, action) => {
  // Remove the loading state.
  return updateObject(state, {
    loading: false
  })
}

// Download MP3.
const downloadMP3 = (state, action) => {
  // Remove the loading state.
  return updateObject(state, {
    mp3DownloadLink: null
  })
}

// Start playing whole project.
const startPlayingProject = (state, action) => {
  // Update the state.
  return updateObject(state, {
    projectPlaying: true,
    loading: false
  })
}

// Change the line playing.
const changeLinePlaying = (state, action) => {
  log(`Changing the playing line to ${action.lineIdx}.`)
  // Update the state.
  return updateObject(state, {
    linePlaying: action.lineIdx
  })
}

// Toggle the metronome.
const toggleMetronome = (state, action) => {
  // Stop the music.
  if (state.player) clearInterval(state.player)
  // Get the previous value.
  let newMetronome = !state.metronome
  // Update the state.
  return updateObject(state, {
    metronome: newMetronome,
    linePlaying: null,
    player: null
  })
}

// ============================================================================
// Reducer definition.
// ============================================================================

// Reducer definition.
const reducer = (state = initialState, action) => {
  switch (action.type) {
    // LAYOUT ACTIONS.
    case actions.NEW_PROJECT:
      return newProject(state, action)
    case actions.FETCH_PROJECTS_SUCCESS:
      return fetchProjectsSuccess(state, action)
    case actions.FETCH_PROJECTS_FAILED:
      return fetchProjectsFailed(state, action)
    case actions.LOAD_PROJECT:
      return loadProject(state, action)
    case actions.START_LOADING:
      return startLoading(state, action)
    case actions.STOP_LOADING:
      return stopLoading(state, action)
    case actions.LOAD_INSTRUMENT_IMAGES_SUCCESS:
      return loadInstrumentImagesSuccess(state, action)
    case actions.LOAD_INSTRUMENT_AUDIOS_SUCCESS:
      return loadInstrumentAudiosSuccess(state, action)

    // PROJECT ACTIONS.
    case actions.SAVE_NEW_PROJECT_SUCCESS:
      return saveNewProjectSuccess(state, action)
    case actions.SAVE_EXISTING_PROJECT_SUCCESS:
      return saveExistingProjectSuccess(state, action)
    case actions.SAVE_PROJECT_FAILED:
      return saveProjectFailed(state, action)
    case actions.DELETE_PROJECT_SUCCESS:
      return deleteProjectSuccess(state, action)
    case actions.DELETE_PROJECT_FAILED:
      return deleteProjectFailed(state, action)
    case actions.GENERATE_SHEETS_SUCCESS:
      return generateSheetsSuccess(state, action)
    case actions.GENERATE_SHEETS_FAILED:
      return generateSheetsFailed(state, action)
    case actions.CHANGE_INSTRUMENTS_ORDER:
      return changeInstrumentsOrder(state, action)
    case actions.CHANGE_INSTRUMENT_ICONS:
      return changeInstrumentIcons(state, action)
    case actions.DOWNLOAD_SHEETS:
      return downloadSheets(state, action)
    case actions.RESET_PROJECT:
      return resetProject(state, action)
    case actions.ADD_LINE:
      return addLine(state, action)
    case actions.UPDATE_TITLE:
      return updateTitle(state, action)
    case actions.UPDATE_TIME_SIGNATURE:
      return updateTimeSignature(state, action)
    case actions.UPDATE_BPM:
      return updateBPM(state, action)
    case actions.TOGGLE_GRID:
      return toggleGrid(state, action)
    case actions.UNDO:
      return undo(state, action)
    case actions.REDO:
      return redo(state, action)
    case actions.COLLAPSE_ALL_LINES:
      return collapseAll(state, action)
    case actions.EXPAND_ALL_LINES:
      return expandAll(state, action)
    case actions.START_PLAYING_LINE:
      return startPlayingLine(state, action)
    case actions.PLAY_STEP:
      return playStep(state, action)
    case actions.STOP_PLAYING:
      return stopPlaying(state, action)
    case actions.TOGGLE_PLAY_LOOP:
      return togglePlayLoop(state, action)
    case actions.PLAY_METRONOME:
      return playMetronome(state, action)
    case actions.GENERATE_WAV_SUCCESS:
      return generateWAVSuccess(state, action)
    case actions.GENERATE_WAV_FAILED:
      return generateWAVFailed(state, action)
    case actions.DOWNLOAD_WAV:
      return downloadWAV(state, action)
    case actions.GENERATE_MP3_SUCCESS:
      return generateMP3Success(state, action)
    case actions.GENERATE_MP3_FAILED:
      return generateMP3Failed(state, action)
    case actions.DOWNLOAD_MP3:
      return downloadMP3(state, action)
    case actions.START_PLAYING_PROJECT:
      return startPlayingProject(state, action)
    case actions.CHANGE_LINE_PLAYING:
      return changeLinePlaying(state, action)
    case actions.TOGGLE_METRONOME:
      return toggleMetronome(state, action)

    // LINE ACTIONS.
    case actions.CLICK_BEAT:
      return clickBeat(state, action)
    case actions.SET_HIT_TYPE:
      return setHitType(state, action)
    case actions.COPY_LINE_BELOW:
      return copyLineBelow(state, action)
    case actions.COPY_LINE_AT_THE_END:
      return copyLineAtTheEnd(state, action)
    case actions.INSERT_LINE_BELOW:
      return insertLineBelow(state, action)
    case actions.INSERT_LINE_ABOVE:
      return insertLineAbove(state, action)
    case actions.INSERT_SEPARATOR_BELOW:
      return insertSeparatorBelow(state, action)
    case actions.INSERT_SEPARATOR_ABOVE:
      return insertSeparatorAbove(state, action)
    case actions.REMOVE_LINE:
      return removeLine(state, action)
    case actions.RESET_LINE:
      return resetLine(state, action)
    case actions.EDIT_SEPARATOR_LABEL:
      return editSeparatorLabel(state, action)
    case actions.COLLAPSE_LINE:
      return collapseLine(state, action)
    case actions.EXPAND_LINE:
      return expandLine(state, action)
    case actions.CHANGE_BG_COLOR:
      return changeBgColor(state, action)
    case actions.COLLAPSE_SECTION:
      return collapseSection(state, action)
    case actions.EXPAND_SECTION:
      return expandSection(state, action)
    case actions.CHANGE_SECTION_BG_COLOR:
      return changeSectionBgColor(state, action)
    case actions.REMOVE_BAR:
      return removeBar(state, action)
    case actions.MOUSE_OVER_LINE:
      return mouseOverLine(state, action)
    case actions.MOVE_LINE_UP:
      return moveLineUp(state, action)
    case actions.MOVE_LINE_DOWN:
      return moveLineDown(state, action)

    // INSTRUMENT ACTIONS.
    case actions.FILL_EACH_STEPS:
      return fillEachSteps(state, action)
    case actions.SHIFT_STEPS_LEFT:
      return shiftStepsLeft(state, action)
    case actions.SHIFT_STEPS_RIGHT:
      return shiftStepsRight(state, action)
    case actions.REMOVE_INSTRUMENT:
      return removeInstrument(state, action)
    case actions.RESET_STEPS:
      return resetSteps(state, action)

    default:
      return state
  }
}

// Export.
export default reducer
