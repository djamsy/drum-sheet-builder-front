// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons'

// Components.
import Tooltip from '../Tooltip/Tooltip'

// Styles.
import './Label.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const label = (props) => {
  // Init tip.
  let tip = null
  if (props.help) {
    tip = (
      <span className='LabelTip IconLabel'>
        <Tooltip content={props.help} placement='bottom'>
          <FontAwesomeIcon icon={faInfoCircle} />
        </Tooltip>
      </span>
    )
  }

  return (
    <div className='Label'>
      {props.children}{tip}
    </div>
  )
}

// Export.
export default label
