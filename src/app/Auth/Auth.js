// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Component } from 'react'
import { Redirect, withRouter, Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope, faLock, faTimes, faInfoCircle } from '@fortawesome/free-solid-svg-icons'

// Style.
import './Auth.css'

// Components.
import Button from '../UI/Button/Button'
import Loader from '../UI/Loader/Loader'
import FormRow from './FormRow/FormRow'
import Checkbox from '../UI/Checkbox/Checkbox'
import PrivacyPolicy from '../PrivacyPolicy/PrivacyPolicy'

// Redux actions.
import * as actions from '../../store/actions/index'

// Logo image.
import * as logo from '../../logo.sm.png'

// Shared.
import {
  validateEmail,
  validatePassword
} from '../../shared/functions'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateful component declaration.
class Auth extends Component {
  state = {
    fields: {
      email: {
        label: 'E-mail',
        icon: faEnvelope,
        type: 'email',
        value: '',
        error: null,
        valid: false,
        required: true,
        touched: false,
        validation: validateEmail,
        errMsg: 'Your e-mail must be a valid e-mail'
      },
      password: {
        label: 'Password',
        icon: faLock,
        type: 'password',
        value: '',
        error: null,
        required: true,
        touched: false,
        validation: validatePassword,
        errMsg: 'Your password must be at least 5 characters long'
      }
    },
    termsAccepted: false,
    privacyPolictModalOpen: false
  }

  // Open the privacy policy modal.
  showPrivacyPolicyModal = () => {
    this.setState({
      privacyPolicyModalOpen: true
    })
  }

  // Close the privacy policy modal.
  hidePrivacyPolicyModal = () => {
    this.setState({
      privacyPolicyModalOpen: false
    })
  }

  // Class method, inherited from Component.
  componentDidMount () {
    this.props.resetLoginError()
  }

  // Class method, inherited from Component.
  componentDidUpdate (prevProps) {
    if (prevProps.mode !== this.props.mode) {
      this.props.resetLoginError()
    }
  }

  // What to do when the value of an input changes.
  valueChangedHandler = (elemKey, newValue) => {
    // Create a copy of the state object.
    let newElement = {
      ...this.state.fields[elemKey]
    }
    // Modify the element value.
    newElement.value = newValue
    // Also, set touched to true.
    newElement.touched = true
    // Check element validity.
    if (newElement.required && !newValue.length && newElement.touched) {
      newElement.error = `Field ${elemKey} is required`
    } else if (newElement.validation && !newElement.validation(newValue)) {
      newElement.error = newElement.errMsg
    } else {
      newElement.error = null
    }
    // Update the state.
    this.setState({
      fields: {
        ...this.state.fields,
        [elemKey]: newElement
      }
    })
    // Reset the login error.
    this.props.resetLoginError()
  }

  // Checkbox handler.
  termsAndConditionsToggle = event => {
    // Get the current value.
    let termsAccepted = this.state.termsAccepted
    // Update the state.
    this.setState({
      termsAccepted: !termsAccepted
    })
  }

  // Return true if the submit button must be disabled.
  shouldSubmitBeDisabled = () => {
    let baseCondition = this.state.fields.email.error !== null ||
      this.state.fields.password.error !== null ||
      !this.state.fields.email.touched ||
      !this.state.fields.password.touched
    // If mode is signUp, then append the 'terms and conditions' condition.
    return this.props.mode === 'signIn' ? baseCondition : (baseCondition || !this.state.termsAccepted)
  }

  // Returns the action to perform on submit.
  getSubmitAction = () => {
    if (this.props.mode === 'signUp') {
      return this.props.signUp(this.state.fields.email.value, this.state.fields.password.value, this.state.termsAccepted)
    } else {
      return this.props.login(this.state.fields.email.value, this.state.fields.password.value)
    }
  }

  // On ENTER key pressed.
  _handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      if (!this.shouldSubmitBeDisabled()) {
        this.getSubmitAction()
      }
    }
  }

  // Class method, inherited from Component.
  render () {
    // If user is authenticated, redirect to home.
    if (this.props.isAuth) {
      return <Redirect to={this.props.authRedirectPath} />
    }

    // Init the privacy policy modal.
    let privacyPolicyModal = null
    if (this.state.privacyPolicyModalOpen) {
      privacyPolicyModal = <PrivacyPolicy modalOpen={this.state.privacyPolicyModalOpen} hideModal={this.hidePrivacyPolicyModal} />
    }

    // Init the component as a loader.
    let auth = <Loader top='50%' />

    // Build the third row of the form depending on the mode.
    let thirdRow = null
    switch (this.props.mode) {
      case 'signUp':
        thirdRow = (
          <div className='FormRow SignUpRow'>
            <div className='TermsConditionsInput'>
              <Checkbox
                active={this.state.termsAccepted}
                clicked={this.termsAndConditionsToggle}
                label='I accept the terms and conditions *'
              />
            </div>
            <div className='TermsConditionsText'>
              * Your e-mail will be stored in our database for authentication purposes and will not be shared with third parties. You will not receive any e-mail from us but the password restoration e-mail. <span className='PrivacyPolicyLink' onClick={this.showPrivacyPolicyModal}>Privacy policy</span>
            </div>
            {privacyPolicyModal}
            <div className='SignInRow TermsConditionsFooter'>
              <FontAwesomeIcon icon={faInfoCircle} /> Do you already have an account? <Link to='login'>Switch to login</Link>
            </div>
          </div>
        )
        break
      case 'signIn':
        thirdRow = (
          <div className='FormRow SignInRow'>
            <FontAwesomeIcon icon={faInfoCircle} /> Are you new here? <Link to='signup'>Switch to sign up</Link>
          </div>
        )
        break
      default:
        thirdRow = null
    }

    // Build the 'recover password' button.
    let recoverPasswordButton = null
    if (this.props.mode !== 'signUp') {
      recoverPasswordButton = (
        <Button
          type='default'
          width='100%'
          clicked={() => this.props.history.push('recovery')}>
          I forgot my password
        </Button>
      )
    }

    // If not loading, render normally.
    if (!this.props.loading) {
      auth = (
        <div className='AuthBox'>
          <div className='AuthBoxHeader'>
            Please, sign in to start your session
          </div>
          {
            Object.keys(this.state.fields).sort().map(elem => (
              <FormRow
                keyPress={this._handleKeyPress}
                key={elem}
                validationMsg={this.state.fields[elem].errMsg}
                showValidationMsg={this.props.mode === 'signUp'}
                label={this.state.fields[elem].label}
                type={this.state.fields[elem].type}
                value={this.state.fields[elem].value}
                icon={this.state.fields[elem].icon}
                error={this.state.fields[elem].error}
                touched={this.state.fields[elem].touched}
                changed={(event) => this.valueChangedHandler(elem, event.target.value)}
              />
            ))
          }
          {thirdRow}
          <div className='AuthBoxLogin'>
            <Button
              type='primary'
              width='100%'
              disable={this.shouldSubmitBeDisabled()}
              clicked={this.getSubmitAction}
            >
              {this.props.mode === 'signUp' ? 'Sign up' : 'Sign in'}
            </Button>
            {recoverPasswordButton}
          </div>
        </div>
      )
    }

    // Init the error.
    let error = null
    if (this.props.error) {
      error = (
        <div className='AuthFooter FooterError'>
          <FontAwesomeIcon icon={faTimes} /> {this.props.error}
        </div>
      )
    }

    return (
      <div className='AuthContainer'>
        <div className='Auth'>
          <div className='AuthHeader'>
            <div>
              <a href='/'>
                <img src={logo} alt='Drum sheet builder' />
              </a>
            </div>
            <div className='AuthHeaderText'>
              DrumSheet<b>Builder</b>
            </div>
          </div>
          {auth}
          {error}
        </div>
      </div>
    )
  }
}

// ============================================================================
// Connect with Redux and export.
// ============================================================================

// State mapping.
const mapStateToProps = state => {
  return {
    authRedirectPath: state.auth.authRedirectPath,
    error: state.auth.error,
    loading: state.auth.loading,
    isAuth: state.auth.userId !== null
  }
}

// Action mapping.
const mapDispatchToProps = dispatch => {
  return {
    resetLoginError: () => dispatch(actions.resetLoginError()),
    login: (email, password) => dispatch(actions.signIn({ email: email, password: password })),
    signUp: (email, password, termsAccepted) => dispatch(actions.signUp({ email: email, password: password, termsAccepted: termsAccepted }))
  }
}

// Export.
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Auth))
