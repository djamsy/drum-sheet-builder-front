// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment, Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCaretRight } from '@fortawesome/free-solid-svg-icons'

// Style.
import './NavbarDropdown.css'

// Components.
import Backdrop from '../../../../UI/Backdrop/Backdrop'
import NavbarDropdownSubmenu from './NavbarDropdownSubmenu/NavbarDropdownSubmenu'

// =============================================================================
// Component declaration.
// =============================================================================

// Declare outside the initial state.
const initialState = {
  projectListSubmenuDisplayed: false,
  instrumentIconsSubmenuDisplayed: false,
  instrumentOrderSubmenuDisplayed: false
}

// Stateful component declaration.
class NavbarDropdown extends Component {
  state = initialState

  // Toggle visibility of the submenu.
  toggleSubmenu = parentKey => {
    this.setState(prevState => {
      return {
        ...initialState,
        [parentKey + 'SubmenuDisplayed']: !prevState[parentKey + 'SubmenuDisplayed']
      }
    })
  }

  // Close submenus.
  closeSubmenus = () => {
    this.setState(initialState)
  }

  render () {
    // If no need to be shown, return null.
    if (!this.props.show) return null

    return (
      <Fragment>
        <Backdrop show={this.props.show} clicked={() => {
          this.props.onClose()
          this.closeSubmenus()
        }} transparent />
        <div className='NavbarDropdown' style={{ left: 36 /* logo width */ + 60 /* button width */ * +this.props.position }}>
          {
            this.props.options.map((option, idx) => {
              // Init the icon.
              let icon = null
              if (option.icon) icon = <FontAwesomeIcon icon={option.icon} />

              // Init the shortcut.
              let shortcut = null
              if (option.shortcut) shortcut = <small>{option.shortcut.join(' + ')}</small>

              // Init the right arrow.
              let arrow = null
              if (option.submenu) arrow = <FontAwesomeIcon icon={faCaretRight} />

              // Init the submenu.
              let submenu = null
              if (option.submenu) {
                submenu = (
                  <NavbarDropdownSubmenu
                    options={option.submenu}
                    show={this.state[option.parentKey + 'SubmenuDisplayed']}
                    onClose={this.props.onClose}
                  />
                )
              }

              // Init the action to perform.
              let action = () => null
              if (!option.disabled) {
                action = () => {
                  if (option.submenu) {
                    this.toggleSubmenu(option.parentKey)
                  } else {
                    this.props.onClose()
                    this.closeSubmenus()
                    option.clicked()
                  }
                }
              }

              return (
                <div className={'DropdownOption' + (option.disabled ? ' disabled' : '')} key={idx} onClick={action}>
                  <span className='DropdownOptionPre'>
                    {icon}
                  </span>
                  {option.label}
                  <span className='DropdownOptionPost'>
                    {shortcut}
                    {arrow}
                  </span>
                  {submenu}
                </div>
              )
            })
          }
        </div>
      </Fragment>
    )
  }
}

// Export.
export default NavbarDropdown
