// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'

// Components.
import Image from '../../UI/Image/Image'
import Callout from '../../UI/Callout/Callout'
import KeyboardKey from '../../UI/KeyboardKey/KeyboardKey'
import ButtonPreview from '../ButtonPreviews/ButtonPreview/ButtonPreview'
import NavbarButtonPreview from '../ButtonPreviews/NavbarButtonPreview/NavbarButtonPreview'

// Screenshots.
import projectInfoScreenshot from '../../../images/Screenshots/project-info.jpg'
import emptyLineScreenshot from '../../../images/Screenshots/empty-line.jpg'

// Constants.
import { shortcuts } from '../../../shared/constants'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const section = (props) => {
  // Init the depth class.
  let depthClass = null
  if (+props.depth > 1) depthClass = `ml-${+props.depth - 1}0`

  return (
    <Fragment>
      <p className={depthClass}>You can create a new project by clicking <NavbarButtonPreview>File</NavbarButtonPreview> - <NavbarButtonPreview>New</NavbarButtonPreview>, on the main navbar.</p>

      <div className='img-container'>
        <Image src={projectInfoScreenshot} alt='Project information (desktop view)' />
        <div className='img-legend'>Project information (desktop view).</div>
      </div>

      <p className={depthClass}>You will then be asked to provide some basic information about your project before you start writing music (see section <i>The musical theory behind</i>). Once you have filled the fields, the <ButtonPreview type='success'>START PROJECT</ButtonPreview> button will be enabled and will let you start writing your sheet. When clicked, a new empty line will be added to your project.</p>

      <div className={depthClass}>
        <Callout type='Info' label='TIP'>
          You can also create a new project by clicking {shortcuts.newProject.keys.map((key, idx) => <KeyboardKey key={idx} idx={idx}>{key}</KeyboardKey>)}.
        </Callout>
      </div>

      <div className='img-container'>
        <Image src={emptyLineScreenshot} alt='Empty line with a time signature of 8/8' />
        <div className='img-legend'>Empty line with a time signature of 8/8.</div>
      </div>
    </Fragment>
  )
}

// Export.
export default section
