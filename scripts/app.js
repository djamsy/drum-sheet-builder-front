'use strict'

// Load environment variables.
require('dotenv').config()

// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const fs = require('fs')
const path = require('path')

// =============================================================================
// Main.
// =============================================================================

// If no environment specified, exit.
if (!process.env.NODE_ENV || ['production', 'development'].indexOf(process.env.NODE_ENV) === -1) {
  console.log('Wrong NODE_ENV environment property. Exiting.')
  process.exit(1)
}

// If environment is production but no build directory exists.
if (process.env.NODE_ENV === 'production') {
  fs.readdir(path.join(__dirname, '../dist'), error => {
    if (error) {
      console.log('Please, execute npm run build to generate a production build before you deploy your production server. Exiting.')
      process.exit(1)
    }
  })
}

// Execute the corresponding script.
require('./start/' + process.env.NODE_ENV)()
