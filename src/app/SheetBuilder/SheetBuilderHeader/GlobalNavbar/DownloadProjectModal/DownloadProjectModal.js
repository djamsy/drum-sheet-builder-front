// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'
import { faFilePdf, faInfoCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// Components.
import Modal from '../../../../UI/Modal/Modal'
import FileButton from '../../../../UI/FileButton/FileButton'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
export const downloadProjectModal = props => {
  return (
    <Modal
      title='Downloading project'
      show={props.modalOpen}
      modalClosed={props.downloadSheets}
    >
      <p>Your sheets for project <b>{props.title}</b> have been generated successfully.</p>
      <p>Here's your download link:</p>
      <FileButton
        type='pdf'
        icon={faFilePdf}
        fileName={`${props.title}.pdf`}
        clicked={() => {
          props.downloadSheets()
          window.open(props.pdfDownloadLink)
        }}
      />
      <p className='ModalTip'>
        <FontAwesomeIcon icon={faInfoCircle} /> Note that the downloaded version will correspond to the <u>last saved version of your project</u>.
      </p>
    </Modal>
  )
}

// Export.
export default downloadProjectModal
