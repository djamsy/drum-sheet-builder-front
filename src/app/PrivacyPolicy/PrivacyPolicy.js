// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons'

// Style.
import './PrivacyPolicy.css'

// Components.
import Modal from '../UI/Modal/Modal'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const privacyPolicy = props => (
  <Modal title='Privacy policy' show={props.modalOpen} modalClosed={props.hideModal}>
    <div className='PrivacyPolicyHeader'>
      <p>
        <FontAwesomeIcon icon={faInfoCircle} /> This website stores yout personal information with the only aim to provide an authentication mechanism.
      </p>
    </div>
    <div className='PrivacyPolicy'>
      <h3>What is my personal information used for?</h3>
      <p>
        We store the previously mentioned data for authentication purposes only. Storing your e-mail is the only way of linking your projects to your account.
      </p>
      <p>
        We <b>do not share</b> your personal information with third parties, nor we use it for other purposes.
      </p>
      <h3>How is the security of my information ensured?</h3>
      <p>
        All the information you enter at DrumSheetBuilder is sent to our API REST server with a secure protocol (HTTPS), which means your data is encrypted during the process.
      </p>
      <p>
        Our server incorporates the necessary security mechanisms to ensure your personal information remains safe and private.
      </p>
      <h3>What personal information do we store?</h3>
      <p>
        At DrumSheetBuilder, we only store the following personal information of yourself:
      </p>
      <ul>
        <li>Your <b>e-mail</b> address, which you entered on the registration process.</li>
        <li>A hashed version of your <b>password</b>, not the password itself, for security reasons.</li>
      </ul>
      <p>
        We also store all the projects that you have saved within your account, and logs fo your actions and HTTP requests/responses.
      </p>
      <p>
        You will not receive any e-mail from us but the password restoration e-mail.
      </p>
      <h3>Can I request a complete deletion of my personal data?</h3>
      <p>
        Yes, you can. For that purpose, please, write to <a href={`mailto:${process.env.REACT_APP_CONTACT_EMAIL}`}>{process.env.REACT_APP_CONTACT_EMAIL}</a> and we will delete all your personal information.
      </p>
      <p>
        Be careful! This action also deletes your projects and generated files, and cannot be undone.
      </p>
    </div>
  </Modal>
)

// Export.
export default privacyPolicy
