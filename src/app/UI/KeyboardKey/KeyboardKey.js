// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'

// Style.
import './KeyboardKey.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const keyboardKey = props => {
  // Init the plus sign.
  let plusSign = null
  if (props.idx !== 0) plusSign = <span className='KeyboardKeyPlus'>+</span>

  return (
    <Fragment>
      {plusSign}
      <span className='KeyboardKey'>
        {props.children}
      </span>
    </Fragment>
  )
}

// Export.
export default keyboardKey
