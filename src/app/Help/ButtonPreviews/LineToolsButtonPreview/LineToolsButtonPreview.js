// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Style.
import './LineToolsButtonPreview.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const lineToolsButtonPreview = (props) => {
  return (
    <span className='LineToolsButtonPreview'>
      {props.children}
    </span>
  )
}

// Export.
export default lineToolsButtonPreview
