// ============================================================================
// Dependencies.
// ============================================================================

// Action types.
import * as actions from './types'

// ============================================================================
// Constants.
// ============================================================================

// Time thet the alert is shown before fading out.
const ALERT_MESSAGE_DURATION = 1800 /* milliseconds */

// ============================================================================
// Action creators.
// ============================================================================

// Trigger a success alert message.
export const alertSuccess = (message) => {
  return dispatch => {
    dispatch(showAlert('Success', message))
  }
}

// Trigger a danger alert message.
export const alertDanger = (message) => {
  return dispatch => {
    dispatch(showAlert('Danger', message))
  }
}

// Trigger a warning alert message.
export const alertWarning = (message) => {
  return dispatch => {
    dispatch(showAlert('Warning', message))
  }
}

// Trigger a info alert message.
export const alertInfo = (message) => {
  return dispatch => {
    dispatch(showAlert('Info', message))
  }
}

// Hide alert message.
export const hideAlert = () => {
  return {
    type: actions.HIDE_ALERT
  }
}

// Show the alert message and hide after some seconds.
export const showAlert = (alertType, message) => {
  return dispatch => {
    dispatch(showAlertMessage(alertType, message))
    setTimeout(() => {
      dispatch(hideAlert())
    }, ALERT_MESSAGE_DURATION)
  }
}

// Just show the alert message.
export const showAlertMessage = (alertType, message) => {
  return {
    type: actions.SHOW_ALERT,
    alertType: alertType,
    message: message
  }
}
