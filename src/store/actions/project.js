// ============================================================================
// Dependencies.
// ============================================================================

// Axios instance.
import axios from '../../axios'

// Action types.
import * as actions from './types'

// Functions.
import { getErrorMessage, getAllInstrumentAssets } from '../../shared/functions'

// Alert actions.
import { alertSuccess, alertDanger } from './alert'

// ============================================================================
// Action creators.
// ============================================================================

// New project.
export const newProject = () => {
  return {
    type: actions.NEW_PROJECT
  }
}

// Fetch projects init.
export const fetchProjects = (userId, token) => {
  return dispatch => {
    // Get the projects from Firebase.
    axios(token).get('/projects')
      .then(response => {
        // Get the projects by parsing response.
        let projects = response.data
        // Update the state.
        dispatch(fetchProjectsSuccess(projects))
      })
      .catch(error => {
        dispatch(fetchProjectsFailed(getErrorMessage(error)))
        dispatch(alertDanger(`There was an error while fetching projects from server: ${getErrorMessage(error)}`))
      })
  }
}

// Stop the project being in loading state.
export const stopLoading = () => {
  return {
    type: actions.STOP_LOADING
  }
}

// Set the project in loading state.
export const startLoading = () => {
  return {
    type: actions.START_LOADING
  }
}

// Fetch projects success.
export const fetchProjectsSuccess = (projects) => {
  return {
    type: actions.FETCH_PROJECTS_SUCCESS,
    projects: projects
  }
}

// Fetch projects failed.
export const fetchProjectsFailed = (error) => {
  return {
    type: actions.FETCH_PROJECTS_FAILED,
    error: error
  }
}

// Save project init.
export const saveProject = (projectId, title, bpm, timeSignature, lines, instrumentOrder, instrumentIcons, showGrid, token) => {
  return dispatch => {
    dispatch(startLoading())
    // Get the project info to save.
    let projectToPost = {
      title: title,
      bpm: bpm,
      timeSignature: timeSignature,
      lines: lines,
      instrumentOrder: instrumentOrder,
      instrumentIcons: instrumentIcons,
      showGrid: showGrid
    }
    // Send to Firebase.
    if (!projectId) {
      // Perform a POST request.
      axios(token).post('/project', projectToPost)
        .then(response => {
          dispatch(saveNewProjectSuccess(response.data))
          dispatch(alertSuccess(`Project "${title}" created successfully.`))
        })
        .catch(error => {
          dispatch(saveProjectFailed(getErrorMessage(error)))
          dispatch(alertDanger(`There was an error wile creating your project: ${getErrorMessage(error)}`))
        })
    } else {
      // Perform a PUT request.
      axios(token).patch(`/project/${projectId}`, projectToPost)
        .then(response => {
          dispatch(saveExistingProjectSuccess(response.data))
          dispatch(alertSuccess(`Project "${title}" saved successfully.`))
        })
        .catch(error => {
          dispatch(saveProjectFailed(getErrorMessage(error)))
          dispatch(alertDanger(`There was an error wile saving your project: ${getErrorMessage(error)}`))
        })
    }
  }
}

// Save new project success.
export const saveNewProjectSuccess = (savedProject) => {
  return {
    type: actions.SAVE_NEW_PROJECT_SUCCESS,
    savedProject: savedProject
  }
}

// Save existing project success.
export const saveExistingProjectSuccess = (savedProject) => {
  return {
    type: actions.SAVE_EXISTING_PROJECT_SUCCESS,
    savedProject: savedProject
  }
}

// Save project failed.
export const saveProjectFailed = (error) => {
  return {
    type: actions.SAVE_PROJECT_FAILED,
    error: error
  }
}

// Delete project init.
export const deleteProject = (projectId, title, token) => {
  return dispatch => {
    // If no project loaded, just start new one.
    if (!projectId) {
      dispatch(newProject())
      dispatch(alertSuccess(`Project "${title}" deleted successfully.`))
      return
    }
    // Start showing the loader.
    dispatch(startLoading())
    // Perform a DELETE request.
    axios(token).delete(`/project/${projectId}`)
      .then(() => {
        dispatch(deleteProjectSuccess(projectId))
        dispatch(newProject())
        dispatch(alertSuccess(`Project "${title}" deleted successfully.`))
      })
      .catch(error => {
        dispatch(deleteProjectFailed(getErrorMessage(error)))
        dispatch(alertDanger(`There was an error wile deleting your project: ${getErrorMessage(error)}`))
      })
  }
}

// Delete project success.
export const deleteProjectSuccess = (projectId) => {
  return {
    type: actions.DELETE_PROJECT_SUCCESS,
    projectId: projectId
  }
}

// Delete project failed.
export const deleteProjectFailed = (error) => {
  return {
    type: actions.DELETE_PROJECT_FAILED,
    error: error
  }
}

// Generate sheets success.
const generateSheetsSuccess = (pdfDownloadLink) => {
  return {
    type: actions.GENERATE_SHEETS_SUCCESS,
    pdfDownloadLink: pdfDownloadLink
  }
}

// Generate sheets success.
const generateSheetsFailed = () => {
  return {
    type: actions.GENERATE_SHEETS_FAILED
  }
}

// Generate sheets.
export const generateSheets = (projectId, title, token) => {
  return dispatch => {
    // Start loading.
    dispatch(startLoading())
    // Perform request.
    axios(token).post(`/pdf/${projectId}`)
      .then(response => {
        dispatch(generateSheetsSuccess(process.env.REACT_APP_BACKEND_URL + response.data.url + '?token=' + token))
      })
      .catch(error => {
        dispatch(generateSheetsFailed(getErrorMessage(error)))
        dispatch(alertDanger(`Project ${title} can't be downloaded: ${getErrorMessage(error)}`))
      })
  }
}

// Download sheets.
export const downloadSheets = () => {
  return {
    type: actions.DOWNLOAD_SHEETS
  }
}

// Load project.
export const loadProject = (projectId) => {
  return {
    type: actions.LOAD_PROJECT,
    projectId: projectId
  }
}

// Executed when clicking the 'reset' button.
export const resetProject = () => {
  return {
    type: actions.RESET_PROJECT
  }
}

// Add a new line to the project.
export const addLine = () => {
  return {
    type: actions.ADD_LINE
  }
}

// Executed when changing the value of the time signature.
export const updateTimeSignature = (timeSignature) => {
  return {
    type: actions.UPDATE_TIME_SIGNATURE,
    timeSignature: timeSignature
  }
}

// Executed when changing the value of the 'title' input.
export const updateTitle = (title) => {
  return {
    type: actions.UPDATE_TITLE,
    title: title
  }
}

// Updated when changing the value of the 'bpm' input.
export const updateBPM = (bpm) => {
  return {
    type: actions.UPDATE_BPM,
    bpm: bpm
  }
}

// Executed when clicking a beat of any line.
export const clickBeat = (lineIdx, barIdx, instrument, beatIdx) => {
  return {
    type: actions.CLICK_BEAT,
    lineIdx: lineIdx,
    barIdx: barIdx,
    instrument: instrument,
    beatIdx: beatIdx
  }
}

// Change the type of hit of an instrument.
export const setHitType = (lineIdx, barIdx, instrument, beatIdx, hitType) => {
  return {
    type: actions.SET_HIT_TYPE,
    lineIdx: lineIdx,
    barIdx: barIdx,
    instrument: instrument,
    beatIdx: beatIdx,
    hitType: hitType
  }
}

// Copy a line and insert it after.
export const copyLineBelow = (lineIdx) => {
  return {
    type: actions.COPY_LINE_BELOW,
    lineIdx: lineIdx
  }
}

// Copy a line at the end of the project.
export const copyLineAtTheEnd = (lineIdx) => {
  return {
    type: actions.COPY_LINE_AT_THE_END,
    lineIdx: lineIdx
  }
}

// Insert a new line after a given one.
export const insertLineBelow = (lineIdx) => {
  return {
    type: actions.INSERT_LINE_BELOW,
    lineIdx: lineIdx
  }
}

// Insert a new line before a given one.
export const insertLineAbove = (lineIdx) => {
  return {
    type: actions.INSERT_LINE_ABOVE,
    lineIdx: lineIdx
  }
}

// Insert a new separator after a given line.
export const insertSeparatorBelow = (lineIdx, label) => {
  return {
    type: actions.INSERT_SEPARATOR_BELOW,
    lineIdx: lineIdx,
    label: label
  }
}

// Insert a new separator before a given line.
export const insertSeparatorAbove = (lineIdx, label) => {
  return {
    type: actions.INSERT_SEPARATOR_ABOVE,
    lineIdx: lineIdx,
    label: label
  }
}

// Remove line at a specific index.
export const removeLine = (lineIdx) => {
  return {
    type: actions.REMOVE_LINE,
    lineIdx: lineIdx
  }
}

// Reset line at a specific index.
export const resetLine = (lineIdx) => {
  return {
    type: actions.RESET_LINE,
    lineIdx: lineIdx
  }
}

// Edit the separator label.
export const editSeparatorLabel = (lineIdx, label) => {
  return {
    type: actions.EDIT_SEPARATOR_LABEL,
    lineIdx: lineIdx,
    label: label
  }
}

// Fill values of a line each N steps.
export const fillEachSteps = (stepSize, lineIdx, instrument) => {
  return {
    type: actions.FILL_EACH_STEPS,
    stepSize: stepSize,
    lineIdx: lineIdx,
    instrument: instrument
  }
}

// Shift values of a line to the left.
export const shiftStepsLeft = (lineIdx, instrument) => {
  return {
    type: actions.SHIFT_STEPS_LEFT,
    lineIdx: lineIdx,
    instrument: instrument
  }
}

// Shift values of a line to the right.
export const shiftStepsRight = (lineIdx, instrument) => {
  return {
    type: actions.SHIFT_STEPS_RIGHT,
    lineIdx: lineIdx,
    instrument: instrument
  }
}

// Remove an instrument from a line.
export const removeInstrument = (lineIdx, instrument) => {
  return {
    type: actions.REMOVE_INSTRUMENT,
    lineIdx: lineIdx,
    instrument: instrument
  }
}

// Reset all the beats of a line.
export const resetSteps = (lineIdx, instrument) => {
  return {
    type: actions.RESET_STEPS,
    lineIdx: lineIdx,
    instrument: instrument
  }
}

// Change the order of the instruments.
export const changeInstrumentsOrder = order => {
  return {
    type: actions.CHANGE_INSTRUMENTS_ORDER,
    order: order
  }
}

// Toggle show grid.
export const toggleGrid = () => {
  return {
    type: actions.TOGGLE_GRID
  }
}

// Change instrument images.
export const changeInstrumentIcons = iconTypes => {
  return {
    type: actions.CHANGE_INSTRUMENT_ICONS,
    icons: iconTypes
  }
}

// Collapse a line.
export const collapseLine = lineIdx => {
  return {
    type: actions.COLLAPSE_LINE,
    lineIdx: lineIdx
  }
}

// Expand a collapsed line.
export const expandLine = lineIdx => {
  return {
    type: actions.EXPAND_LINE,
    lineIdx: lineIdx
  }
}

// Change the background color of a line.
export const changeBgColor = (color, lineIdx) => {
  return {
    type: actions.CHANGE_BG_COLOR,
    lineIdx: lineIdx,
    color: color
  }
}

// Collapse all lines below a separator.
export const collapseSection = lineIdx => {
  return {
    type: actions.COLLAPSE_SECTION,
    lineIdx: lineIdx
  }
}

// Expand all lines below a separator.
export const expandSection = lineIdx => {
  return {
    type: actions.EXPAND_SECTION,
    lineIdx: lineIdx
  }
}

// Change the background clor of a section.
export const changeSectionBgColor = (lineIdx, bgColor) => {
  return {
    type: actions.CHANGE_SECTION_BG_COLOR,
    lineIdx: lineIdx,
    bgColor: bgColor
  }
}

// Remove a bar at the right of a line.
export const removeBar = lineIdx => {
  return {
    type: actions.REMOVE_BAR,
    lineIdx: lineIdx
  }
}

// Undo.
export const undo = () => {
  return {
    type: actions.UNDO
  }
}

// Redo.
export const redo = () => {
  return {
    type: actions.REDO
  }
}

// Collapse all lines.
export const collapseAll = () => {
  return {
    type: actions.COLLAPSE_ALL_LINES
  }
}

// Expand all lines.
export const expandAll = () => {
  return {
    type: actions.EXPAND_ALL_LINES
  }
}

// Load image assets.
export const loadInstrumentImages = () => {
  return dispatch => {
    // Execute all promises.
    Promise.all(getAllInstrumentAssets('icons'))
      .then(() => {
        dispatch(loadInstrumentImagesSuccess())
      })
  }
}

// On instrument images load success.
const loadInstrumentImagesSuccess = () => {
  return {
    type: actions.LOAD_INSTRUMENT_IMAGES_SUCCESS
  }
}

// Load audio assets.
export const loadInstrumentAudios = () => {
  return dispatch => {
    // Execute all promises.
    Promise.all(getAllInstrumentAssets('audio'))
      .then(() => {
        dispatch(loadInstrumentAudiosSuccess())
      })
  }
}

// On instrument audios load success.
const loadInstrumentAudiosSuccess = () => {
  return {
    type: actions.LOAD_INSTRUMENT_AUDIOS_SUCCESS
  }
}

// Start playing line.
export const startPlayingLine = (lineIdx, player) => {
  return {
    type: actions.START_PLAYING_LINE,
    lineIdx: lineIdx,
    player: player
  }
}

// Stop playing line.
export const stopPlaying = () => {
  return {
    type: actions.STOP_PLAYING
  }
}

// Toggle loop play.
export const togglePlayLoop = () => {
  return {
    type: actions.TOGGLE_PLAY_LOOP
  }
}

// On mouse over line.
export const mouseOverLine = lineIdx => {
  return {
    type: actions.MOUSE_OVER_LINE,
    lineIdx: lineIdx
  }
}

// Move line up.
export const moveLineUp = lineIdx => {
  return {
    type: actions.MOVE_LINE_UP,
    lineIdx: lineIdx
  }
}

// Move line down.
export const moveLineDown = lineIdx => {
  return {
    type: actions.MOVE_LINE_DOWN,
    lineIdx: lineIdx
  }
}

// Play metronome.
export const playMetronome = (player) => {
  return {
    type: actions.PLAY_METRONOME,
    player: player
  }
}

// Generate WAV audio success.
const generateWAVSuccess = wavDownloadLink => {
  return {
    type: actions.GENERATE_WAV_SUCCESS,
    wavDownloadLink: wavDownloadLink
  }
}

// Generate WAV audio success.
const generateWAVFailed = () => {
  return {
    type: actions.GENERATE_WAV_FAILED
  }
}

// Generate WAV audio .
export const generateWAV = (projectId, title, token) => {
  return dispatch => {
    // Start loading.
    dispatch(startLoading())
    // Perform request.
    axios(token).post(`/wav/${projectId}`)
      .then(response => {
        dispatch(generateWAVSuccess(process.env.REACT_APP_BACKEND_URL + response.data.file + '?token=' + token))
      })
      .catch(error => {
        dispatch(generateWAVFailed(getErrorMessage(error)))
        dispatch(alertDanger(`A WAV file couldn't be generated for project ${title}: ${getErrorMessage(error)}`))
      })
  }
}

// Download WAV audio .
export const downloadWAV = () => {
  return {
    type: actions.DOWNLOAD_WAV
  }
}

// Generate MP3 audio success.
const generateMP3Success = mp3DownloadLink => {
  return {
    type: actions.GENERATE_MP3_SUCCESS,
    mp3DownloadLink: mp3DownloadLink
  }
}

// Generate MP3 audio success.
const generateMP3Failed = () => {
  return {
    type: actions.GENERATE_MP3_FAILED
  }
}

// Generate MP3 audio .
export const generateMP3 = (projectId, title, token) => {
  return dispatch => {
    // Start loading.
    dispatch(startLoading())
    // Perform request.
    axios(token).post(`/mp3/${projectId}`)
      .then(response => {
        dispatch(generateMP3Success(process.env.REACT_APP_BACKEND_URL + response.data.file + '?token=' + token))
      })
      .catch(error => {
        dispatch(generateMP3Failed(getErrorMessage(error)))
        dispatch(alertDanger(`An MP3 file couldn't be generated for project ${title}: ${getErrorMessage(error)}`))
      })
  }
}

// Download MP3 audio .
export const downloadMP3 = () => {
  return {
    type: actions.DOWNLOAD_MP3
  }
}

// Play a line step.
export const playStep = (stepIdx, hitDepth) => {
  return {
    type: actions.PLAY_STEP,
    stepIdx: stepIdx,
    hitDepth: hitDepth
  }
}

// Start playing a project.
export const startPlayingProject = () => {
  return {
    type: actions.START_PLAYING_PROJECT
  }
}

// Change the line that is playing.
export const changeLinePlaying = lineIdx => {
  return {
    type: actions.CHANGE_LINE_PLAYING,
    lineIdx: lineIdx
  }
}

// Toggle metronome.
export const toggleMetronome = () => {
  return {
    type: actions.TOGGLE_METRONOME
  }
}
