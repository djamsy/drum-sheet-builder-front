// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Style.
import './ProjectNavbarButtonPreview.css'

// PNG icons.
import metronomeIcon from '../../../SheetBuilder/SheetBuilderHeader/ProjectNavbar/ProjectNavbarButton/icons/metronome.png'
import loopIcon from '../../../SheetBuilder/SheetBuilderHeader/ProjectNavbar/ProjectNavbarButton/icons/loop.png'
import analyzerIcon from '../../../SheetBuilder/SheetBuilderHeader/ProjectNavbar/ProjectNavbarButton/icons/analyzer.png'

// =============================================================================
// Constants.
// =============================================================================

// Init the icons object.
const icons = {
  metronome: metronomeIcon,
  loop: loopIcon,
  analyzer: analyzerIcon
}

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const projectNavbarButtonPreview = (props) => {
  return (
    <span className={`ProjectNavbarButtonPreview ${props.active ? 'active' : ''}`}>
      <button>
        <img src={icons[props.icon]} alt={props.icon} />
      </button>
    </span>
  )
}

// Export.
export default projectNavbarButtonPreview
