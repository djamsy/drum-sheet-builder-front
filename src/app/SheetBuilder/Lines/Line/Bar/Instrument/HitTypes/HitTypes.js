// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'

// Style
import './HitTypes.css'

// Components.
import HitType from './HitType/HitType'
import Backdrop from '../../../../../../UI/Backdrop/Backdrop'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const hitTypes = (props) => {
  if (!props.show) {
    return null
  }

  return (
    <Fragment>
      <Backdrop show={props.show} clicked={props.hideInstrumentHitTypes} transparent />
      <div className='HitTypes' style={{ top: (props.instrumentSize - 1) }}>
        {
          props.hits.map((hit, idx) => {
            return (
              <HitType
                instrumentSize={props.instrumentSize}
                changeHit={props.changeHit}
                active={idx + 1 === props.current}
                key={idx}
                hitKey={idx}
                hitName={hit.name}
                icon={hit.icons[props.instrumentIcons]}
              />
            )
          })
        }
      </div>
    </Fragment>
  )
}

// Export.
export default hitTypes
