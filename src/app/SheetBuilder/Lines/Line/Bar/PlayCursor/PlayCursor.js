// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Style.
import './PlayCursor.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const playCursor = (props) => {
  if (props.hidden) return null
  return (
    <div className='PlayCursor' style={{ left: props.left * (props.instrumentSize - 1) - 4 }} />
  )
}

// Export.
export default playCursor
