// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Style.
import './NavbarWatermark.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const navbarWatermark = (props) => (
  <div className='NavbarWatermark'>
    <span className='only-lg'>{props.content}</span>
  </div>
)

// Export.
export default navbarWatermark
