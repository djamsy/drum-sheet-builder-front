// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Style.
import './PlayProjectButtonPreview.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const playProjectButtonPreview = (props) => {
  return (
    <span className={`PlayProjectButtonPreview ${props.active ? 'active' : ''}`}>
      <button>
        {props.children}
      </button>
    </span>
  )
}

// Export.
export default playProjectButtonPreview
