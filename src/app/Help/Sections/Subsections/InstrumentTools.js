// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCaretDown } from '@fortawesome/free-solid-svg-icons'

// Components.
import Image from '../../../UI/Image/Image'
import InstrumentToolsButtonPreview from '../../ButtonPreviews/InstrumentToolsButtonPreview/InstrumentToolsButtonPreview'

// Screenshots.
import instrumentToolsScreenshot from '../../../../images/Screenshots/instrument-tools.jpg'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const section = (props) => {
  // Init the depth class.
  let depthClass = null
  if (+props.depth > 1) depthClass = `ml-${+props.depth - 1}0`

  return (
    <Fragment>
      <p className={depthClass}>On every line, for each instrument, you will see a <InstrumentToolsButtonPreview>Tools <FontAwesomeIcon icon={faCaretDown} /></InstrumentToolsButtonPreview> button at the right. By clicking on it, you will be shown a dropdown menu with several options that will affect the instrument on that line. These actions will ease and fasten the music writing process.</p>

      <ul className={depthClass}>
        <li><b>Fill each N steps</b>: These buttons will fill the beats of the instrument every N steps (for example: <InstrumentToolsButtonPreview child>Fill each step</InstrumentToolsButtonPreview>, <InstrumentToolsButtonPreview child>Fill each 2 steps</InstrumentToolsButtonPreview>...). The number of steps to fill will depend on the time signature of your project.</li>
        <li><b>Shift beats</b> right/left: These actions shift the beats of the instrument one step to the left or the right (<InstrumentToolsButtonPreview child>Shift 1 step left</InstrumentToolsButtonPreview> and <InstrumentToolsButtonPreview child>Shift 1 step right</InstrumentToolsButtonPreview>).</li>
        <li><b>Reset beats</b>: This button resets the steps of the instrument (<InstrumentToolsButtonPreview child>Reset</InstrumentToolsButtonPreview>).</li>
      </ul>

      <div className='img-container'>
        <Image src={instrumentToolsScreenshot} alt='Instrument tools (desktop view)' />
        <div className='img-legend'>Instrument tools (desktop view).</div>
      </div>
    </Fragment>
  )
}

// Export.
export default section
