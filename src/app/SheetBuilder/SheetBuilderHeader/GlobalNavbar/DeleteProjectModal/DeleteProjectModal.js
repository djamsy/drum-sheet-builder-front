// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons'

// Components.
import Modal from '../../../../UI/Modal/Modal'
import Button from '../../../../UI/Button/Button'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
export const deleteProjectModal = props => {
  return (
    <Modal
      title='Deleting project'
      show={props.modalOpen}
      modalClosed={props.hideModal}
    >
      <p>Are you sure you want to delete project <br /><b>{props.title}</b>?</p>
      <Button
        type='danger'
        clicked={() => {
          props.hideModal()
          props.deleteProject(props.projectId, props.title, props.token)
        }}
      >
        Confirm
      </Button>

      <p className='ModalTip'>
        <FontAwesomeIcon icon={faInfoCircle} /> This action cannot be undone.
      </p>
    </Modal>
  )
}

// Export.
export default deleteProjectModal
