// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Components.
import LegendInstrument from './LegendInstrument/LegendInstrument'

// Style.
import './Legend.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const legend = (props) => {
  return (
    <div className={props.firstElement ? 'Legend' : 'Legend hidden-lg'}>
      <table className='InstrumentsTable'>
        <tbody>
          {
            props.instruments.map((i, idx) => (
              <tr key={'instrument-tools-' + idx} style={{ borderColor: props.borderColor }}>
                <LegendInstrument
                  backgroundColor={props.backgroundColor}
                  borderColor={props.borderColor}
                  instrumentSize={props.instrumentSize}
                >{i}</LegendInstrument>
              </tr>
            ))
          }
        </tbody>
      </table>
    </div>
  )
}

// Export.
export default legend
