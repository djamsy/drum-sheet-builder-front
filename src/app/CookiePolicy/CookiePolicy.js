// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons'

// Style.
import './CookiePolicy.css'

// Components.
import Modal from '../UI/Modal/Modal'
import Button from '../UI/Button/Button'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const cookiePolicy = props => (
  <Modal title='Cookie policy' show={props.show} modalClosed={props.onAccept} noAutoClose>
    <div className='CookiePolicyHeader'>
      <p>
        <FontAwesomeIcon icon={faInfoCircle} /> This website uses web storage to improve user experience. To use <a href='/'>DrumSheetBuilder</a>, you must <b>read</b> and <b>accept</b> our cookie policy.
      </p>
    </div>
    <div className='CookiePolicy'>
      <h3>What is a cookie?</h3>
      <p>
        An HTTP <b>cookie</b> (also called web cookie, Internet cookie, browser cookie, or simply cookie) is a small piece of data sent from a website and stored on the user's computer by the user's web browser while the user is browsing. <a target='_blank' rel='noopener noreferrer' href='https://en.wikipedia.org/wiki/HTTP_cookie'>Click here</a> for more information.
      </p>
      <p>
        We don't use cookies at <a href='/'>DrumSheetBuilder</a>, but <b>web storage</b> variables. Web storage supports persistent data storage, similar to cookies but with a greatly enhanced capacity and no information stored in the HTTP request header. <a target='_blank' rel='noopener noreferrer' href='https://en.wikipedia.org/wiki/Web_storage'>Click here</a> for more information.
      </p>
      <p>
        Web storage variables are not cookies, but are very similar, and you must explicitly consent its usage.
      </p>
      <h3>What web storage variables do we create?</h3>
      <p>
        We use web storage just for authentication purposes. The reason why your session remains open is because we store the following cookies in your browser:
      </p>
      <ul>
        <li>A variable for storing your access token.</li>
        <li>A variable for storing your user ID.</li>
        <li>A variable for storing the expiration time of your token.</li>
      </ul>
      <p>
        As a result, you will be logged in automatically every time you visit <a href='/'>DrumSheetBuilder</a> and won't be asked for your credentials. The access token lasts exactly 7 days, and will expire afterwards. When it expires, you will be asked to login again.
      </p>
      <p>
        We also store the status of your consent to this policy in another web storage variable, in order to not show it again once you have accepted it.
      </p>
      <h3>How can I delete cookies and web storage variables?</h3>
      <p>
        Cookies and web storage variables can be deleted from any browser. To do so, you must search for the specific documentation for your browser on how to delete cookies and web storage.
      </p>
    </div>
    <Button type='primary' clicked={props.onAccept}>
      Accept
    </Button>
  </Modal>
)

// Export.
export default cookiePolicy
