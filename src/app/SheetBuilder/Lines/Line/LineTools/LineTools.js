// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// Style
import './LineTools.css'

// Components.
import ToolsButton from './ToolsButton/ToolsButton'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const lineTools = (props) => {
  return (
    <div className='LineTools'>
      {
        props.elements.map((element, idx) => (
          <ToolsButton
            key={idx}
            backgroundColor={props.buttonsColor}
            hoverBackgroundColor={props.buttonsBorderColor}
            type='default'
            title='Reset line'
            clicked={() => element.clicked(props.lineIdx)}
          >
            <FontAwesomeIcon icon={element.icon} />
            <span className='ToolsButtonText only-sm'>{element.labelShort}</span>
            <span className='ToolsButtonText only-lg'>{element.label}</span>
          </ToolsButton>
        ))
      }
    </div>
  )
}

// Export.
export default lineTools
