// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faTimes,
  faCaretLeft,
  faCaretDown,
  faFillDrip,
  faSync,
  faCopy,
  faPlus,
  faMinus,
  faCaretUp,
  faHeading,
  faPlay,
  faStop
} from '@fortawesome/free-solid-svg-icons'

// Components.
import PlayButtonPreview from '../../ButtonPreviews/PlayButtonPreview/PlayButtonPreview'
import LineToolsButtonPreview from '../../ButtonPreviews/LineToolsButtonPreview/LineToolsButtonPreview'
import LineHeaderButtonPreview from '../../ButtonPreviews/LineHeaderButtonPreview/LineHeaderButtonPreview'
import HTEMButtonPreview from '../../ButtonPreviews/HTEMButtonPreview/HTEMButtonPreview'
import Callout from '../../../UI/Callout/Callout'
import KeyboardKey from '../../../UI/KeyboardKey/KeyboardKey'

// Constants.
import { shortcuts } from '../../../../shared/constants'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const section = (props) => {
  // Init the depth class.
  let depthClass = null
  if (+props.depth > 1) depthClass = `ml-${+props.depth - 1}0`

  return (
    <Fragment>
      <p className={depthClass}>Above every line, you will see a <b>tools menu</b> with buttons that will allow you to perform some actions over the project or the line itself. Most of them will let you write music much faster and avoid time waste by creating lines that are similar.</p>

      <ul className={depthClass}>
        <li><b>Resetting</b> a line: To reset a line, click the <LineToolsButtonPreview><FontAwesomeIcon icon={faSync} /> Reset line</LineToolsButtonPreview> button.</li>
        <li><b>Copying</b> a line <b>below</b>: To copy a line below current, click the <LineToolsButtonPreview><FontAwesomeIcon icon={faCopy} /> Copy below</LineToolsButtonPreview> button.</li>
        <li><b>Copying</b> a line <b>at the end</b>: To copy a line and insert it at the end of the project, click the <LineToolsButtonPreview><FontAwesomeIcon icon={faCopy} /> Copy at the end</LineToolsButtonPreview> button.</li>
        <li>Inserting a <b>new line below</b>: To insert an empty line below current, click the <LineToolsButtonPreview><FontAwesomeIcon icon={faPlus} /> Insert below</LineToolsButtonPreview> button.</li>
        <li>Inserting a <b>new line above</b>: To insert an empty line above current, click the <LineToolsButtonPreview><FontAwesomeIcon icon={faPlus} /> Insert above</LineToolsButtonPreview> button.</li>
        <li><b>Removing</b> a <b>bar</b>: To remove the last bar of a line, click the <LineToolsButtonPreview><FontAwesomeIcon icon={faMinus} /> Remove bar</LineToolsButtonPreview> button.</li>
        <li>Inserting a new <b>separator</b> above: To insert a new separator above current line and create a new section, click the <LineToolsButtonPreview><FontAwesomeIcon icon={faHeading} /> Add separator above</LineToolsButtonPreview> button.</li>
        <li><b>Moving</b> a line <b>down</b>: To move a line down, click the <LineToolsButtonPreview><FontAwesomeIcon icon={faCaretDown} /> Move down</LineToolsButtonPreview> button.</li>
        <li><b>Moving</b> a line <b>up</b>: To move a line up, click the <LineToolsButtonPreview><FontAwesomeIcon icon={faCaretUp} /> Move up</LineToolsButtonPreview> button.</li>
      </ul>

      <div className={depthClass}>
        <Callout type='Info' label='TIP'>
          You can add a new line at the bottom of your project by clicking {shortcuts.addLine.keys.map((key, idx) => <KeyboardKey key={idx} idx={idx}>{key}</KeyboardKey>)}, and insert a line above the one under your cursor by clicking {shortcuts.insertLineAbove.keys.map((key, idx) => <KeyboardKey key={idx} idx={idx}>{key}</KeyboardKey>)}.
        </Callout>
      </div>

      <p className={depthClass}>You will also be able to customize the style of your lines with the buttons at the <b>top left</b> and <b>top right</b> of them. These buttons will be visible even if the line is collapsed.</p>

      <ul className={depthClass}>
        <li><b>Deleting</b> a line: You can delete a line by clicking the delete button (<LineHeaderButtonPreview><FontAwesomeIcon icon={faTimes} /></LineHeaderButtonPreview>) at the top left of the line.</li>
        <li><b>Collapsing/expanding</b> a line: You can collapse or expand lines by clicking the arrow buttons at the top right of the line (<LineHeaderButtonPreview><FontAwesomeIcon icon={faCaretLeft} /></LineHeaderButtonPreview> and <LineHeaderButtonPreview><FontAwesomeIcon icon={faCaretDown} /></LineHeaderButtonPreview>).</li>
        <li><b>Changing the background color</b> of a line: You can change the background color of a line by clicking the paint button at the top right of the line <LineHeaderButtonPreview><FontAwesomeIcon icon={faFillDrip} /></LineHeaderButtonPreview>). A color selector will be opened, and you will be able to choose a color from there.</li>
      </ul>

      <div className={depthClass}>
        <Callout type='Info' label='TIP'>
          You can collapse, expand and delete a line by clicking keys {shortcuts.collapseLine.keys.map((key, idx) => <KeyboardKey key={idx} idx={idx}>{key}</KeyboardKey>)}, {shortcuts.expandLine.keys.map((key, idx) => <KeyboardKey key={idx} idx={idx}>{key}</KeyboardKey>)} and {shortcuts.removeLine.keys.map((key, idx) => <KeyboardKey key={idx} idx={idx}>{key}</KeyboardKey>)} respectively while having your cursor over it.
        </Callout>
      </div>

      <p className={depthClass}>Two buttons at the top of the line, right near the title, will let you play it and toggle the <i>Hit Type Editing Mode</i> (see section <i>Changing type of hit</i>).</p>

      <ul className={depthClass}>
        <li><b>Playing</b> a line: You can play/stop a line by clicking on the player icons above the line content (<PlayButtonPreview><FontAwesomeIcon icon={faPlay} /></PlayButtonPreview> and <PlayButtonPreview active><FontAwesomeIcon icon={faStop} /></PlayButtonPreview>).</li>
        <li>Toggle the <b>Hit Type Editing Mode</b>: You can toggle its value by clicking over the HTEM button (<HTEMButtonPreview active>HTEM ON</HTEMButtonPreview> and <HTEMButtonPreview>HTEM OFF</HTEMButtonPreview>). To learn how to use this feature, see section <i>Cahnging type of hit</i>.</li>
      </ul>
    </Fragment>
  )
}

// Export.
export default section
