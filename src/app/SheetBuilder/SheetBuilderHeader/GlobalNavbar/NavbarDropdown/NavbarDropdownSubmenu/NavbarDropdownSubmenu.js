// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// Style.
import './NavbarDropdownSubmenu.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const navbarDropdownSubmenu = (props) => {
  // If no need to be shown, return null.
  if (!props.show) return null

  return (
    <div className='NavbarDropdownSubmenu'>
      {
        props.options.map((option, idx) => {
          // Init the icon.
          let icon = null
          if (option.icon) icon = <span className='DropdownOptionPre'><FontAwesomeIcon icon={option.icon} /></span>

          // Init the shortcut.
          let shortcut = null
          if (option.shortcut) shortcut = <small>{option.shortcut.join(' ')}</small>

          // Init the action to perform.
          let action = () => null
          if (!option.disabled) {
            action = () => {
              props.onClose()
              option.clicked()
            }
          }

          return (
            <div className={'DropdownOption' + (option.disabled ? ' disabled' : '')} key={idx} onClick={action}>
              {icon}
              {option.label}
              <span className='DropdownOptionPost'>
                {shortcut}
              </span>
            </div>
          )
        })
      }
    </div>
  )
}

// Export.
export default navbarDropdownSubmenu
