// Custom logger.
const log = function () {
  if (process.env.NODE_ENV === 'development') console.log.apply(console, arguments)
}

// Export.
export default log
