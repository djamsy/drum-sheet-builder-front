// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Style.
import './LegendInstrument.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const legendInstrument = (props) => {
  // Calculate font size responsively.
  let fontSize = props.instrumentSize > 15
    ? 11 + 'px'
    : (props.instrumentSize - 4) + 'px'

  return (
    <td className='LegendInstrument' style={{ height: props.instrumentSize, fontSize: fontSize, backgroundColor: props.backgroundColor }}>
      {props.children}
    </td>
  )
}

// Export.
export default legendInstrument
