// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'
import ReactDOM from 'react-dom'
import thunk from 'redux-thunk'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { createStore, compose, applyMiddleware, combineReducers } from 'redux'

// Style.
import './index.css'

// Components.
import App from './app/App'

// Other scripts.
import * as serviceWorker from './serviceWorker'

// Reducers.
import projectReducer from './store/reducers/project'
import alertReducer from './store/reducers/alert'
import authReducer from './store/reducers/auth'

// =============================================================================
// Main.
// =============================================================================

// Create compose enhancers.
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

// Combine reducers.
const rootReducer = combineReducers({
  project: projectReducer,
  alert: alertReducer,
  auth: authReducer
})

// Create store.
const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)))

// Declare app inside Redux provider.
const app = (
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
)

// Render the <Layout/> element.
ReactDOM.render(app, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA.
serviceWorker.register()
