// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Component } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'

// Style.
import './Bar.css'

// Components.
import Instrument from './Instrument/Instrument'
import PlayCursor from './PlayCursor/PlayCursor'

// Redux actions.
import * as actions from '../../../../../store/actions/index'

// Shared.
import { getCellBorder } from '../../../../../shared/functions'
import { timeSignatures } from '../../../../../shared/constants'
import log from '../../../../../shared/logger'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateful component declaration.
class Bar extends Component {
  // Class method, inherited from Component.
  shouldComponentUpdate (nextProps) {
    // Update only when this line is changed.
    let shouldUpdate = !_.isEqual(nextProps.lines[nextProps.lineIdx].content[nextProps.idx], this.props.lines[this.props.lineIdx].content[this.props.idx]) ||
      nextProps.hitTypeEditingMode !== this.props.hitTypeEditingMode ||
      nextProps.instrumentOrder !== this.props.instrumentOrder ||
      nextProps.instrumentIcons !== this.props.instrumentIcons ||
      nextProps.instrumentSize !== this.props.instrumentSize ||
      nextProps.showGrid !== this.props.showGrid ||
      nextProps.lastElement !== this.props.lastElement ||
      (
        nextProps.stepPlaying !== this.props.stepPlaying &&
        nextProps.stepPlaying >= nextProps.stepRangeMin &&
        nextProps.stepPlaying <= nextProps.stepRangeMax + 1 &&
        nextProps.linePlaying === this.props.lineIdx
      ) ||
      (
        this.props.stepPlaying !== null &&
        nextProps.stepPlaying === null
      ) || /* end of play */
      (
        this.props.stepPlaying !== null &&
        nextProps.stepPlaying === 0 &&
        !this.props.projectPlaying &&
        nextProps.linePlaying === this.props.lineIdx
      ) /* loop mode */
    if (shouldUpdate) {
      log(`Re-rendering bar ${this.props.idx} from line ${this.props.lineIdx}`)
    }
    return shouldUpdate
  }

  // Class method, inherited from Component.
  render () {
    return (
      <div className={this.props.lastElement ? 'Bar no-border' : 'Bar'}>
        <PlayCursor
          left={(this.props.stepPlaying || 0) - this.props.barWidth * this.props.idx}
          instrumentSize={this.props.instrumentSize}
          hidden={this.props.stepPlaying < this.props.stepRangeMin || this.props.stepPlaying > this.props.stepRangeMax}
        />
        <table>
          <tbody>
            {
              Object.keys(this.props.bar).map((instrument, instrumentIdx) => {
                return (
                  <tr key={instrumentIdx}>
                    {
                      this.props.bar[instrument].map((beat, beatIdx) => {
                        return (
                          <td
                            style={{
                              height: this.props.instrumentSize - 1,
                              width: this.props.instrumentSize - 1
                            }}
                            key={beatIdx}
                          >
                            <Instrument
                              borderLeft={getCellBorder(beatIdx, this.props.timeSignature, this.props.showGrid)}
                              disableClick={timeSignatures[this.props.timeSignature].disabledBeats && timeSignatures[this.props.timeSignature].disabledBeats.indexOf(beatIdx) !== -1}
                              instrumentIcons={this.props.instrumentIcons}
                              instrumentOrder={this.props.instrumentOrder}
                              instrumentSize={this.props.instrumentSize}
                              hitTypeEditingMode={this.props.hitTypeEditingMode}
                              instrument={instrument}
                              type={this.props.bar[instrument][beatIdx]}
                              changeHit={(hitType) => this.props.setHitType(this.props.lineIdx, this.props.idx, instrument, beatIdx, hitType)}
                              clicked={() => this.props.clickBeat(this.props.lineIdx, this.props.idx, instrument, beatIdx)}
                            />
                          </td>
                        )
                      })
                    }
                  </tr>
                )
              })
            }
          </tbody>
        </table>
      </div>
    )
  }
}

// ============================================================================
// Connect with Redux and export.
// ============================================================================

// State mapping.
const mapStateToProps = state => {
  return {
    projectPlaying: state.project.projectPlaying,
    linePlaying: state.project.linePlaying,
    stepPlaying: state.project.stepPlaying,
    instrumentOrder: state.project.instrumentOrder,
    instrumentIcons: state.project.instrumentIcons,
    instrumentSize: state.project.instrumentSize,
    timeSignature: state.project.timeSignature,
    showGrid: state.project.showGrid,
    lines: state.project.lines
  }
}

// Action mapping.
const mapDispatchToProps = dispatch => {
  return {
    clickBeat: (lineIdx, barIdx, instrument, beatIdx) => dispatch(actions.clickBeat(lineIdx, barIdx, instrument, beatIdx)),
    setHitType: (lineIdx, barIdx, instrument, beatIdx, hitType) => dispatch(actions.setHitType(lineIdx, barIdx, instrument, beatIdx, hitType))
  }
}

// Export.
export default connect(mapStateToProps, mapDispatchToProps)(Bar)
