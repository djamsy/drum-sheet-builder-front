// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Style.
import './Callout.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const callout = (props) => {
  // Init the title, if existing.
  let title = null
  if (props.title) {
    title = <h1>{props.title}</h1>
  }

  // Init the label, if existing.
  let label = null
  if (props.label) {
    label = <span className='CalloutLabel'>{props.label}</span>
  }

  return (
    <div className={'Callout ' + props.type}>
      {title}
      {label}<span>{props.children}</span>
    </div>
  )
}

// Export.
export default callout
