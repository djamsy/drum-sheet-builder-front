// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Style.
import './NavbarButton.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const navbarButton = (props) => {
  // Init the 'float' attribute.
  let float = null
  if (props.left) float = 'left'
  else if (props.right) float = 'right'

  return (
    <div className='NavbarButton' style={{ float: float }}>
      <button
        onClick={props.clicked}
        type='button'
        disabled={props.disabled}
      >
        {props.children}
      </button>
    </div>
  )
}

// Export.
export default navbarButton
