// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'

// Components.
import KeyboardKey from '../../UI/KeyboardKey/KeyboardKey'
import Callout from '../../UI/Callout/Callout'

// Screenshots.
import NavbarButtonPreview from '../ButtonPreviews/NavbarButtonPreview/NavbarButtonPreview'

// Constants.
import { shortcuts } from '../../../shared/constants'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const section = (props) => {
  // Init the depth class.
  let depthClass = null
  if (+props.depth > 1) depthClass = `ml-${+props.depth - 1}0`

  return (
    <Fragment>
      <p className={depthClass}>You can save your project by clicking <NavbarButtonPreview>File</NavbarButtonPreview> - <NavbarButtonPreview>Save</NavbarButtonPreview>, on the main navbar. You will be shown an alert message at the bottom of the page that tells you if the action succeeded or failed</p>

      <div className={depthClass}>
        <Callout type='Info' label='TIP'>
          You can also save your project by clicking {shortcuts.saveProject.keys.map((key, idx) => <KeyboardKey key={idx} idx={idx}>{key}</KeyboardKey>)} once you have started a project or opened an existing one.
        </Callout>
      </div>

      <p className={depthClass}>In case the application fails to save your project, make sure you have provided a correct value for all the main fields (<i>Title</i>, <i>Time signature</i> and <i>BPM</i>).</p>

      <p className={depthClass}>As of versions 1.x.x, auto-save is not implemented yet. Don't forget to save your project periodically!</p>
    </Fragment>
  )
}

// Export.
export default section
