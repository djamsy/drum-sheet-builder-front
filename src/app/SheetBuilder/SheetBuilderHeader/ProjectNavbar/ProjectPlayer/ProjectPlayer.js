// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlay, faStop } from '@fortawesome/free-solid-svg-icons'

// Styles.
import './ProjectPlayer.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const projectPlayer = (props) => {
  // Init the action to perform.
  let action = props.play
  let icon = faPlay
  let extraClass = ''

  if (props.player && props.projectPlaying) {
    action = () => {
      // Update the played step.
      if (props.onStopPlaying) props.onStopPlaying()
      // Stop playing.
      props.stopPlaying()
    }
    icon = faStop
    extraClass = 'active'
  }

  return (
    <div className={`ProjectPlayerButton ${extraClass}`}>
      <button type='button' onClick={action}>
        <FontAwesomeIcon icon={icon} />
      </button>
    </div>
  )
}

// Export.
export default projectPlayer
