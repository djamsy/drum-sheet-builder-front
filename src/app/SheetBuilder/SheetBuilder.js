// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Component } from 'react'
import { Redirect, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

// Style.
import './SheetBuilder.css'

// Components.
import Alert from '../UI/Alert/Alert'
import Lines from './Lines/Lines'
import SheetBuilderHeader from './SheetBuilderHeader/SheetBuilderHeader'
import Footer from '../Footer/Footer'

// Redux actions.
import * as actions from '../../store/actions/index'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateful component declaration.
class SheetBuilder extends Component {
  // Class method, inherited from Component.
  render () {
    // Check authentication state.
    if (!this.props.isAuth) {
      // Change the redirection path.
      this.props.setAuthRedirectPath('/')
      // Redirect to /login.
      return <Redirect to='/login' />
    }

    // Init the alert message.
    let alert = null
    if (this.props.showAlert) {
      alert = (
        <Alert type={this.props.alertType} message={this.props.alertMessage} hideAlert={this.props.hideAlert} />
      )
    }

    return (
      <div className='SheetBuilder'>
        <SheetBuilderHeader />
        <Lines />
        <Footer />
        {alert}
      </div>
    )
  }
}

// ============================================================================
// Connect with Redux and export.
// ============================================================================

// State mapping.
const mapStateToProps = state => {
  return {
    alertType: state.alert.type,
    alertMessage: state.alert.message,
    showAlert: state.alert.message !== null
  }
}

// Action mapping.
const mapDispatchToProps = dispatch => {
  return {
    hideAlert: () => dispatch(actions.hideAlert()),
    setAuthRedirectPath: path => dispatch(actions.setAuthRedirectPath(path))
  }
}

// Export.
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SheetBuilder))
