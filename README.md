This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Deployment

You can deploy the server with the `start` script, that will launch the development or production server depending on the environment.

### Development

#### 1. Modify `.env` file

Copy the default development environment file, `.env.development`.

```
cp .env.development .env
```

#### 2. Start the development server

Start with the following:

```
npm start
```
    
The development server is provided by the `create-react-app` module and runs on port **3000**.

### Production

#### 1. Modify `.env` file

Copy the default production environment file, `.env.production`.

```
cp .env.production .env
```

#### 2. Create production build

```
npm run build
```
    
The production build will be created under directory `build/`, and when finished, it will replace the old `dist/` directory that contains the production build. This is done this way to prevent the server from being down while performing the build.

#### 3. Start the production server

```
npm start
```
    
The production server is a simple Koa.js server that serves static files in the `dist/` directory and forwards all other endpoints to the `index.html` file for React to handle 404 errors.

The server will listen on port **3000**.
