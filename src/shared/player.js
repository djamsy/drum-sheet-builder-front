// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import _ from 'lodash'

// Shared.
import {
  getInstrumentsAsObject,
  calculateTBB,
  calculateHitDepth,
  calculateDOB, buildMetronomeContent
} from './functions'
import log from './logger'
import { defaultMetronomeAudioLib } from './constants'

// =============================================================================
// Functions.
// =============================================================================

// Declare class.
class Player {
  // Constructor.
  constructor (options) {
    this.compressedLine = options.compressedLine
    this.audioLib = options.audioLib
    this.bpm = options.bpm
    this.timeSignature = options.timeSignature
    this.onStepPlayed = options.onStepPlayed
    this.onStopPlaying = options.onStopPlaying
    this.infinite = options.infinite || false
    this.metronome = options.metronome || false
    // Init.
    this.init()
  }

  // Init.
  init = () => {
    // Init the audios object.
    this.audio = {}
    // Restart pointer.
    this.pointer = 0
    // Calculate the Time Between Beats (TBB).
    this.tbb = calculateTBB(this.bpm)
    // Calculate the hit depth.
    this.hitDepth = calculateHitDepth(this.timeSignature)
    // Calculate the Duration Of a Beat (DOB), in milliseconds.
    this.dob = calculateDOB(this.tbb, this.hitDepth)
    // Get instruments as an object, easier to read on every iteration.
    this.instruments = getInstrumentsAsObject(this.compressedLine, this.metronome)
    // Build the new lines object with metronome.
    this.line = this.buildLinesWithMetronome(this.compressedLine)
    // Update the value of nSteps.
    this.nSteps = this.line[_.head(_.keys(this.line))].length
    // Load the audios object.
    this.buildAudiosObject()
    // Init the number of audios to load.
    this.nAudioFilesToLoad = 0
    log(`Player initiated ${this.metronome ? 'with' : 'without'} metronome.`)
    log(`[player] BPM: ${this.bpm}.`)
    log(`[player] DOB: ${this.dob.toFixed(2)} milliseconds.`)
    log(`[player] TBB: ${this.tbb.toFixed(2)} milliseconds.`)
  }

  // Build the lines object with metronome.
  buildLinesWithMetronome = compressedLine => {
    // If metronome is not to be played, return the lines without modification.
    if (!this.metronome) return compressedLine
    // Get the metronome content.
    let metronomeContent = buildMetronomeContent(this.timeSignature)
    // If the compressed line is empty, return only the metronome content.
    if (_.isEmpty(_.keys(compressedLine))) return metronomeContent
    // Calculate the bucle step size and limits.
    let stepSize = metronomeContent[_.head(_.keys(metronomeContent))].length
    let topLimit = compressedLine[_.head(_.keys(compressedLine))].length
    // Merge the lines with the metronome content.
    for (let i = 0; i < topLimit; i += stepSize) {
      // Loop through every key of the metronome object.
      for (let metronomeInstrKey in metronomeContent) {
        if (metronomeContent.hasOwnProperty(metronomeInstrKey)) {
          // Ensure the key exists in the compressed line object.
          if (!compressedLine[metronomeInstrKey]) compressedLine[metronomeInstrKey] = []
          // Concatenate with previous.
          compressedLine[metronomeInstrKey] = compressedLine[metronomeInstrKey].concat(metronomeContent[metronomeInstrKey])
        }
      }
    }
    return compressedLine
  }

  // Build the audios object.
  buildAudiosObject = () => {
    // Build the audios object.
    for (let label in this.instruments) {
      if (this.instruments.hasOwnProperty(label)) {
        // Init the instrument attribute.
        this.audio[label] = this.line[label].map(hit => {
          if (hit !== 0) {
            if (this.instruments[label][hit - 1].audio[this.audioLib]) {
              return new Audio(this.instruments[label][hit - 1].audio[this.audioLib])
            } else {
              // If no audio in current lib found, it means that it's the metronome audio lib.
              return new Audio(this.instruments[label][hit - 1].audio[defaultMetronomeAudioLib])
            }
          }
          return null
        })
      }
    }
  }

  // Load all the audio files.
  loadAudios = () => {
    return new Promise(resolve => {
      log(`[player] Loading audio files.`)
      // Init the number of loaded audios.
      let nAudiosLoaded = 0
      // Function that resolves the promise, if possible.
      let resolveIfPossible = () => {
        if (this.nAudioFilesToLoad === ++nAudiosLoaded) return resolve()
      }
      // For every element in the audio object.
      for (let label in this.audio) {
        if (this.audio.hasOwnProperty(label)) {
          for (let i in this.audio[label]) {
            if (this.audio[label].hasOwnProperty(i)) {
              if (this.audio[label][i]) {
                // Increment the value of the number of audios to load.
                this.nAudioFilesToLoad++
                // Load the audio file.
                this.audio[label][i].addEventListener('canplaythrough', resolveIfPossible)
                this.audio[label][i].load()
              }
            }
          }
        }
      }
    })
  }

  // Play audio.
  play () {
    return new Promise(resolve => {
      // Load audio files.
      this.loadAudios()
        .then(() => {
          log(`[player] Audio files loaded.`)
          // Start playing audio stream.
          log(`[player] Playing audio.`)
          this.track = setInterval(this.playChunk(), this.dob)
          resolve(this.track)
        })
    })
  }

  // Stop audio.
  stop () {
    log(`[player] Stopping audio.`)
    clearInterval(this.track)
  }

  // Play audio chunk.
  playChunk () {
    return () => {
      // If audio track has come to an end, clear interval and resolve.
      if (this.pointer === this.nSteps) {
        this.pointer = 0
        // Stop playing.
        if (!this.infinite) return this.onStopPlaying()
      }
      // Update the played step.
      if (this.onStepPlayed) this.onStepPlayed(this.pointer)
      // Play every instrument.
      for (let label in this.line) {
        if (this.line.hasOwnProperty(label)) {
          // If previous audio track must be stopped, stop it.
          if (this.pointer > 0) {
            // If current hit is not empty.
            if (this.line[label][this.pointer] !== 0) {
              // If the instrument must cut previous.
              if (this.instruments[label][this.line[label][this.pointer] - 1].cutPrevious) {
                // Cut the last one that is not paused.
                for (let i = this.pointer; i > 0; i--) {
                  if (this.audio[label][i] && !this.audio[label][i].paused) {
                    this.audio[label][i].pause()
                    break
                  }
                }
              }
            }
          }
          // If instrument is not empty.
          if (this.line[label][this.pointer] !== 0) {
            // First, pause it if infinite mode is enabled.
            if (this.infinite) {
              this.audio[label][this.pointer].pause()
              this.audio[label][this.pointer].currentTime = 0
            }
            // Drag.
            if (this.instruments[label][this.line[label][this.pointer] - 1].drag) {
              let currentPointer = this.pointer
              // log(`DRAG (1st)! Pointer: ${currentPointer}. DOB: ${this.dob / 2}.`)
              setTimeout(() => {
                // log(`DRAG (2nd)! Pointer: ${currentPointer}. DOB: ${this.dob / 2}.`)
                this.audio[label][currentPointer].pause()
                this.audio[label][currentPointer].currentTime = 0
                this.audio[label][currentPointer].play()
              }, this.dob / 2)
            }
            // log(`Moment: ${this.pointer}. Playing instrument ${label}`)
            this.audio[label][this.pointer].play()
          }
        }
      }
      // Increment pointer.
      this.pointer++
    }
  }
}

// Export.
export default Player
