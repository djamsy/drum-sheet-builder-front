// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInfoCircle, faCheck } from '@fortawesome/free-solid-svg-icons'

// Style.
import './FormRow.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const formRow = (props) => {
  // Init the validation message (signup mode only).
  let validationMsg = null
  if (props.showValidationMsg) {
    // Init the error icon.
    let errorIcon = props.error || !props.touched ? faInfoCircle : faCheck
    // Populate the validation message.
    validationMsg = (
      <div className={'ValidationMessage ' + (props.error || !props.touched ? 'Invalid' : 'Valid')}>
        <span className={props.error || !props.touched ? 'Invalid' : 'Valid'}><FontAwesomeIcon icon={errorIcon} /></span>
        {props.validationMsg}
      </div>
    )
  }

  return (
    <div className='FormRow'>
      <div className='FormRowInput'>
        <input
          disabled={props.disable}
          onKeyPress={props.keyPress}
          value={props.value}
          placeholder={props.label || 'Type something here'}
          onChange={props.changed}
          type={props.type || 'text'}
        />
        <div className='FormRowAddon'>
          <FontAwesomeIcon icon={props.icon} />
        </div>
      </div>
      {validationMsg}
    </div>
  )
}

// Export.
export default formRow
