// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// Style.
import './FileButton.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const fileButton = (props) => (
  <button
    type='button'
    className={`btn btn-file btn-${props.type}`}
    onClick={props.clicked}
    disabled={props.disable}
  >
    <span className='btn-file-icon'><FontAwesomeIcon icon={props.icon} /></span> {props.fileName}
  </button>
)

// Export.
export default fileButton
