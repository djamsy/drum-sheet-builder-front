// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faKeyboard, faUserSecret } from '@fortawesome/free-solid-svg-icons'

// Components.
import ShortcutsModal from './ShortcutsModal/ShortcutsModal'
import PrivacyPolicy from '../PrivacyPolicy/PrivacyPolicy'

// Style.
import './Footer.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateful component declaration.
class Footer extends Component {
  state = {
    shortcutsModalOpen: false,
    privacyPolicyModalOpen: false
  }

  // Open the privacy policy modal.
  showPrivacyPolicyModal = () => {
    this.setState({
      privacyPolicyModalOpen: true
    })
  }

  // Close the privacy policy modal.
  hidePrivacyPolicyModal = () => {
    this.setState({
      privacyPolicyModalOpen: false
    })
  }

  // Open the shortcuts modal.
  showShortcutsModal = () => {
    this.setState({
      shortcutsModalOpen: true
    })
  }

  // Close the shortcuts modal.
  hideShortcutsModal = () => {
    this.setState({
      shortcutsModalOpen: false
    })
  }

  render () {
    // Init the shortcuts modal.
    let shortcutsModal = null
    if (this.state.shortcutsModalOpen) {
      shortcutsModal = <ShortcutsModal modalOpen={this.state.shortcutsModalOpen} hideModal={this.hideShortcutsModal} />
    }

    // Init the privacy policy modal.
    let privacyPolicyModal = null
    if (this.state.privacyPolicyModalOpen) {
      privacyPolicyModal = <PrivacyPolicy modalOpen={this.state.privacyPolicyModalOpen} hideModal={this.hidePrivacyPolicyModal} />
    }

    return (
      (
        <footer>
          {shortcutsModal}
          {privacyPolicyModal}
          <div className='Footer'>
            <div className='FooterWatermark'>
              <div>
                <a href='/'>DrumSheet<b>Builder</b></a>
                <small>Version 2.12.5</small>
              </div>
              <p>
                By <a href='https://bitbucket.org/djamsy/' target='_blank' rel='noopener noreferrer'>@djamsy</a>
              </p>
            </div>
            <div className='FooterLinks'>
              <div className='FooterLinksSite' onClick={this.showShortcutsModal}>
                <FontAwesomeIcon icon={faKeyboard} /> <span className='hidden-xs'>Shortcuts</span>
              </div>
              <div className='FooterLinksSite' onClick={this.showPrivacyPolicyModal}>
                <FontAwesomeIcon icon={faUserSecret} /> <span className='hidden-xs'>Privacy policy</span>
              </div>
            </div>
          </div>
        </footer>
      )
    )
  }
}

// Export.
export default Footer
