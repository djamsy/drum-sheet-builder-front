// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'

// Components.
import Image from '../../UI/Image/Image'

// Constants.
import { instruments } from '../../../shared/constants'

// Screenshots.
import beatClickScreenshot from '../../../images/Screenshots/beat-click.jpg'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const section = (props) => {
  // Init the depth class.
  let depthClass = null
  if (+props.depth > 1) depthClass = `ml-${+props.depth - 1}0`

  return (
    <Fragment>
      <p className={depthClass}>You can start writing music by <b>clicking</b> the blank beats. Whenever you do so, the icon of the instrument will be printed.</p>

      <ul className={depthClass}>
        <li>You can <b>reset</b> the beat by clicking it again.</li>
        <li>If you click a blank beat, the <b>default hit</b> will be printed.</li>
        <li>To <b>change the type of hit</b>, see section <i>Changing type of hit</i>.</li>
      </ul>

      <div className='img-container'>
        <Image src={beatClickScreenshot} alt='Clicking a beat (desktop view)' />
        <div className='img-legend'>Clicking a beat (desktop view).</div>
      </div>

      <p className={depthClass}>The {instruments.length} instruments represented are the basic {instruments.length} instruments present in every drum set: <b>{
        instruments.map(i => i.name).join(', ')
      }</b>.</p>

    </Fragment>
  )
}

// Export.
export default section
