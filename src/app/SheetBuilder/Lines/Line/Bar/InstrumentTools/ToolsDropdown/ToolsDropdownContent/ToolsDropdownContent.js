// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Style.
import './ToolsDropdownContent.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const toolsDropdownContent = (props) => {
  if (!props.show) {
    return null
  }
  return (
    <div className='ToolsDropdownContent' style={{ height: props.instrumentSize }}>
      {
        props.fillEachStepsOptions.map((e, idx) => (
          <button
            key={idx}
            style={{ backgroundColor: props.backgroundColor }}
            className='ToolsDropdownButton'
            onClick={() => {
              props.hideDropdownContent()
              props.fillEachSteps(e, props.lineIdx, props.instrument)
            }}
          >
            Fill each {e === 1 ? 'step' : e + ' steps'}
          </button>
        ))
      }
      <button
        style={{ backgroundColor: props.backgroundColor }}
        className='ToolsDropdownButton'
        onClick={() => {
          props.hideDropdownContent()
          props.shiftStepsLeft(props.lineIdx, props.instrument)
        }}
      >
        Shift 1 step left
      </button>
      <button
        style={{ backgroundColor: props.backgroundColor }}
        className='ToolsDropdownButton'
        onClick={() => {
          props.hideDropdownContent()
          props.shiftStepsRight(props.lineIdx, props.instrument)
        }}
      >
        Shift 1 step right
      </button>
      <button
        style={{ backgroundColor: props.backgroundColor }}
        className='ToolsDropdownButton'
        onClick={() => {
          props.hideDropdownContent()
          props.resetSteps(props.lineIdx, props.instrument)
        }}
      >
        Reset
      </button>
    </div>
  )
}

// Export.
export default toolsDropdownContent
