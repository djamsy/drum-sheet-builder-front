// A script to :
// - Automatically change version of package.json.
// - Append the version log to package.json.
// - Change the version name in the footer.

// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
const fs = require('fs')
const path = require('path')
const chalk = require('chalk')

// =============================================================================
// Constants.
// =============================================================================

// Valid upgrade types.
const upgradeTypes = ['patch', 'minor', 'major']

// Package.json location.
let packageJsonLocation = path.resolve(__dirname, '../package.json')

// Footer.js location.
const footerLocation = path.resolve(__dirname, '../src/app/Footer/Footer.js')

// Regular expressions.
const versionRegExp = /"[\s]*version[\s]*"[\s]*:[\s]*"[\d]+\.[\d]+\.[\d]+"/
const versionDeclarationRegExp = /"[\s]*version[\s]*"[\s]*:[\s]*/
const footerVersionRegExp = /Version [\d]+\.[\d]+\.[\d]+/

// =============================================================================
// Class definition.
// =============================================================================

class Project {
  constructor () {
    // Load package.json as a string.
    this.packageJson = fs.readFileSync(packageJsonLocation, 'utf8')
    // Load Footer.js as a string.
    this.footer = fs.readFileSync(footerLocation, 'utf8')
  }

  setNewVersion (type) {
    // Get the previous version.
    this.previousVersion = this.packageJson
      .match(versionRegExp)[0] /* example: '"version": "1.1.1"' */
      .replace(versionDeclarationRegExp, '') /* remove the 'version' attribute name */
      .replace(/"/g, '') /* remove double quotes */
    // Split the three version digits.
    let versionDigits = this.previousVersion.split('.').map(n => +n)
    // Build the new version from the previous one based on upgrade type.
    switch (type) {
      case 'patch': this.newVersion = `${versionDigits[0]}.${versionDigits[1]}.${versionDigits[2] + 1}`; break
      case 'minor': this.newVersion = `${versionDigits[0]}.${versionDigits[1] + 1}.0`; break
      case 'major': this.newVersion = `${versionDigits[0] + 1}.0.0`; break
    }
  }

  getNewVersion () {
    return this.newVersion
  }

  upgradePackageJson () {
    // Updated the package.json string.
    this.packageJson = this.packageJson
      .replace(versionRegExp, `"version": "${this.newVersion}"`)
  }

  setVersionLog (log) {
    // Set the version log as class attribute.
    this.newVersionLog = log
    // Get the regular expression.
    let lastVersionLogRegExp = this.getLastVersionLogRegExp()
    // Get the matched string.
    let lastVersionLogString = this.packageJson.match(lastVersionLogRegExp)[0]
    // Update the package.json string.
    this.packageJson = this.packageJson
      .replace(lastVersionLogRegExp, lastVersionLogString + `,\n    "${this.newVersion}": "${this.newVersionLog}"`)
  }

  getLastVersionLogRegExp () {
    // Return the regular expression.
    return new RegExp('"[\\s]*' + this.previousVersion + '[\\s]*"[\\s]*:[\\s]*"(.+)"')
  }

  updateFooterVersion () {
    // Update the version in footer.
    this.footer = this.footer.replace(footerVersionRegExp, `Version ${this.newVersion}`)
  }

  savePackageJson () {
    // Replace previous package.json file.
    fs.writeFileSync(packageJsonLocation, this.packageJson, 'utf8')
  }

  saveFooter () {
    // Replace previous Footer.js file.
    fs.writeFileSync(footerLocation, this.footer, 'utf8')
  }
}

// =============================================================================
// Main.
// =============================================================================

// Functionality.
const performUpgrade = (type, log) => {
  // Create new object.
  let project = new Project()
  // Upgrade version, set log and save package.json.
  project.setNewVersion(type)
  project.upgradePackageJson()
  project.setVersionLog(log)
  project.updateFooterVersion()
  project.savePackageJson()
  project.saveFooter()
  // Return the new version.
  return project.getNewVersion()
}

// Main function.
const main = args => {
  if (args.length < 4) {
    console.log(`${chalk.red('✖')} Arguments needed: upgrade type, version description.`)
    return process.exit(1)
  }
  // Validate the upgrade type.
  if (upgradeTypes.indexOf(process.argv[2]) === -1) {
    console.log(`${chalk.red('✖')} Incorrect upgrade type: ${chalk.underline(process.argv[2])}.`)
    console.log(`Valid types: ${upgradeTypes.join(', ')}.`)
    return process.exit(1)
  }
  // Perform the upgrade.
  let newVersion = performUpgrade(process.argv[2], process.argv[3])
  // If everything went OK, log and exit.
  console.log(`${chalk.green('✔')} Successfully upgraded to version ${newVersion}`)
}

// Execute.
main(process.argv)
