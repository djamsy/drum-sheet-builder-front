// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'

// Components.
import NavbarButtonPreview from '../ButtonPreviews/NavbarButtonPreview/NavbarButtonPreview'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const section = (props) => {
  // Init the depth class.
  let depthClass = null
  if (+props.depth > 1) depthClass = `ml-${+props.depth - 1}0`

  return (
    <Fragment>
      <p className={depthClass}>You can open an existing project by clicking <NavbarButtonPreview>File</NavbarButtonPreview> - <NavbarButtonPreview>Open</NavbarButtonPreview>, on the main navbar. A new window will be opened at the right, showing the list of all your projects.</p>
    </Fragment>
  )
}

// Export.
export default section
