// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'
import { faFileAudio, faInfoCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// Components.
import Modal from '../../../../UI/Modal/Modal'
import FileButton from '../../../../UI/FileButton/FileButton'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
export const downloadWavModal = props => {
  return (
    <Modal
      title='Downloading WAV'
      show={props.modalOpen}
      modalClosed={props.downloadWAV}
    >
      <p>Your WAV audio file for project <b>{props.title}</b> has been generated successfully.</p>
      <p>Here's your download link (WAV format, 44.1 KHz):</p>
      <FileButton
        type='wav'
        icon={faFileAudio}
        fileName={`${props.title}.wav`}
        clicked={() => {
          props.downloadWAV()
          window.open(props.wavDownloadLink)
        }}
      />
      <p className='ModalTip'>
        <FontAwesomeIcon icon={faInfoCircle} /> Note that the downloaded version will correspond to the <u>last saved version of your project</u>.
      </p>
    </Modal>
  )
}

// Export.
export default downloadWavModal
