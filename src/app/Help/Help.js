// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Component, Fragment } from 'react'
import ReactDOM from 'react-dom'
import { Redirect, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

// Style.
import './Help.css'

// Components.
import Footer from '../Footer/Footer'
import NavbarLogo from '../SheetBuilder/SheetBuilderHeader/GlobalNavbar/NavbarLogo/NavbarLogo'
import NavbarButton from '../SheetBuilder/SheetBuilderHeader/GlobalNavbar/NavbarButton/NavbarButton'
import NavbarWatermark from '../SheetBuilder/SheetBuilderHeader/GlobalNavbar/NavbarWatermark/NavbarWatermark'

// Redux actions.
import * as actions from '../../store/actions/index'

// Sections.
import SectionIntroduction from './Sections/Introduction'
import SectionTheoryBehind from './Sections/TheoryBehind'
import SectionCreatingProject from './Sections/CreatingProject'
import SectionOpeningProject from './Sections/OpeningProject'
import SectionEditingProject from './Sections/EditingProject'
import SectionPlayingProject from './Sections/PlayingProject'
import SectionDownloadingProject from './Sections/DownloadingProject'
import SectionDownloadingWav from './Sections/DownloadingWav'
import SectionDownloadingMp3 from './Sections/DownloadingMp3'
import SectionDeletingProject from './Sections/DeletingProject'
import SectionSavingProject from './Sections/SavingProject'
import SectionProjectConfiguration from './Sections/ProjectConfiguration'

// Subsections.
import SubsectionLineTools from './Sections/Subsections/LineTools'
import SubsectionInstrumentTools from './Sections/Subsections/InstrumentTools'
import SubsectionInstrumentsOrder from './Sections/Subsections/InstrumentsOrder'
import SubsectionInstrumentIcons from './Sections/Subsections/InstrumentIcons'
import SubsectionGridOptions from './Sections/Subsections/GridOptions'
import SubsectionHitTypeDocs from './Sections/Subsections/HitTypeDocs'
import SubsectionSeparators from './Sections/Subsections/Separators'
import SubsectionHitTypeLabels from './Sections/Subsections/HitTypeLabels'
import SubsectionHitTypes from './Sections/Subsections/HitTypes'
import SubsectionPlayerMode from './Sections/Subsections/PlayMode'
import SubsectionMetronome from './Sections/Subsections/Metronome'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateful component declaration.
class Help extends Component {
  // Constructor.
  constructor (props) {
    super(props)
    // Init refs.
    this.creatingAProject = null
    this.openingAProject = null
    this.editingSheets = null
    this.lineTools = null
    this.instrumentTools = null
    this.separators = null
    this.changingHit = null
    this.hitTypes = null
    this.hitLabels = null
    this.savingProject = null
    this.playingProject = null
    this.downloadingSheets = null
    this.downloadingWav = null
    this.downloadingMp3 = null
    this.deletingProject = null
    this.projectConfiguration = null
    this.instrumentOrder = null
    this.instrumentIcons = null
    this.gridOptions = null
    this.playerLoopMode = null
    this.metronome = null
  }
  // Class method, inherited from Component.
  componentDidMount () {
    // Always scroll to top when component mounted.
    window.scrollTo(0, 0)
    // Stop player.
    this.props.stopPlaying()
  }

  // Handle section click.
  goTo = refName => {
    return e => {
      e.preventDefault()
      // Get the target position.
      let to = ReactDOM.findDOMNode(this[refName]).offsetTop - 62 /* menu height (32px) + extra 30px */
      // Go to that position.
      window.scrollTo(0, to)
    }
  }

  // Class method, inherited from Component.
  render () {
    // Check authentication state.
    if (!this.props.isAuth) {
      // Change the redirection path.
      this.props.setAuthRedirectPath('/docs')
      // Redirect to /login.
      return <Redirect to='/login' />
    }

    let body = (
      <Fragment>
        <div className='HelpContainer'>
          <div className='HelpMenu'>
            <h1>DrumSheet<b>Builder</b> <small>docs</small></h1>
            <div className='MenuListContainer'>
              <div>
                <button onClick={this.goTo('introduction')}><span>1.</span> Introducion</button>
                <button onClick={this.goTo('musicTheory')}><span>2.</span> The musical theory behind</button>
                <button onClick={this.goTo('creatingAProject')}><span>3.</span> Creating a project</button>
                <button onClick={this.goTo('openingAProject')}><span>4.</span> Opening an existing project</button>
                <button className='mb' onClick={this.goTo('editingSheets')}><span>5.</span> Editing your sheets</button>
                <div className='mb'>
                  <button onClick={this.goTo('lineTools')}><span>5.1</span> Line tools</button>
                  <button onClick={this.goTo('instrumentTools')}><span>5.2</span> Instrument tools</button>
                  <button onClick={this.goTo('separators')}><span>5.3</span> Separators</button>
                  <button className='mb' onClick={this.goTo('changingHit')}><span>5.4</span> Changing type of hit</button>
                  <div>
                    <button onClick={this.goTo('hitTypes')}><span>5.4.1</span> Types of hits</button>
                    <button className='mb' onClick={this.goTo('hitLabels')}><span>5.4.2</span> Hit labels</button>
                  </div>
                </div>
                <button onClick={this.goTo('savingProject')}><span>6.</span> Saving a project</button>
                <button onClick={this.goTo('playingProject')}><span>7.</span> Playing a project</button>
                <button onClick={this.goTo('downloadingSheets')}><span>8.</span> Downloading sheets in PDF format</button>
                <button onClick={this.goTo('downloadingWav')}><span>9.</span> Downloading audio in WAV format</button>
                <button onClick={this.goTo('downloadingMp3')}><span>10.</span> Downloading audio in MP3 format</button>
                <button onClick={this.goTo('deletingProject')}><span>11.</span> Deleting a project</button>
                <button className='mb' onClick={this.goTo('projectConfiguration')}><span>11.</span> Configuring your project</button>
                <div className='mb'>
                  <button onClick={this.goTo('instrumentOrder')}><span>12.1</span> Order of instruments</button>
                  <button onClick={this.goTo('instrumentIcons')}><span>12.2</span> Instrument icons</button>
                  <button onClick={this.goTo('gridOptions')}><span>12.3</span> Grid options</button>
                  <button onClick={this.goTo('playerLoopMode')}><span>12.4</span> Player loop mode</button>
                  <button onClick={this.goTo('metronome')}><span>12.5</span> Playing with metronome</button>
                </div>
              </div>
            </div>
          </div>

          <div className='HelpBody'>
            <h1 ref={ref => { this.introduction = ref }}><span>1.</span> Introduction</h1>
            <SectionIntroduction depth='1' />

            <h1 ref={ref => { this.musicTheory = ref }}><span>2.</span> The musical theory behind</h1>
            <SectionTheoryBehind depth='1' />

            <h1 ref={ref => { this.creatingAProject = ref }}><span>3.</span> Creating a project</h1>
            <SectionCreatingProject depth='1' />

            <h1 ref={ref => { this.openingAProject = ref }}><span>4.</span> Opening an existing project</h1>
            <SectionOpeningProject depth='1' />

            <h1 ref={ref => { this.editingSheets = ref }}><span>5.</span> Editing your sheets</h1>
            <SectionEditingProject depth='1' />

            <h2 ref={ref => { this.lineTools = ref }}><span>5.1.</span> Line tools</h2>
            <SubsectionLineTools depth='2' />

            <h2 ref={ref => { this.instrumentTools = ref }}><span>5.2.</span> Instrument tools</h2>
            <SubsectionInstrumentTools depth='2' />

            <h2 ref={ref => { this.separators = ref }}><span>5.3.</span> Separators</h2>
            <SubsectionSeparators depth='2' />

            <h2 ref={ref => { this.changingHit = ref }}><span>5.4.</span> Changing type of hit</h2>
            <SubsectionHitTypes depth='2' />

            <h3 ref={ref => { this.hitTypes = ref }}><span>5.4.1.</span> Types of hits</h3>
            <SubsectionHitTypeDocs depth='3' />

            <h3 ref={ref => { this.hitLabels = ref }}><span>5.4.2.</span> Hit labels</h3>
            <SubsectionHitTypeLabels depth='3' />

            <h1 ref={ref => { this.savingProject = ref }}><span>6.</span> Saving a project</h1>
            <SectionSavingProject depth='1' />

            <h1 ref={ref => { this.playingProject = ref }}><span>7.</span> Playing a project</h1>
            <SectionPlayingProject depth='1' />

            <h1 ref={ref => { this.downloadingSheets = ref }}><span>8.</span> Downloading sheets in PDF format</h1>
            <SectionDownloadingProject depth='1' />

            <h1 ref={ref => { this.downloadingWav = ref }}><span>9.</span> Downloading audio in WAV format</h1>
            <SectionDownloadingWav depth='1' />

            <h1 ref={ref => { this.downloadingMp3 = ref }}><span>10.</span> Downloading audio in MP3 format</h1>
            <SectionDownloadingMp3 depth='1' />

            <h1 ref={ref => { this.deletingProject = ref }}><span>11.</span> Deleting a project</h1>
            <SectionDeletingProject depth='1' />

            <h1 ref={ref => { this.projectConfiguration = ref }}><span>12.</span> Configuring your project</h1>
            <SectionProjectConfiguration depth='1' />

            <h2 ref={ref => { this.instrumentOrder = ref }}><span>12.1.</span>Order of instruments</h2>
            <SubsectionInstrumentsOrder depth='2' />

            <h2 ref={ref => { this.instrumentIcons = ref }}><span>12.2.</span>Instrument icons</h2>
            <SubsectionInstrumentIcons depth='2' />

            <h2 ref={ref => { this.gridOptions = ref }}><span>12.3.</span>Grid options</h2>
            <SubsectionGridOptions depth='2' />

            <h2 ref={ref => { this.playerLoopMode = ref }}><span>11.4.</span>Player loop mode</h2>
            <SubsectionPlayerMode depth='2' />

            <h2 ref={ref => { this.metronome = ref }}><span>11.5.</span>Playing with metronome</h2>
            <SubsectionMetronome depth='2' />
          </div>
        </div>
      </Fragment>
    )

    return (
      <div className='Help'>
        <header>
          <div className='GlobalNavbar'>
            <NavbarLogo />
            <NavbarButton left clicked={() => this.props.history.push('/')}>
              HOME
            </NavbarButton>
            <NavbarButton right clicked={() => {
              this.props.logout()
              this.props.history.push('/')
            }}>
              Log out
            </NavbarButton>
            <NavbarWatermark content={this.props.email} />
          </div>
        </header>
        {body}
        <Footer />
      </div>
    )
  }
}

// ============================================================================
// Connect with Redux and export.
// ============================================================================

// State mapping.
const mapStateToProps = state => {
  return {
    email: state.auth.email
  }
}

// Action mapping.
const mapDispatchToProps = dispatch => {
  return {
    logout: () => {
      dispatch(actions.logout())
      dispatch(actions.newProject())
      dispatch(actions.hideAlert())
    },
    stopPlaying: () => dispatch(actions.stopPlaying()),
    setAuthRedirectPath: path => dispatch(actions.setAuthRedirectPath(path))
  }
}

// Export.
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Help))
