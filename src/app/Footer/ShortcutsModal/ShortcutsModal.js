// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import _ from 'lodash'
import React, { Fragment } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faKeyboard } from '@fortawesome/free-solid-svg-icons'

// Components.
import Modal from '../../UI/Modal/Modal'
import KeyboardKey from '../../UI/KeyboardKey/KeyboardKey'

// Style.
import './ShortcutsModal.css'

// Constants.
import { shortcuts } from '../../../shared/constants'
import { ucFirst } from '../../../shared/functions'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateful component declaration.
const shortcutsModal = (props) => {
  // Create mapped shortcuts object.
  let mappedShortcuts = {
    project: {
      description: 'Executed when a project is open',
      shortcuts: _.filter(shortcuts, { type: 'project' })
    },
    line: {
      description: 'Executed when your mouse is over a line',
      shortcuts: _.filter(shortcuts, { type: 'line' })
    }
  }

  return (
    <Modal
      title='Shortcut cheat sheet'
      show={props.modalOpen}
      modalClosed={props.hideModal}
    >
      <div className='ShortcutsModal'>
        {
          Object.keys(mappedShortcuts).map(shortcutType => {
            return (
              <Fragment key={shortcutType}>
                <h1>
                  <FontAwesomeIcon icon={faKeyboard} />
                  <span className='IconLabel'>{ucFirst(shortcutType)} shortcuts</span>
                </h1>
                <h4>
                  {mappedShortcuts[shortcutType].description}
                </h4>
                <table>
                  <tbody>
                    {
                      Object.keys(mappedShortcuts[shortcutType].shortcuts).map((i, idx) => {
                        return (
                          <tr key={idx}>
                            <td>
                              {
                                mappedShortcuts[shortcutType].shortcuts[i].keys.map((key, idx) => (
                                  <KeyboardKey idx={idx} key={idx}>{key}</KeyboardKey>
                                ))
                              }
                            </td>
                            <td>
                              {mappedShortcuts[shortcutType].shortcuts[i].description}
                            </td>
                          </tr>
                        )
                      })
                    }
                  </tbody>
                </table>
              </Fragment>
            )
          })
        }
      </div>
    </Modal>
  )
}

// Export.
export default shortcutsModal
