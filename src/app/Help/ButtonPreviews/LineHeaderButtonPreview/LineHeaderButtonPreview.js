// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Style.
import './LineHeaderButtonPreview.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const lineHeaderButtonPreview = (props) => {
  return (
    <span className='LineHeaderButtonPreview'>
      {props.children}
    </span>
  )
}

// Export.
export default lineHeaderButtonPreview
