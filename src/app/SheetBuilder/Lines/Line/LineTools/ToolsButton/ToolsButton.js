// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'

// Components.
import './ToolsButton.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const toolsButton = (props) => {
  return (
    <div className={'ToolsButton' + (props.type ? ' btn-' + props.type : 'btn-default')} style={{ backgroundColor: props.backgroundColor }}>
      <button onClick={props.clicked} className={props.active ? 'active' : null}>
        {props.children}
      </button>
    </div>
  )
}

// Export.
export default toolsButton
