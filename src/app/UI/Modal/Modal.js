// ============================================================================
// Dependencies.
// ============================================================================

// Vendor.
import React, { Component, Fragment } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'

// Style.
import './Modal.css'

// Components.
import Backdrop from '../Backdrop/Backdrop'

// ============================================================================
// Stateful component declaration.
// ============================================================================

class Modal extends Component {
  // Only update the component when the modal is shown or hidden.
  shouldComponentUpdate (nextProps, nextState) {
    return nextProps.show !== this.props.show ||
      nextProps.children !== this.props.children
  }

  render () {
    // Init the close button.
    let closeButton = null
    if (!this.props.noAutoClose) {
      closeButton = (
        <button type='Button' onClick={this.props.modalClosed}>
          <FontAwesomeIcon icon={faTimes} />
        </button>
      )
    }

    return (
      <Fragment>
        <Backdrop show={this.props.show} clicked={() => { if (!this.props.noAutoClose) this.props.modalClosed() }} />
        <div className='Modal' style={{
          transform: this.props.show ? 'translateY(0)' : 'translateY(-100vh)',
          opacity: this.props.show ? '1' : '0'
        }}>
          <div className='ModalHeader'>
            {closeButton}
            <h2>{this.props.title}</h2>
          </div>
          <div className='ModalBody'>
            {this.props.children}
          </div>
        </div>
      </Fragment>
    )
  }
}

// Export.
export default Modal
