// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React, { Fragment } from 'react'

// Constants.
import { minimumBpm, maximumBpm } from '../../../shared/constants'

// Components.
import Image from '../../UI/Image/Image'

// Screenshots.
import timeSignatureScreenshot from '../../../images/Screenshots/time-signature.jpg'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const section = (props) => {
  // Init the depth class.
  let depthClass = null
  if (+props.depth > 1) depthClass = `ml-${+props.depth - 1}0`

  return (
    <Fragment>
      <p className={depthClass}>DrumSheet<b>Builder</b> offers you the possibility of writing compositions of low-medium complexity, which is why it's designed for low-medium level drummers. You must take into consideration that this is only an alternative way to write music, and not a way of writing <b>music sheets</b>. You should <b>never avoid</b> learning how to write or read <b>music sheets</b>, as they offer you 100% freedom for composing music.</p>

      <p className={depthClass}>However, we incorporate some of the core musical concepts for writing drum sheets:</p>

      <ul className={depthClass}>
        <li>
          <b>BPM</b>: the speed of your song, in beats per minute (default value: <b>90 bpm</b>). It's the way of representing the <b>tempo</b> of your song.
          <ul>
            <li>The minimum and maximum values allowed are, respectively, {minimumBpm} and {maximumBpm}.</li>
            <li>You can listen to a metronome that plays the <b>bpm</b> you select.</li>
            <li>Check the documentation on <a target='_blank' rel='noopener noreferrer' href='https://en.wikipedia.org/wiki/Tempo'>Wikipedia</a>!</li>
          </ul>
        </li>
        <li>
          <b>Time signature</b>: a measure used to specify how many beats (pulses) are contained in each measure (bar), and which note value is equivalent to a beat (default value: <b>8/8</b>).
          <ul>
            <li>This value will determine the shape and the size of the lines.</li>
            <li>Check the documentation on <a target='_blank' rel='noopener noreferrer' href='https://en.wikipedia.org/wiki/Time_signature'>Wikipedia</a>!</li>
          </ul>
        </li>
      </ul>

      <div className='img-container'>
        <Image src={timeSignatureScreenshot} alt='Canonical representation of the time signature in music sheets.' />
        <div className='img-legend'>Canonical representation of the time signature in music sheets.</div>
      </div>

      <p className={depthClass}>Also, you can configure the instrument icons to be an approximation to the canonical representation of musical notes (see section <i>Configuring your project</i>).</p>
    </Fragment>
  )
}

// Export.
export default section
