// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'
import { Tooltip } from 'react-lightweight-tooltip'

// Styles.
import './Tooltip.css'

// =============================================================================
// Constants.
// =============================================================================

// Base tooltip style.
const baseStyle = {
  wrapper: {
    position: 'relative',
    display: 'inline-block',
    zIndex: 20,
    color: '#555',
    cursor: 'inherit'
  },
  tooltip: {
    padding: '6px 6px 6px 6px',
    maxWidth: 180,
    minWidth: 120,
    textAlign: 'center',
    position: 'absolute',
    zIndex: 20,
    background: '#000',
    left: '50%',
    WebkitTransform: 'translateX(-50%)',
    msTransform: 'translateX(-50%)',
    OTransform: 'translateX(-50%)',
    transform: 'translateX(-50%)'
  },
  content: {
    display: 'block',
    background: '#000',
    color: '#fff',
    fontSize: 10,
    padding: 0,
    whiteSpace: 'none'
  },
  arrow: {
    position: 'absolute',
    width: '0',
    height: '0',
    left: '50%',
    marginLeft: '-5px',
    borderLeft: 'solid transparent 5px',
    borderRight: 'solid transparent 5px'
  },
  gap: {
    position: 'absolute',
    width: '1px',
    height: '20px',
    visibility: 'hidden'
  }
}

// Top style.
const topStyle = {
  wrapper: baseStyle.wrapper,
  tooltip: {
    ...baseStyle.tooltip,
    top: null,
    bottom: '100%',
    marginBottom: 6,
    marginTop: 0,
    paddingBottom: 10
  },
  content: baseStyle.content,
  arrow: {
    ...baseStyle.arrow,
    bottom: '-5px',
    borderTop: 'solid #000 5px'
  },
  gap: {
    ...baseStyle.gap,
    bottom: '-20px'
  }
}

// Bottom style.
const bottomStyle = {
  wrapper: baseStyle.wrapper,
  tooltip: {
    ...baseStyle.tooltip,
    top: '100%',
    bottom: null,
    marginTop: 6,
    marginBottom: 0,
    paddingBottom: 8
  },
  content: baseStyle.content,
  arrow: {
    ...baseStyle.arrow,
    top: '-5px',
    borderTop: 'solid #000 5px',
    transform: 'rotate(180deg)',
    WebkitTransform: 'rotate(180deg)'
  },
  gap: {
    ...baseStyle.gap,
    top: '-20px'
  }
}

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const tooltip = (props) => {
  let style = null
  switch (props.placement) {
    case 'top': style = topStyle; break
    case 'bottom': style = bottomStyle; break
    default:
  }

  return (
    <Tooltip content={props.content} styles={style}>
      {props.children}
    </Tooltip>
  )
}

// Export.
export default tooltip
