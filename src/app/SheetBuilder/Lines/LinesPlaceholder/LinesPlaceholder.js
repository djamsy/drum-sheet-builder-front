// =============================================================================
// Dependencies.
// =============================================================================

// Vendor.
import React from 'react'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons'

// Styles.
import './LinesPlaceholder.css'

// =============================================================================
// Component declaration.
// =============================================================================

// Stateless component declaration.
const linesPlaceholder = () => (
  <div className='LinesPlaceholder'>
    <div>
      <p className='p1'>Type title, BPM and time signature and then click <span>Start project</span></p>
      <p className='p2'><FontAwesomeIcon icon={faQuestionCircle} /> Need help? Check the <Link to='/docs'>documentation</Link>!</p>
    </div>
  </div>
)

// Export.
export default linesPlaceholder
